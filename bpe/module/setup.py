#! /usr/bin/env python2

from distutils.core import setup, Extension

setup(
    name="fastsearch",
    ext_modules=[
        Extension("fastsearch",
                  extra_compile_args=[
                      "-O3",
                      "-Wall",
                      "-Wextra",
                      "-Werror",
                  ],
                  sources=["fastsearch.c"]
                  )
    ]
)
