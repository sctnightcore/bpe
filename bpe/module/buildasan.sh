#!/bin/bash

rm -rf build/temp.*
rm *.so

source rungcc7
export x86_64-linux-gnu-gcc=gcc

export ASAN_OPTIONS="detect_leaks=0:detect_stack_use_after_return=true:strict_init_order=true"
export CFLAGS="-fsanitize=address -fsanitize=undefined \
-fsanitize=shift -fsanitize=integer-divide-by-zero -fsanitize=unreachable \
-fsanitize=vla-bound -fsanitize=null -fsanitize=return \
-fsanitize=signed-integer-overflow -fsanitize=bounds -fsanitize=alignment \
-fsanitize=object-size -fsanitize=float-divide-by-zero -fsanitize=float-cast-overflow \
-fsanitize=nonnull-attribute -fsanitize=returns-nonnull-attribute -fsanitize=bool \
-fsanitize=enum -fsanitize=vptr -fsanitize=bounds-strict \
-fsanitize=leak \
-fsanitize=shift-exponent -fsanitize=shift-base -fsanitize=bounds-strict \
-fsanitize-address-use-after-scope"

python setup.py build_ext --inplace
