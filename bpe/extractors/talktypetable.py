#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchTalkTypeTable(self):
    offset, section = self.exe.string(b"/help")
    if offset is False:
        self.log("failed in search /help")
        exit(1)
    helpHex = self.exe.toHex(section.rawToVa(offset), 4)

    # search in CSession_InitTalkTypeTable
    # 0  push ebp
    # 1  mov ebp, esp
    # 3  sub esp, 8
    # 6  push esi
    # 7  push edi
    # 8  mov edi, ecx
    # 10 mov [ebp+value.first], offset aHelp
    # 17 mov eax, [edi+CSession.m_talkTypeTable._First]
    # 23 lea esi, [edi+CSession.m_talkTypeTable]
    # 29 mov ecx, esi
    # 31 mov [esi+4], eax
    # 34 lea eax, [ebp+value]
    # 37 push eax
    # 38 mov [ebp+value.second], 0Eh
    # 45 call std_vector_TALKTYPE_insert
    code = (
        b"\x55"                            # 0 push ebp
        b"\x8B\xEC"                        # 1 mov ebp, esp
        b"\x83\xEC\x08"                    # 3 sub esp, 8
        b"\x56"                            # 6 push esi
        b"\x57"                            # 7 push edi
        b"\x8B\xF9"                        # 8 mov edi, ecx
        b"\xC7\x45\xAB" + helpHex +        # 10 mov [ebp+value.first], offset a
        b"\x8B\x87\xAB\xAB\xAB\xAB"        # 17 mov eax, [edi+CSession.m_talkTy
        b"\x8D\xB7\xAB\xAB\xAB\xAB"        # 23 lea esi, [edi+CSession.m_talkTy
        b"\x8B\xCE"                        # 29 mov ecx, esi
        b"\x89\x46\xAB"                    # 31 mov [esi+4], eax
        b"\x8D\x45\xAB"                    # 34 lea eax, [ebp+value]
        b"\x50"                            # 37 push eax
        b"\xC7\x45\xAB\xAB\xAB\xAB\x00"    # 38 mov [ebp+value.second], 0Eh
        b"\xE8"                            # 45 call std_vector_TALKTYPE_insert
    )
    valueFirstOffset = (12, 1, 0)
    valueOffset = (36, 1, 0)
    valueSecondOffet = (40, 1, 0)
    talkTypeTableOffset = (19, 4)
    talkTypeTableAllocOffset = -1
    talkTypeTableFirstOffset = (25, 4, True)
    talkTypeTableLastOffset = (33, 1, False)
    cmdIdOffset = (41, 4)
    insertOffset = 46
    clearOffset = 0
    stringAllocOffset = 0
    offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  push ebp
        # 1  mov ebp, esp
        # 3  sub esp, 0Ch
        # 6  mov eax, [ecx+CSession.m_talkTypeTable._First]
        # 12 push ebx
        # 13 push esi
        # 14 lea esi, [ecx+CSession.m_talkTypeTable]
        # 20 mov [ebp+var_4], ecx
        # 23 mov ecx, [esi+std_vector_pair_ci._Last]
        # 26 push edi
        # 27 cmp eax, ecx
        # 29 jz short loc_8EA8F8
        # 31 mov edx, ecx
        # 33 cmp ecx, ecx
        # 35 jz short loc_8EA8F5
        # 37 mov edi, ecx
        # 39 sub edi, eax
        # 41 lea esp, [esp+0]
        # 48 mov ebx, [edx]
        # 50 mov [eax+pair_char_ptr_int.first], ebx
        # 52 mov ebx, [edi+eax+pair_char_ptr_int.second]
        # 56 mov [eax+pair_char_ptr_int.second], ebx
        # 59 add edx, 8
        # 62 add eax, 8
        # 65 cmp edx, ecx
        # 67 jnz short loc_8EA8E0
        # 69 mov [esi+std_vector_pair_ci._Last], eax
        # 72 lea eax, [ebp+value]
        # 75 mov edi, 0Eh
        # 80 push eax
        # 81 mov ecx, esi
        # 83 mov [ebp+value.first], offset aHelp
        # 90 mov [ebp+value.second], edi
        # 93 call std_vector_TALKTYPE_insert
        code = (
            b"\x55"                            # 0 push ebp
            b"\x8B\xEC"                        # 1 mov ebp, esp
            b"\x83\xEC\x0C"                    # 3 sub esp, 0Ch
            b"\x8B\x81\xAB\xAB\xAB\xAB"        # 6 mov eax, [ecx+CSession.m_tal
            b"\x53"                            # 12 push ebx
            b"\x56"                            # 13 push esi
            b"\x8D\xB1\xAB\xAB\xAB\xAB"        # 14 lea esi, [ecx+CSession.m_ta
            b"\x89\x4D\xAB"                    # 20 mov [ebp+var_4], ecx
            b"\x8B\x4E\xAB"                    # 23 mov ecx, [esi+std_vector_pa
            b"\x57"                            # 26 push edi
            b"\x3B\xC1"                        # 27 cmp eax, ecx
            b"\x74\x29"                        # 29 jz short loc_8EA8F8
            b"\x8B\xD1"                        # 31 mov edx, ecx
            b"\x3B\xC9"                        # 33 cmp ecx, ecx
            b"\x74\x20"                        # 35 jz short loc_8EA8F5
            b"\x8B\xF9"                        # 37 mov edi, ecx
            b"\x2B\xF8"                        # 39 sub edi, eax
            b"\x8D\xA4\x24\x00\x00\x00\x00"    # 41 lea esp, [esp+0]
            b"\x8B\x1A"                        # 48 mov ebx, [edx]
            b"\x89\xAB"                        # 50 mov [eax+pair_char_ptr_int.
            b"\x8B\x5C\xAB\xAB"                # 52 mov ebx, [edi+eax+pair_char
            b"\x89\x58\xAB"                    # 56 mov [eax+pair_char_ptr_int.
            b"\x83\xC2\x08"                    # 59 add edx, 8
            b"\x83\xC0\x08"                    # 62 add eax, 8
            b"\x3B\xD1"                        # 65 cmp edx, ecx
            b"\x75\xEB"                        # 67 jnz short loc_8EA8E0
            b"\x89\x46\xAB"                    # 69 mov [esi+std_vector_pair_ci
            b"\x8D\x45\xAB"                    # 72 lea eax, [ebp+value]
            b"\xBF\xAB\xAB\xAB\xAB"            # 75 mov edi, 0Eh
            b"\x50"                            # 80 push eax
            b"\x8B\xCE"                        # 81 mov ecx, esi
            b"\xC7\x45\xAB" + helpHex +        # 83 mov [ebp+value.first], offs
            b"\x89\x7D\xAB"                    # 90 mov [ebp+value.second], edi
            b"\xE8"                            # 93 call std_vector_TALKTYPE_in
        )
        valueFirstOffset = (85, 1, 0)
        valueOffset = (74, 1, 0)
        valueSecondOffet = (92, 1, 0)
        talkTypeTableOffset = (16, 4)
        talkTypeTableAllocOffset = -1
        talkTypeTableFirstOffset = (8, 4, True)
        talkTypeTableLastOffset = (25, 1, False)
        cmdIdOffset = (76, 4)
        insertOffset = 94
        clearOffset = 0
        stringAllocOffset = 0
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  push ebp
        # 1  mov ebp, esp
        # 3  and esp, 0FFFFFFF8h
        # 6  sub esp, 10h
        # 9  push ebx
        # 10 push ebp
        # 11 mov ebx, ecx
        # 13 mov ecx, [ebx+CSession.m_talkTypeTable._First]
        # 19 mov edx, [ebx+CSession.m_talkTypeTable._Last]
        # 25 push esi
        # 26 lea esi, [ebx+CSession.m_talkTypeTable]
        # 32 push edi
        # 33 cmp ecx, edx
        # 35 jz short loc_84F817
        # 37 mov eax, edx
        # 39 cmp edx, edx
        # 41 jz short loc_84F814
        # 43 jmp short loc_84F800
        # 45 align 10h
        # 48 mov edi, [eax+pair_char_ptr_int.first]
        # 50 mov [ecx+pair_char_ptr_int.first], edi
        # 52 mov edi, [eax+pair_char_ptr_int.second]
        # 55 mov [ecx+pair_char_ptr_int.second], edi
        # 58 add eax, 8
        # 61 add ecx, 8
        # 64 cmp eax, edx
        # 66 jnz short loc_84F800
        # 68 mov [esi+std_vector_pair_ci._Last], ecx
        # 71 lea eax, [esp+20h+value]
        # 75 mov edi, 0Eh
        # 80 push eax
        # 81 mov ecx, esi
        # 83 mov [esp+24h+value.first], offset aHelp
        # 91 mov [esp+24h+value.second], edi
        # 95 call std_vector_TALKTYPE_insert
        code = (
            b"\x55"                            # 0 push ebp
            b"\x8B\xEC"                        # 1 mov ebp, esp
            b"\x83\xE4\xF8"                    # 3 and esp, 0FFFFFFF8h
            b"\x83\xEC\xAB"                    # 6 sub esp, 10h
            b"\x53"                            # 9 push ebx
            b"\x55"                            # 10 push ebp
            b"\x8B\xD9"                        # 11 mov ebx, ecx
            b"\x8B\x8B\xAB\xAB\xAB\xAB"        # 13 mov ecx, [ebx+CSession.m_ta
            b"\x8B\x93\xAB\xAB\xAB\xAB"        # 19 mov edx, [ebx+CSession.m_ta
            b"\x56"                            # 25 push esi
            b"\x8D\xB3\xAB\xAB\xAB\xAB"        # 26 lea esi, [ebx+CSession.m_ta
            b"\x57"                            # 32 push edi
            b"\x3B\xCA"                        # 33 cmp ecx, edx
            b"\x74\x22"                        # 35 jz short loc_84F817
            b"\x8B\xC2"                        # 37 mov eax, edx
            b"\x3B\xD2"                        # 39 cmp edx, edx
            b"\x74\x19"                        # 41 jz short loc_84F814
            b"\xEB\x03"                        # 43 jmp short loc_84F800
            b"\xAB\xAB\xAB"                    # 45 align 10h
            b"\x8B\xAB"                        # 48 mov edi, [eax+pair_char_ptr
            b"\x89\xAB"                        # 50 mov [ecx+pair_char_ptr_int.
            b"\x8B\x78\xAB"                    # 52 mov edi, [eax+pair_char_ptr
            b"\x89\x79\xAB"                    # 55 mov [ecx+pair_char_ptr_int.
            b"\x83\xC0\x08"                    # 58 add eax, 8
            b"\x83\xC1\x08"                    # 61 add ecx, 8
            b"\x3B\xC2"                        # 64 cmp eax, edx
            b"\x75\xEC"                        # 66 jnz short loc_84F800
            b"\x89\x4E\xAB"                    # 68 mov [esi+std_vector_pair_ci
            b"\x8D\x44\x24\xAB"                # 71 lea eax, [esp+20h+value]
            b"\xBF\xAB\xAB\xAB\xAB"            # 75 mov edi, 0Eh
            b"\x50"                            # 80 push eax
            b"\x8B\xCE"                        # 81 mov ecx, esi
            b"\xC7\x44\x24\xAB" + helpHex +    # 83 mov [esp+24h+value.first],
            b"\x89\x7C\x24\xAB"                # 91 mov [esp+24h+value.second],
            b"\xE8"                            # 95 call std_vector_TALKTYPE_in
        )
        valueFirstOffset = (86, 1, 0)
        valueOffset = (74, 1, 4)
        valueSecondOffet = (94, 1, 0)
        talkTypeTableOffset = (28, 4)
        talkTypeTableAllocOffset = (28, 4, True)
        talkTypeTableFirstOffset = (15, 4, True)
        talkTypeTableLastOffset = (70, 1, False)
        cmdIdOffset = (76, 4)
        insertOffset = 96
        clearOffset = 0
        stringAllocOffset = 0
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  push ebp
        # 1  mov ebp, esp
        # 3  sub esp, 14h
        # 6  push ebx
        # 7  mov ebx, ecx
        # 9  mov ecx, [ebx+CSession.m_talkTypeTable._First]
        # 15 push esi
        # 16 mov eax, [ebx+CSession.m_talkTypeTable._Last]
        # 22 lea esi, [ebx+CSession.m_talkTypeTable]
        # 28 push edi
        # 29 push eax
        # 30 push ecx
        # 31 mov ecx, esi
        # 33 call std_vector_TALKTYPE_clear
        # 38 mov edi, 0Eh
        # 43 mov [ebp+value.first], offset aHelp
        # 50 mov [ebp+value.last], edi
        # 53 mov ecx, [esi+std_vector_pair_ci._Last]
        # 56 lea eax, [ebp+value]
        # 59 push eax
        # 60 push ecx
        # 61 mov ecx, esi
        # 63 call std_vector_TALKTYPE_insert
        code = (
            b"\x55"                            # 0 push ebp
            b"\x8B\xEC"                        # 1 mov ebp, esp
            b"\x83\xEC\xAB"                    # 3 sub esp, 14h
            b"\x53"                            # 6 push ebx
            b"\x8B\xD9"                        # 7 mov ebx, ecx
            b"\x8B\x8B\xAB\xAB\xAB\xAB"        # 9 mov ecx, [ebx+CSession.m_tal
            b"\x56"                            # 15 push esi
            b"\x8B\x83\xAB\xAB\xAB\xAB"        # 16 mov eax, [ebx+CSession.m_ta
            b"\x8D\xB3\xAB\xAB\xAB\xAB"        # 22 lea esi, [ebx+CSession.m_ta
            b"\x57"                            # 28 push edi
            b"\x50"                            # 29 push eax
            b"\x51"                            # 30 push ecx
            b"\x8B\xCE"                        # 31 mov ecx, esi
            b"\xE8\xAB\xAB\xAB\xAB"            # 33 call std_vector_TALKTYPE_cl
            b"\xBF\xAB\xAB\xAB\xAB"            # 38 mov edi, 0Eh
            b"\xC7\x45\xAB" + helpHex +        # 43 mov [ebp+value.first], offs
            b"\x89\x7D\xAB"                    # 50 mov [ebp+value.second], edi
            b"\x8B\x4E\xAB"                    # 53 mov ecx, [esi+std_vector_pa
            b"\x8D\x45\xAB"                    # 56 lea eax, [ebp+value]
            b"\x50"                            # 59 push eax
            b"\x51"                            # 60 push ecx
            b"\x8B\xCE"                        # 61 mov ecx, esi
            b"\xE8"                            # 63 call std_vector_TALKTYPE_in
        )
        valueFirstOffset = (45, 1, 0)
        valueOffset = (58, 1, 0)
        valueSecondOffet = (52, 1, 0)
        talkTypeTableOffset = (24, 4)
        talkTypeTableAllocOffset = (24, 4, True)
        talkTypeTableFirstOffset = (11, 4, True)
        talkTypeTableLastOffset = (55, 1, False)
        cmdIdOffset = (39, 4)
        insertOffset = 64
        clearOffset = 34
        stringAllocOffset = 0
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  push ebp
        # 1  mov ebp, esp
        # 3  sub esp, 18h
        # 6  mov eax, [ecx+CSession.m_talkTypeTable._Last]
        # 12 push ebx
        # 13 push esi
        # 14 lea esi, [ecx+CSession.m_talkTypeTable]
        # 20 mov [ebp+var_4], ecx
        # 23 push edi
        # 24 mov ecx, [esi+std_vector_pair_ci._First]
        # 27 push eax
        # 28 push ecx
        # 29 mov ecx, esi
        # 31 call std_vector_TALKTYPE_clear
        # 36 mov edi, 0Eh
        # 41 mov [ebp+value.first], offset aHelp
        # 48 mov [ebp+value.second], edi
        # 51 mov ecx, [esi+std_vector_pair_ci._Last]
        # 54 lea eax, [ebp+value]
        # 57 push eax
        # 58 push ecx
        # 59 mov ecx, esi
        # 61 call std_vector_TALKTYPE_insert
        code = (
            b"\x55"                            # 0 push ebp
            b"\x8B\xEC"                        # 1 mov ebp, esp
            b"\x83\xEC\xAB"                    # 3 sub esp, 18h
            b"\x8B\x81\xAB\xAB\xAB\xAB"        # 6 mov eax, [ecx+CSession.m_tal
            b"\x53"                            # 12 push ebx
            b"\x56"                            # 13 push esi
            b"\x8D\xB1\xAB\xAB\xAB\xAB"        # 14 lea esi, [ecx+CSession.m_ta
            b"\x89\x4D\xAB"                    # 20 mov [ebp+var_4], ecx
            b"\x57"                            # 23 push edi
            b"\x8B\x4E\xAB"                    # 24 mov ecx, [esi+std_vector_pa
            b"\x50"                            # 27 push eax
            b"\x51"                            # 28 push ecx
            b"\x8B\xCE"                        # 29 mov ecx, esi
            b"\xE8\xAB\xAB\xAB\xAB"            # 31 call std_vector_TALKTYPE_cl
            b"\xBF\xAB\xAB\xAB\xAB"            # 36 mov edi, 0Eh
            b"\xC7\x45\xAB" + helpHex +        # 41 mov [ebp+value.first], offs
            b"\x89\x7D\xAB"                    # 48 mov [ebp+value.second], edi
            b"\x8B\x4E\xAB"                    # 51 mov ecx, [esi+std_vector_pa
            b"\x8D\x45\xAB"                    # 54 lea eax, [ebp+value]
            b"\x50"                            # 57 push eax
            b"\x51"                            # 58 push ecx
            b"\x8B\xCE"                        # 59 mov ecx, esi
            b"\xE8"                            # 61 call std_vector_TALKTYPE_in
        )
        valueFirstOffset = (43, 1, 0)
        valueOffset = (56, 1, 0)
        valueSecondOffet = (50, 1, 0)
        talkTypeTableOffset = (16, 4)
        talkTypeTableAllocOffset = (16, 4, True)
        talkTypeTableFirstOffset = (26, 1, False)
        talkTypeTableLastOffset = (53, 1, False)
        cmdIdOffset = (37, 4)
        insertOffset = 62
        clearOffset = 32
        stringAllocOffset = 0
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  push ebp
        # 1  mov ebp, esp
        # 3  and esp, 0FFFFFFF8h
        # 6  sub esp, 8
        # 9  push ebx
        # 10 push ebp
        # 11 push esi
        # 12 push edi
        # 13 mov edi, ecx
        # 15 mov ecx, [edi+CSession.m_talkTypeTable._First]
        # 21 mov edx, [edi+CSession.m_talkTypeTable._Last]
        # 27 lea esi, [edi+CSession.m_talkTypeTable]
        # 33 cmp ecx, edx
        # 35 jz short loc_69BED7
        # 37 mov eax, edx
        # 39 cmp edx, edx
        # 41 jz short loc_69BED4
        # 43 jmp short loc_69BEC0
        # 45 align 10h
        # 48 mov ebx, [eax+std_vector_pair_ci._Allocator]
        # 50 mov [ecx], ebx
        # 52 mov ebx, [eax+std_vector_pair_ci._First]
        # 55 mov [ecx+4], ebx
        # 58 add eax, 8
        # 61 add ecx, 8
        # 64 cmp eax, edx
        # 66 jnz short loc_69BEC0
        # 68 mov [esi+std_vector_pair_ci._Last], ecx
        # 71 lea eax, [esp+18h+value]
        # 75 mov ebx, 0Eh
        # 80 push eax
        # 81 mov ecx, esi
        # 83 mov [esp+1Ch+value.first], offset aHelp
        # 91 mov [esp+1Ch+value.second], ebx
        # 95 call std_vector_TALKTYPE_insert
        code = (
            b"\x55"                            # 0 push ebp
            b"\x8B\xEC"                        # 1 mov ebp, esp
            b"\x83\xE4\xF8"                    # 3 and esp, 0FFFFFFF8h
            b"\x83\xEC\xAB"                    # 6 sub esp, 8
            b"\x53"                            # 9 push ebx
            b"\x55"                            # 10 push ebp
            b"\x56"                            # 11 push esi
            b"\x57"                            # 12 push edi
            b"\x8B\xF9"                        # 13 mov edi, ecx
            b"\x8B\x8F\xAB\xAB\xAB\xAB"        # 15 mov ecx, [edi+CSession.m_ta
            b"\x8B\x97\xAB\xAB\xAB\xAB"        # 21 mov edx, [edi+CSession.m_ta
            b"\x8D\xB7\xAB\xAB\xAB\xAB"        # 27 lea esi, [edi+CSession.m_ta
            b"\x3B\xCA"                        # 33 cmp ecx, edx
            b"\x74\x22"                        # 35 jz short loc_69BED7
            b"\x8B\xC2"                        # 37 mov eax, edx
            b"\x3B\xD2"                        # 39 cmp edx, edx
            b"\x74\x19"                        # 41 jz short loc_69BED4
            b"\xEB\x03"                        # 43 jmp short loc_69BEC0
            b"\xAB\xAB\xAB"                    # 45 align 10h
            b"\x8B\xAB"                        # 48 mov ebx, [eax+std_vector_pa
            b"\x89\x19"                        # 50 mov [ecx], ebx
            b"\x8B\x58\xAB"                    # 52 mov ebx, [eax+std_vector_pa
            b"\x89\x59\xAB"                    # 55 mov [ecx+4], ebx
            b"\x83\xC0\xAB"                    # 58 add eax, 8
            b"\x83\xC1\xAB"                    # 61 add ecx, 8
            b"\x3B\xC2"                        # 64 cmp eax, edx
            b"\x75\xEC"                        # 66 jnz short loc_69BEC0
            b"\x89\x4E\xAB"                    # 68 mov [esi+std_vector_pair_ci
            b"\x8D\x44\x24\xAB"                # 71 lea eax, [esp+18h+value]
            b"\xBB\xAB\xAB\xAB\xAB"            # 75 mov ebx, 0Eh
            b"\x50"                            # 80 push eax
            b"\x8B\xCE"                        # 81 mov ecx, esi
            b"\xC7\x44\x24\xAB" + helpHex +    # 83 mov [esp+1Ch+value.first],
            b"\x89\x5C\x24\xAB"                # 91 mov [esp+1Ch+value.second],
            b"\xE8"                            # 95 call std_vector_TALKTYPE_in
        )
        valueFirstOffset = (86, 1, 0)
        valueOffset = (74, 1, 4)
        valueSecondOffet = (94, 1, 0)
        talkTypeTableOffset = (29, 4)
        talkTypeTableAllocOffset = (29, 4, True)
        talkTypeTableFirstOffset = (54, 1, False)
        talkTypeTableLastOffset = (70, 1, False)
        cmdIdOffset = (76, 4)
        insertOffset = 96
        clearOffset = 0
        stringAllocOffset = 0
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  push ebp
        # 1  mov ebp, esp
        # 3  sub esp, 18h
        # 6  push ebx
        # 7  push esi
        # 8  push edi
        # 9  push 0
        # 11 push 0
        # 13 push 0
        # 15 push 0
        # 17 mov ebx, ecx
        # 19 sub esp, 10h
        # 22 lea eax, [ebp+a3]
        # 25 mov ecx, esp
        # 27 mov [ebp+value.second], esp
        # 30 push eax
        # 31 push offset aInitemotionmsg
        # 36 call DebugFunc1
        # 41 mov ecx, dword_7F393C
        # 47 call DebugFunc2
        # 52 mov eax, [ebx+CSession.m_talkTypeTable._Last]
        # 58 mov ecx, [ebx+CSession.m_talkTypeTable._First]
        # 64 lea esi, [ebx+CSession.m_talkTypeTable]
        # 70 push eax
        # 71 push ecx
        # 72 mov ecx, esi
        # 74 call std_vector_TALKTYPE_clear
        # 79 mov edi, 0Eh
        # 84 mov [ebp+value.first], offset aHelp
        # 91 mov [ebp+value.second], edi
        # 94 mov edx, [esi+std_vector_pair_ci._Last]
        # 97 lea ecx, [ebp+value]
        # 100 push ecx
        # 101 push edx
        # 102 mov ecx, esi
        # 104 call std_vector_TALKTYPE_insert
        code = (
            b"\x55"                            # 0 push ebp
            b"\x8B\xEC"                        # 1 mov ebp, esp
            b"\x83\xEC\xAB"                    # 3 sub esp, 18h
            b"\x53"                            # 6 push ebx
            b"\x56"                            # 7 push esi
            b"\x57"                            # 8 push edi
            b"\x6A\x00"                        # 9 push 0
            b"\x6A\x00"                        # 11 push 0
            b"\x6A\x00"                        # 13 push 0
            b"\x6A\x00"                        # 15 push 0
            b"\x8B\xD9"                        # 17 mov ebx, ecx
            b"\x83\xEC\x10"                    # 19 sub esp, 10h
            b"\x8D\x45\xAB"                    # 22 lea eax, [ebp+a3]
            b"\x8B\xCC"                        # 25 mov ecx, esp
            b"\x89\x65\xAB"                    # 27 mov [ebp+value.second], esp
            b"\x50"                            # 30 push eax
            b"\x68\xAB\xAB\xAB\xAB"            # 31 push offset aInitemotionmsg
            b"\xE8\xAB\xAB\xAB\xAB"            # 36 call std_string_allocator
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 41 mov ecx, dword_7F393C
            b"\xE8\xAB\xAB\xAB\xAB"            # 47 call DebugFunc1
            b"\x8B\x83\xAB\xAB\xAB\xAB"        # 52 mov eax, [ebx+CSession.m_ta
            b"\x8B\x8B\xAB\xAB\xAB\xAB"        # 58 mov ecx, [ebx+CSession.m_ta
            b"\x8D\xB3\xAB\xAB\xAB\xAB"        # 64 lea esi, [ebx+CSession.m_ta
            b"\x50"                            # 70 push eax
            b"\x51"                            # 71 push ecx
            b"\x8B\xCE"                        # 72 mov ecx, esi
            b"\xE8\xAB\xAB\xAB\xAB"            # 74 call std_vector_TALKTYPE_cl
            b"\xBF\xAB\xAB\xAB\xAB"            # 79 mov edi, 0Eh
            b"\xC7\x45\xAB" + helpHex +        # 84 mov [ebp+value.first], offs
            b"\x89\x7D\xAB"                    # 91 mov [ebp+value.second], edi
            b"\x8B\x56\xAB"                    # 94 mov edx, [esi+std_vector_pa
            b"\x8D\x4D\xAB"                    # 97 lea ecx, [ebp+value]
            b"\x51"                            # 100 push ecx
            b"\x52"                            # 101 push edx
            b"\x8B\xCE"                        # 102 mov ecx, esi
            b"\xE8"                            # 104 call std_vector_TALKTYPE_i
        )
        valueFirstOffset = (86, 1, 0)
        valueOffset = (99, 1, 0)
        valueSecondOffet = (93, 1, 0)
        talkTypeTableOffset = (66, 4)
        talkTypeTableAllocOffset = (66, 4, True)
        talkTypeTableFirstOffset = (60, 4, True)
        talkTypeTableLastOffset = (96, 1, False)
        cmdIdOffset = (80, 4)
        insertOffset = 105
        clearOffset = 75
        stringAllocOffset = (37, True)
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  push ebp
        # 1  mov ebp, esp
        # 3  and esp, 0FFFFFFF8h
        # 6  push 0FFFFFFFFh
        # 8  push offset sub_72F679
        # 13 mov eax, large fs:0
        # 19 push eax
        # 20 sub esp, 0Ch
        # 23 push ebx
        # 24 push ebp
        # 25 push esi
        # 26 push edi
        # 27 mov eax, dword_7B2520
        # 32 xor eax, esp
        # 34 push eax
        # 35 lea eax, [esp+2Ch+var_C]
        # 39 mov large fs:0, eax
        # 45 mov edi, ecx
        # 47 push 0
        # 49 push 0
        # 51 push 0
        # 53 push 0
        # 55 sub esp, 1Ch
        # 58 mov ecx, esp
        # 60 mov [esp+58h+value.first], esp
        # 64 push offset aInitemotionmsg
        # 69 call std_string_allocator
        # 75 mov [esp+58h+var_4], 0
        # 83 mov [esp+58h+var_4], 0FFFFFFFFh
        # 91 mov ecx, dword_83BC38
        # 97 call sub_573420
        # 102 mov edx, [edi+CSession.m_talkTypeTable._First]
        # 108 mov ecx, [edi+CSession.m_talkTypeTable._Last]
        # 114 lea esi, [edi+CSession.m_talkTypeTable]
        # 120 cmp edx, ecx
        # 122 jz short loc_6E0039
        # 124 mov eax, ecx
        # 126 cmp ecx, ecx
        # 128 jz short loc_6E0036
        # 130 mov ebx, [eax+std_vector_pair_ci._Allocator]
        # 132 mov [edx], ebx
        # 134 mov ebx, [eax+std_vector_pair_ci._First]
        # 137 mov [edx+std_vector_pair_ci._First], ebx
        # 140 add eax, 8
        # 143 add edx, 8
        # 146 cmp eax, ecx
        # 148 jnz short loc_6E0022
        # 150 mov [esi+std_vector_pair_ci._Last], edx
        # 153 lea eax, [esp+2Ch+value]
        # 157 mov ebx, 0Eh
        # 162 push eax
        # 163 mov ecx, esi
        # 165 mov [esp+30h+value.first], offset aHelp
        # 173 mov [esp+30h+value.second], ebx
        # 177 call std_vector_TALKTYPE_insert
        code = (
            b"\x55"                            # 0 push ebp
            b"\x8B\xEC"                        # 1 mov ebp, esp
            b"\x83\xE4\xAB"                    # 3 and esp, 0FFFFFFF8h
            b"\x6A\xFF"                        # 6 push 0FFFFFFFFh
            b"\x68\xAB\xAB\xAB\xAB"            # 8 push offset sub_72F679
            b"\x64\xA1\x00\x00\x00\x00"        # 13 mov eax, large fs:0
            b"\x50"                            # 19 push eax
            b"\x83\xEC\x0C"                    # 20 sub esp, 0Ch
            b"\x53"                            # 23 push ebx
            b"\x55"                            # 24 push ebp
            b"\x56"                            # 25 push esi
            b"\x57"                            # 26 push edi
            b"\xA1\xAB\xAB\xAB\xAB"            # 27 mov eax, dword_7B2520
            b"\x33\xC4"                        # 32 xor eax, esp
            b"\x50"                            # 34 push eax
            b"\x8D\x44\x24\xAB"                # 35 lea eax, [esp+2Ch+var_C]
            b"\x64\xA3\x00\x00\x00\x00"        # 39 mov large fs:0, eax
            b"\x8B\xF9"                        # 45 mov edi, ecx
            b"\x6A\x00"                        # 47 push 0
            b"\x6A\x00"                        # 49 push 0
            b"\x6A\x00"                        # 51 push 0
            b"\x6A\x00"                        # 53 push 0
            b"\x83\xEC\x1C"                    # 55 sub esp, 1Ch
            b"\x8B\xCC"                        # 58 mov ecx, esp
            b"\x89\x64\x24\xAB"                # 60 mov [esp+58h+value.first],
            b"\x68\xAB\xAB\xAB\xAB"            # 64 push offset aInitemotionmsg
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 69 call std_string_allocator
            b"\xC7\x44\x24\xAB\x00\x00\x00\x00"  # 75 mov [esp+58h+var_4], 0
            b"\xC7\x44\x24\xAB\xFF\xFF\xFF\xFF"  # 83 mov [esp+58h+var_4], 0FFF
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 91 mov ecx, dword_83BC38
            b"\xE8\xAB\xAB\xAB\xAB"            # 97 call sub_573420
            b"\x8B\x97\xAB\xAB\xAB\xAB"        # 102 mov edx, [edi+CSession.m_t
            b"\x8B\x8F\xAB\xAB\xAB\xAB"        # 108 mov ecx, [edi+CSession.m_t
            b"\x8D\xB7\xAB\xAB\xAB\xAB"        # 114 lea esi, [edi+CSession.m_t
            b"\x3B\xD1"                        # 120 cmp edx, ecx
            b"\x74\x1D"                        # 122 jz short loc_6E0039
            b"\x8B\xC1"                        # 124 mov eax, ecx
            b"\x3B\xC9"                        # 126 cmp ecx, ecx
            b"\x74\x14"                        # 128 jz short loc_6E0036
            b"\x8B\xAB"                        # 130 mov ebx, [eax+std_vector_p
            b"\x89\x1A"                        # 132 mov [edx], ebx
            b"\x8B\x58\xAB"                    # 134 mov ebx, [eax+std_vector_p
            b"\x89\x5A\xAB"                    # 137 mov [edx+std_vector_pair_c
            b"\x83\xC0\xAB"                    # 140 add eax, 8
            b"\x83\xC2\xAB"                    # 143 add edx, 8
            b"\x3B\xC1"                        # 146 cmp eax, ecx
            b"\x75\xEC"                        # 148 jnz short loc_6E0022
            b"\x89\x56\xAB"                    # 150 mov [esi+std_vector_pair_c
            b"\x8D\x44\x24\xAB"                # 153 lea eax, [esp+2Ch+value]
            b"\xBB\xAB\xAB\xAB\xAB"            # 157 mov ebx, 0Eh
            b"\x50"                            # 162 push eax
            b"\x8B\xCE"                        # 163 mov ecx, esi
            b"\xC7\x44\x24\xAB" + helpHex +    # 165 mov [esp+30h+value.first
            b"\x89\x5C\x24\xAB"                # 173 mov [esp+30h+value.second]
            b"\xE8"                            # 177 call std_vector_TALKTYPE_i
        )
        valueFirstOffset = (168, 1, 0)
        valueOffset = (156, 1, 4)
        valueSecondOffet = (176, 1, 0)
        talkTypeTableOffset = (116, 4)
        talkTypeTableAllocOffset = (116, 4, True)
        talkTypeTableFirstOffset = (104, 4, True)
        talkTypeTableLastOffset = (152, 1, False)
        cmdIdOffset = (158, 4)
        insertOffset = 178
        clearOffset = 0
        stringAllocOffset = (71, False)
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  push ebp
        # 1  mov ebp, esp
        # 3  sub esp, 18h
        # 6  push ebx
        # 7  push esi
        # 8  push edi
        # 9  push offset _byte_7B654C
        # 14 push 0
        # 16 mov ebx, ecx
        # 18 sub esp, 10h
        # 21 lea eax, [ebp+var_1]
        # 24 mov ecx, esp
        # 26 mov [ebp+value.second], esp
        # 29 push eax
        # 30 push offset aInitemotionmsg
        # 35 call std_string_allocator
        # 40 mov ecx, dword_81C4C4
        # 46 push ecx
        # 47 call DebugFunc1
        # 52 mov eax, [ebx+CSession.m_talkTypeTable._Last]
        # 58 mov ecx, [ebx+CSession.m_talkTypeTable._First]
        # 64 lea esi, [ebx+CSession.m_talkTypeTable]
        # 70 add esp, 1Ch
        # 73 push eax
        # 74 push ecx
        # 75 mov ecx, esi
        # 77 call std_vector_TALKTYPE_clear
        # 82 mov edi, 0Eh
        # 87 mov [ebp+value.first], offset aHelp
        # 94 mov [ebp+value.second], edi
        # 97 mov eax, [esi+std_vector_pair_ci._Last]
        # 100 lea edx, [ebp+value]
        # 103 mov ecx, esi
        # 105 push edx
        # 106 push eax
        # 107 call std_vector_TALKTYPE_insert
        code = (
            b"\x55"                            # 0 push ebp
            b"\x8B\xEC"                        # 1 mov ebp, esp
            b"\x83\xEC\xAB"                    # 3 sub esp, 18h
            b"\x53"                            # 6 push ebx
            b"\x56"                            # 7 push esi
            b"\x57"                            # 8 push edi
            b"\x68\xAB\xAB\xAB\xAB"            # 9 push offset _byte_7B654C
            b"\x6A\x00"                        # 14 push 0
            b"\x8B\xD9"                        # 16 mov ebx, ecx
            b"\x83\xEC\x10"                    # 18 sub esp, 10h
            b"\x8D\x45\xAB"                    # 21 lea eax, [ebp+var_1]
            b"\x8B\xCC"                        # 24 mov ecx, esp
            b"\x89\x65\xAB"                    # 26 mov [ebp+value.second], esp
            b"\x50"                            # 29 push eax
            b"\x68\xAB\xAB\xAB\xAB"            # 30 push offset aInitemotionmsg
            b"\xE8\xAB\xAB\xAB\xAB"            # 35 call std_string_allocator
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 40 mov ecx, dword_81C4C4
            b"\x51"                            # 46 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"            # 47 call DebugFunc1
            b"\x8B\x83\xAB\xAB\xAB\xAB"        # 52 mov eax, [ebx+CSession.m_ta
            b"\x8B\x8B\xAB\xAB\xAB\xAB"        # 58 mov ecx, [ebx+CSession.m_ta
            b"\x8D\xB3\xAB\xAB\xAB\xAB"        # 64 lea esi, [ebx+CSession.m_ta
            b"\x83\xC4\x1C"                    # 70 add esp, 1Ch
            b"\x50"                            # 73 push eax
            b"\x51"                            # 74 push ecx
            b"\x8B\xCE"                        # 75 mov ecx, esi
            b"\xE8\xAB\xAB\xAB\xAB"            # 77 call std_vector_TALKTYPE_cl
            b"\xBF\xAB\xAB\xAB\xAB"            # 82 mov edi, 0Eh
            b"\xC7\x45\xAB" + helpHex +        # 87 mov [ebp+value.first], offs
            b"\x89\x7D\xAB"                    # 94 mov [ebp+value.second], edi
            b"\x8B\x46\xAB"                    # 97 mov eax, [esi+std_vector_pa
            b"\x8D\x55\xAB"                    # 100 lea edx, [ebp+value]
            b"\x8B\xCE"                        # 103 mov ecx, esi
            b"\x52"                            # 105 push edx
            b"\x50"                            # 106 push eax
            b"\xE8"                            # 107 call std_vector_TALKTYPE_i
        )
        valueFirstOffset = (89, 1, 0)
        valueOffset = (102, 1, 0)
        valueSecondOffet = (96, 1, 0)
        talkTypeTableOffset = (66, 4)
        talkTypeTableAllocOffset = (66, 4, True)
        talkTypeTableFirstOffset = (60, 4, True)
        talkTypeTableLastOffset = (99, 1, False)
        cmdIdOffset = (83, 4)
        insertOffset = 108
        clearOffset = 78
        stringAllocOffset = (36, True)
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  push ebp
        # 1  mov ebp, esp
        # 3  sub esp, 8
        # 6  push esi
        # 7  push edi
        # 8  mov edi, ecx
        # 10 mov [ebp+value.first], offset aHelp
        # 17 mov eax, [edi+CSession.m_talkTypeTable._First]
        # 23 lea esi, [edi+CSession.m_talkTypeTable._First]
        # 29 mov [esi+std_vector._Last], eax
        # 32 mov [ebp+value.second], 0Eh
        # 39 cmp [esi+std_vector._End], eax
        # 42 jz short loc_A7D44F
        # 44 mov [eax+std_pair_.first], offset aHelp
        # 50 mov [eax+std_pair_.second], 0Eh
        # 57 add [esi+std_vector._Last], 8
        # 61 jmp short loc_A7D45B
        # 63 lea ecx, [ebp+value.first]
        # 66 push ecx
        # 67 push eax
        # 68 mov ecx, esi
        # 70 call std_vector_TALKTYPE_insert
        code = (
            b"\x55"                            # 0 push ebp
            b"\x8B\xEC"                        # 1 mov ebp, esp
            b"\x83\xEC\xAB"                    # 3 sub esp, 8
            b"\x56"                            # 6 push esi
            b"\x57"                            # 7 push edi
            b"\x8B\xF9"                        # 8 mov edi, ecx
            b"\xC7\x45\xF8" + helpHex +        # 10 mov [ebp+value.first], offs
            b"\x8B\x87\xAB\xAB\xAB\xAB"        # 17 mov eax, [edi+CSession.m_ta
            b"\x8D\xB7\xAB\xAB\xAB\xAB"        # 23 lea esi, [edi+CSession.m_ta
            b"\x89\x46\xAB"                    # 29 mov [esi+std_vector._Last],
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"    # 32 mov [ebp+value.second], 0Eh
            b"\x39\x46\xAB"                    # 39 cmp [esi+std_vector._End],
            b"\x74\x13"                        # 42 jz short loc_A7D44F
            b"\xC7\x00" + helpHex +            # 44 mov [eax+std_pair_.first],
            b"\xC7\x40\xAB\xAB\xAB\xAB\xAB"    # 50 mov [eax+std_pair_.second],
            b"\x83\x46\xAB\x08"                # 57 add [esi+std_vector._Last],
            b"\xEB\x0C"                        # 61 jmp short loc_A7D45B
            b"\x8D\x4D\xAB"                    # 63 lea ecx, [ebp+value]
            b"\x51"                            # 66 push ecx
            b"\x50"                            # 67 push eax
            b"\x8B\xCE"                        # 68 mov ecx, esi
            b"\xE8"                            # 70 call std_vector_TALKTYPE_in
        )
        valueFirstOffset = (12, 1, 0)
        valueOffset = (65, 1, 0)
        valueSecondOffet = (34, 1, 0)
        talkTypeTableOffset = (25, 4)
        talkTypeTableAllocOffset = -1
        talkTypeTableFirstOffset = (25, 4, True)
        talkTypeTableLastOffset = (31, 1, False)
        cmdIdOffset = (53, 4)
        insertOffset = 71
        clearOffset = 0
        stringAllocOffset = 0
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        self.log("failed in search CSession::InitTalkTypeTable.")
        # can works on older version, but vector struct unknown
        if self.packetVersion < "20090000":
            return
        exit(1)

    if valueOffset != 0:
        value = self.getVarAddr(offset, valueOffset) + valueOffset[2]
    else:
        value = 0
    valueFirst = self.getVarAddr(offset,
                                 valueFirstOffset) + valueFirstOffset[2]
    valueSecond = self.getVarAddr(offset,
                                  valueSecondOffet) + valueSecondOffet[2]
    if valueFirst != value:
        self.log("Error: found wrong pair.first offset: {0}".
                 format(valueFirst))
        exit(1)
    if valueFirst + 4 != valueSecond:
        self.log("Error: found wrong pair.first or pair.second: {0}, {1}".
                 format(valueFirst, valueSecond))
        exit(1)

    talkTypeTable = self.getVarAddr(offset, talkTypeTableOffset)
    if talkTypeTableAllocOffset == -1:
        talkTypeTableAlloc = -1
    else:
        talkTypeTableAlloc = self.getVarAddr(offset, talkTypeTableAllocOffset)
        if talkTypeTableAlloc < 0:
            self.log("Error: Found wrong _Alloc offset in std::vector: {0}".
                     format(talkTypeTableAlloc))
            exit(1)
        if talkTypeTableAllocOffset[2] is True:
            talkTypeTableAlloc = talkTypeTableAlloc - talkTypeTable
    talkTypeTableFirst = self.getVarAddr(offset, talkTypeTableFirstOffset)
    if talkTypeTableFirstOffset[2] is True:
        talkTypeTableFirst = talkTypeTableFirst - talkTypeTable
    talkTypeTableLast = self.getVarAddr(offset, talkTypeTableLastOffset)
    if talkTypeTableLastOffset[2] is True:
        talkTypeTableLast = talkTypeTableLast - talkTypeTable
    if talkTypeTableFirst < 0:
        self.log("Error: Found wrong _First offset in std::vector: {0}".
                 format(talkTypeTableFirst))
        exit(1)
    if talkTypeTableLast < 0 or talkTypeTableFirst + 4 != talkTypeTableLast:
        self.log("Error: Found wrong _Last offset in std::vector: {0}".
                 format(talkTypeTableLast))
        exit(1)
    talkTypeTableEnd = talkTypeTableLast + 4

    self.CSession_InitTalkTypeTable = offset

    if insertOffset != 0:
        self.std_vector_TALKTYPE_insert = self.getAddr(offset,
                                                       insertOffset,
                                                       insertOffset + 4)
        self.addRawFunc("std_vector_TALKTYPE_insert",
                        self.std_vector_TALKTYPE_insert)
    if clearOffset != 0:
        self.std_vector_TALKTYPE_clear = self.getAddr(offset,
                                                      clearOffset,
                                                      clearOffset + 4)
        self.addRawFunc("std_vector_TALKTYPE_clear",
                        self.std_vector_TALKTYPE_clear)
    if stringAllocOffset != 0:
        if stringAllocOffset[1] is True:
            self.std_string_allocator = self.getAddr(offset,
                                                     stringAllocOffset[0],
                                                     stringAllocOffset[0] + 4)
            self.addRawFunc("std_string_allocator",
                            self.std_string_allocator)
        else:
            self.std_string_allocator = self.exe.readUInt(offset +
                                                          stringAllocOffset[0])
            self.addVaFunc("std_string_allocator",
                           self.std_string_allocator)

    cmdId = self.getVarAddr(offset, cmdIdOffset)

    self.addStruct("pair_char_ptr_int")
    self.addStructMember("first", 0, 4, False)
    self.setStructMemberType(0, "char*")
    self.addStructMember("second", 4, 4, False)

    self.addStruct("std_vector_pair_ci")
    if talkTypeTableAlloc != -1:
        self.addStructMember("_Allocator", talkTypeTableAlloc, 4, True)
    self.addStructMember("_First", talkTypeTableFirst, 4, True)
    self.setStructMemberType(talkTypeTableFirst, "pair_char_ptr_int*")
    self.addStructMember("_Last", talkTypeTableLast, 4, True)
    self.setStructMemberType(talkTypeTableLast, "pair_char_ptr_int*")
    self.addStructMember("_End", talkTypeTableEnd, 4, True)
    self.setStructMemberType(talkTypeTableEnd, "pair_char_ptr_int*")

    self.vectorStruct = (talkTypeTableAlloc,
                         talkTypeTableFirst,
                         talkTypeTableLast,
                         talkTypeTableEnd)

    self.CSession_talkTypeTable = talkTypeTable
    self.addStruct("CSession")
    self.addStructMember("m_talkTypeTable", talkTypeTable, 12, True)
    self.setStructMemberType(talkTypeTable, "std_vector_pair_ci")

    self.addRawFuncType("CSession::InitTalkTypeTable",
                        self.CSession_InitTalkTypeTable,
                        "void __thiscall CSession_InitTalkTypeTable("
                        "CSession *this)")

    self.commands.append(("/help", cmdId))
