#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchCCurlMgr(self):
    if self.isVarPresent("CCURLMgr_vptr") is False:
        self.log("CCURLMgr_vptr not found")
        return
    offset, section = self.exe.string(b"libcurl.dll")
    if offset is False:
        self.log("Error: libcurl.dll not found")
        return
    curlDllName = self.exe.toHex(section.rawToVa(offset), 4)
    curlVptr = self.exe.toHex(self.getAddrByName("CCURLMgr_vptr"), 4)
    mode = 0

    # search in CCURLMgr::init

    # 0  mov large fs:0, eax
    # 6  cmp g_CCURLMgr, 0
    # 13 jnz loc_4A804A
    # 19 push offset _dword_10BE590
    # 24 push 38h
    # 26 call malloc3
    # 31 mov edi, eax
    # 33 add esp, 8
    # 36 mov [ebp+var_10], edi
    # 39 mov [ebp+var_4], 0
    # 46 test edi, edi
    # 48 jz loc_4A801F
    # 54 push offset LibFileName
    # 59 mov dword ptr [edi], offset CCURLMgr_vptr
    # 65 mov [edi+CCURLMgr.handle_LibCurl], 0
    # 72 mov [edi+CCURLMgr.isInit], 0
    # 76 call ds:LoadLibraryA
    # 82 mov [edi+CCURLMgr.handle_LibCurl], eax
    code = (
        b"\x64\xA3\x00\x00\x00\x00"        # 0 mov large fs:0, eax
        b"\x83\x3D\xAB\xAB\xAB\xAB\x00"    # 6 cmp g_CCURLMgr, 0
        b"\x0F\x85\xAB\xAB\xAB\x00"        # 13 jnz loc_4A804A
        b"\x68\xAB\xAB\xAB\xAB"            # 19 push offset _dword_10BE590
        b"\x6A\x38"                        # 24 push 38h
        b"\xE8\xAB\xAB\xAB\xAB"            # 26 call malloc3
        b"\x8B\xF8"                        # 31 mov edi, eax
        b"\x83\xC4\x08"                    # 33 add esp, 8
        b"\x89\x7D\xAB"                    # 36 mov [ebp+var_10], edi
        b"\xC7\x45\xAB\x00\x00\x00\x00"    # 39 mov [ebp+var_4], 0
        b"\x85\xFF"                        # 46 test edi, edi
        b"\x0F\x84\xAB\xAB\xAB\x00"        # 48 jz loc_4A801F
        b"\x68" + curlDllName +            # 54 push offset LibFileName
        b"\xC7\x07" + curlVptr +           # 59 mov dword ptr [edi], offset CCU
        b"\xC7\x47\xAB\x00\x00\x00\x00"    # 65 mov [edi+CCURLMgr.handle_LibCur
        b"\xC6\x47\xAB\x00"                # 72 mov [edi+CCURLMgr.isInit], 0
        b"\xFF\x15\xAB\xAB\xAB\xAB"        # 76 call ds:LoadLibraryA
        b"\x89\x47"                        # 82 mov [edi+CCURLMgr.handle_LibCur
    )
    curlMgrOffset = 8
    handleOffset1 = (67, 1)
    isInitOffset = (74, 1)
    isInitOffset2 = 0
    loadLibraryAOffset = (78, True)
    handleOffset2 = (84, 1)
    CCURLMgrCtorOffset = 0
    objSizeOffset = (25, 1)
    offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 0  mov large fs:0, eax
        # 6  cmp g_CCURLMgr, 0
        # 13 jnz loc_4A804A
        # 19 push offset _dword_10BE590
        # 24 push 38h
        # 26 call malloc3
        # 31 mov edi, eax
        # 33 add esp, 8
        # 36 mov [ebp+var_10], edi
        # 39 mov [ebp+var_4], 0
        # 46 test edi, edi
        # 48 jz loc_4A801F
        # 54 push offset LibFileName
        # 59 mov dword ptr [edi], offset CCURLMgr_vptr
        # 65 mov [edi+CCURLMgr.handle_LibCurl], 0
        # 72 mov [edi+CCURLMgr.isInit], 0
        # 76 call ds:LoadLibraryA
        # 82 mov [edi+CCURLMgr.handle_LibCurl], eax
        code = (
            b"\x64\xA3\x00\x00\x00\x00"        # 0 mov large fs:0, eax
            b"\x83\x3D\xAB\xAB\xAB\xAB\x00"    # 6 cmp g_CCURLMgr, 0
            b"\x0F\x85\xAB\xAB\xAB\x00"        # 13 jnz loc_4A804A
            b"\x68\xAB\xAB\xAB\xAB"            # 19 push offset _dword_10BE590
            b"\x6A\x34"                        # 24 push 34h
            b"\xE8\xAB\xAB\xAB\xAB"            # 26 call malloc3
            b"\x8B\xF8"                        # 31 mov edi, eax
            b"\x83\xC4\x08"                    # 33 add esp, 8
            b"\x89\x7D\xAB"                    # 36 mov [ebp+var_10], edi
            b"\xC7\x45\xAB\x00\x00\x00\x00"    # 39 mov [ebp+var_4], 0
            b"\x85\xFF"                        # 46 test edi, edi
            b"\x0F\x84\xAB\xAB\xAB\x00"        # 48 jz loc_4A801F
            b"\x68" + curlDllName +            # 54 push offset LibFileName
            b"\xC7\x07" + curlVptr +           # 59 mov dword ptr [edi], offset
            b"\xC7\x47\xAB\x00\x00\x00\x00"    # 65 mov [edi+CCURLMgr.handle_Li
            b"\xC6\x47\xAB\x00"                # 72 mov [edi+CCURLMgr.isInit],
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 76 call ds:LoadLibraryA
            b"\x89\x47"                        # 82 mov [edi+CCURLMgr.handle_Li
        )
        curlMgrOffset = 8
        handleOffset1 = (67, 1)
        isInitOffset = (74, 1)
        isInitOffset2 = 0
        loadLibraryAOffset = (78, True)
        handleOffset2 = (84, 1)
        CCURLMgrCtorOffset = 0
        objSizeOffset = (25, 1)
        offset = self.exe.codeWildcard(code, b"\xAB")

    # search if CCURLMgr::CCURLMgr not inlined
    if offset is False:
        mode = 1
        # 0  mov large fs:0, eax
        # 6  cmp g_CCURLMgr, 0
        # 13 jnz short loc_4650C9
        # 15 push offset dword_1034CF8
        # 20 push 34h
        # 22 call malloc2
        # 27 mov esi, eax
        # 29 add esp, 8
        # 32 mov [ebp+var_10], esi
        # 35 mov [ebp+var_4], 0
        # 42 test esi, esi
        # 44 jz short loc_46509E
        # 46 mov ecx, esi
        # 48 mov [esi+CCURLMgr.vptr], offset CCURLMgr_vptr
        # 54 mov [esi+CCURLMgr.handle_LibCurl], 0
        # 61 mov byte ptr [esi+CCURLMgr.isInit], 0
        # 65 call CCURLMgr_CCURLMgr
        # 70 test al, al
        # 72 jz short loc_4650A0
        # 74 mov byte ptr [esi+CCURLMgr.isInit], 1
        # 78 jmp short loc_4650A0
        # 80 xor esi, esi
        code = (
            b"\x64\xA3\x00\x00\x00\x00"        # 0 mov large fs:0, eax
            b"\x83\x3D\xAB\xAB\xAB\xAB\x00"    # 6 cmp g_CCURLMgr, 0
            b"\x75\xAB"                        # 13 jnz short loc_4650C9
            b"\x68\xAB\xAB\xAB\xAB"            # 15 push offset dword_1034CF8
            b"\x6A\x34"                        # 20 push 34h
            b"\xE8\xAB\xAB\xAB\xAB"            # 22 call malloc2
            b"\x8B\xF0"                        # 27 mov esi, eax
            b"\x83\xC4\x08"                    # 29 add esp, 8
            b"\x89\x75\xAB"                    # 32 mov [ebp+var_10], esi
            b"\xC7\x45\xAB\x00\x00\x00\x00"    # 35 mov [ebp+var_4], 0
            b"\x85\xF6"                        # 42 test esi, esi
            b"\x74\x22"                        # 44 jz short loc_46509E
            b"\x8B\xCE"                        # 46 mov ecx, esi
            b"\xC7\x06" + curlVptr +           # 48 mov [esi+CCURLMgr.vptr], of
            b"\xC7\x46\xAB\x00\x00\x00\x00"    # 54 mov [esi+CCURLMgr.handle_Li
            b"\xC6\x46\xAB\x00"                # 61 mov byte ptr [esi+CCURLMgr.
            b"\xE8\xAB\xAB\xAB\xAB"            # 65 call CCURLMgr_CCURLMgr
            b"\x84\xC0"                        # 70 test al, al
            b"\x74\x08"                        # 72 jz short loc_4650A0
            b"\xC6\x46\xAB\x01"                # 74 mov byte ptr [esi+CCURLMgr.
            b"\xEB\x02"                        # 78 jmp short loc_4650A0
            b"\x33\xF6"                        # 80 xor esi, esi
        )
        curlMgrOffset = 8
        handleOffset1 = (56, 1)
        isInitOffset = (63, 1)
        isInitOffset2 = (76, 1)
        loadLibraryAOffset = 0
        handleOffset2 = 0
        CCURLMgrCtorOffset = 66
        objSizeOffset = (21, 1)
        offset = self.exe.codeWildcard(code, b"\xAB")

        if offset is False:
            # 0  mov large fs:0, eax
            # 6  cmp g_CCURLMgr, 0
            # 13 jnz short loc_4650C9
            # 15 push offset dword_1034CF8
            # 20 push 38h
            # 22 call malloc2
            # 27 mov esi, eax
            # 29 add esp, 8
            # 32 mov [ebp+var_10], esi
            # 35 mov [ebp+var_4], 0
            # 42 test esi, esi
            # 44 jz short loc_46509E
            # 46 mov ecx, esi
            # 48 mov [esi+CCURLMgr.vptr], offset CCURLMgr_vptr
            # 54 mov [esi+CCURLMgr.handle_LibCurl], 0
            # 61 mov byte ptr [esi+CCURLMgr.isInit], 0
            # 65 call CCURLMgr_CCURLMgr
            # 70 test al, al
            # 72 jz short loc_4650A0
            # 74 mov byte ptr [esi+CCURLMgr.isInit], 1
            # 78 jmp short loc_4650A0
            # 80 xor esi, esi
            code = (
                b"\x64\xA3\x00\x00\x00\x00"        # 0 mov large fs:0, eax
                b"\x83\x3D\xAB\xAB\xAB\xAB\x00"    # 6 cmp g_CCURLMgr, 0
                b"\x75\xAB"                        # 13 jnz short loc_4650C9
                b"\x68\xAB\xAB\xAB\xAB"            # 15 push offset dword_1034C
                b"\x6A\x38"                        # 20 push 38h
                b"\xE8\xAB\xAB\xAB\xAB"            # 22 call malloc2
                b"\x8B\xF0"                        # 27 mov esi, eax
                b"\x83\xC4\x08"                    # 29 add esp, 8
                b"\x89\x75\xAB"                    # 32 mov [ebp+var_10], esi
                b"\xC7\x45\xAB\x00\x00\x00\x00"    # 35 mov [ebp+var_4], 0
                b"\x85\xF6"                        # 42 test esi, esi
                b"\x74\x22"                        # 44 jz short loc_46509E
                b"\x8B\xCE"                        # 46 mov ecx, esi
                b"\xC7\x06" + curlVptr +           # 48 mov [esi+CCURLMgr.vptr]
                b"\xC7\x46\xAB\x00\x00\x00\x00"    # 54 mov [esi+CCURLMgr.handl
                b"\xC6\x46\xAB\x00"                # 61 mov byte ptr [esi+CCURL
                b"\xE8\xAB\xAB\xAB\xAB"            # 65 call CCURLMgr_CCURLMgr
                b"\x84\xC0"                        # 70 test al, al
                b"\x74\x08"                        # 72 jz short loc_4650A0
                b"\xC6\x46\xAB\x01"                # 74 mov byte ptr [esi+CCURL
                b"\xEB\x02"                        # 78 jmp short loc_4650A0
                b"\x33\xF6"                        # 80 xor esi, esi
            )
            curlMgrOffset = 8
            handleOffset1 = (56, 1)
            isInitOffset = (63, 1)
            isInitOffset2 = (76, 1)
            loadLibraryAOffset = 0
            handleOffset2 = 0
            CCURLMgrCtorOffset = 66
            objSizeOffset = (21, 1)
            offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        self.log("failed in search CCURLMgr.")
        exit(1)

    offset1 = offset
    self.CCURLMgrInitBlock = offset
    self.CCURLMgrInitBlockVa = self.exe.rawToVa(offset)

    self.g_CCURLMgr = self.exe.readUInt(offset + curlMgrOffset)
    self.addVaVar("g_CCURLMgr", self.g_CCURLMgr)

    handle1 = self.getVarAddr(offset, handleOffset1)
    if handleOffset2 != 0:
        handle2 = self.getVarAddr(offset, handleOffset2)
        if handle1 != handle2:
            self.log("Error: found two different curl handle offset")
            exit(1)
    self.CCURLMgr_handle = handle1

    isInit = self.getVarAddr(offset, isInitOffset)
    if isInitOffset2 != 0:
        isInit2 = self.getVarAddr(offset, isInitOffset2)
        if isInit != isInit2:
            self.log("Error: found two different curl isInit offset")
            exit(1)
    if isInit + 4 != handle1:
        self.log("Error: found wrong isInit and handle curl offsets: "
                 "{0}, {1}".format(isInit, handle1))
        exit(1)

    if loadLibraryAOffset != 0:
        if loadLibraryAOffset[1] is True:
            self.loadLibraryA = self.exe.readUInt(offset +
                                                  loadLibraryAOffset[0])
            self.addVaFunc("LoadLibraryA", self.loadLibraryA)
        else:
            self.log("Error: unknown load library flag")
            exit(1)

    if CCURLMgrCtorOffset != 0:
        self.CCURLMgr_CCURLMgr = self.getAddr(offset,
                                              CCURLMgrCtorOffset,
                                              CCURLMgrCtorOffset + 4)
        self.addRawFunc("CCURLMgr::CCURLMgr", self.CCURLMgr_CCURLMgr)

    objSize = 0
    if objSizeOffset != 0:
        objSize = self.getVarAddr(offset, objSizeOffset)
        if objSize < 4:
            self.log("Error: wrong size detected for object CCURLMgr: "
                     "{0}".format(objSize))
            exit(1)
        if objSize < handle1:
            self.log("Error: padding smaller than m_libCurl: "
                     "{0} vs {1}".format(objSize, handle1))
            exit(1)

    handleOffset3 = 0
    # additional validation for CCURLMgr::CCURLMgr and search inside it
    if mode == 1:
        # 0  push edi
        # 1  push offset LibFileName
        # 6  mov edi, ecx
        # 8  call ds:LoadLibraryA
        # 14 mov [edi+CCURLMgr.handle_LibCurl], eax
        # 17 test eax, eax
        # 19 jnz short loc_464F99
        # 21 xor al, al
        # 23 pop edi
        # 24 ret retn
        code = (
            b"\x57"                            # 0 push edi
            b"\x68" + curlDllName +            # 1 push offset LibFileName
            b"\x8B\xF9"                        # 6 mov edi, ecx
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 8 call ds:LoadLibraryA
            b"\x89\x47\xAB"                    # 14 mov [edi+CCURLMgr.handle_Li
            b"\x85\xC0"                        # 17 test eax, eax
            b"\x75\x04"                        # 19 jnz short loc_464F99
            b"\x32\xC0"                        # 21 xor al, al
            b"\x5F"                            # 23 pop edi
            b"\xC3"                            # 24 ret retn
        )
        loadLibraryAOffset = (10, True)
        handleOffset3 = (16, 1)
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.CCURLMgr_CCURLMgr,
                                       self.CCURLMgr_CCURLMgr + 0x20)

        if offset is False:
            self.log("failed in search CCURLMgr second block.")
            exit(1)

        if loadLibraryAOffset != 0:
            if loadLibraryAOffset[1] is True:
                self.loadLibraryA = self.exe.readUInt(offset +
                                                      loadLibraryAOffset[0])
                self.addVaFunc("LoadLibraryA", self.loadLibraryA)
            else:
                self.log("Error: unknown load library flag 2")
                exit(1)

    if handleOffset3 != 0:
        handle3 = self.getVarAddr(offset, handleOffset3)
        if handle1 != handle3:
            self.log("Error: found two different curl handle offset 2")
            exit(1)

    self.addStruct("CCURLMgr")
    self.addStructMember("m_libCurl", handle1, 4, True)
    self.addStructMember("m_isInit", isInit, 4, True)
    if objSize != 0 and objSize - 4 != handle1:
        self.addStructMember("padding_end", objSize - 4, 4, True)


    # search in same function some function names

    offset, section = self.exe.string(b"curl_easy_init")
    if offset is False:
        self.log("Error: curl_easy_init string not found")
        exit(1)
    curl_easy_init = self.exe.toHex(section.rawToVa(offset), 4)

    offset, section = self.exe.string(b"curl_easy_setopt")
    if offset is False:
        self.log("Error: curl_easy_setopt string not found")
        exit(1)
    curl_easy_setopt = self.exe.toHex(section.rawToVa(offset), 4)

    handleHex = self.exe.toHexB(self.CCURLMgr_handle)

    # 0  push offset aCurl_easy_init
    # 5  push [edi+CCURLMgr.handle_LibCurl]
    # 8  mov [edi+CCURLMgr.curl_formadd], eax
    # 11 call esi
    # 13 push offset aCurl_easy_seto
    # 18 push [edi+CCURLMgr.handle_LibCurl]
    # 21 mov [edi+CCURLMgr.curl_easy_init], eax
    # 24 call esi
    code = (
        b"\x68" + curl_easy_init +         # 0 push offset aCurl_easy_init
        b"\xFF\x77" + handleHex +          # 5 push [edi+CCURLMgr.handle_LibCur
        b"\x89\x47\xAB"                    # 8 mov [edi+CCURLMgr.curl_formadd],
        b"\xFF\xD6"                        # 11 call esi
        b"\x68" + curl_easy_setopt +       # 13 push offset aCurl_easy_seto
        b"\xFF\x77" + handleHex +          # 18 push [edi+CCURLMgr.handle_LibCu
        b"\x89\x47\xAB"                    # 21 mov [edi+CCURLMgr.curl_easy_ini
        b"\xFF\xD6"                        # 24 call esi
    )
    initOffset = (23, 1)

    if mode == 1:
        funcAddr1 = self.CCURLMgr_CCURLMgr
        funcAddr2 = self.CCURLMgr_CCURLMgr + 0x90
    else:
        funcAddr1 = offset1 + 0x20
        funcAddr2 = offset1 + 0xf0
    offset = self.exe.codeWildcard(code,
                                   b"\xAB",
                                   funcAddr1,
                                   funcAddr2)

    if offset is False:
        self.log("Error: curl_easy_setopt not found")
        exit(1)

    self.CCURLMgr_curl_init = self.getVarAddr(offset, initOffset)
    self.addStruct("CCURLMgr")
    self.addStructMember("m_curl_easy_init", self.CCURLMgr_curl_init, 4, True)

    # add other CCURLMgr fields from same function
