#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchPacket73(self):
    # search in recv_packet_73

    if self.session == 0:
        self.log("Error: in packet73. session not defined")
        exit(1)
    if self.g_modeMgr == 0:
        self.log("Error: in packet73. g_modeMgr not defined")
        exit(1)
    offset, section = self.exe.string(b"%s.rsw")
    if offset is False:
        self.log("failed in search '%s.rsw'")
        exit(1)
    rswStr = section.rawToVa(offset)

    offset, section = self.exe.string(b"PingLog.txt")
    if offset is False:
        self.log("failed in search 'PingLog.txt'")
        exit(1)
    pingLog = section.rawToVa(offset)

    sessionHex = self.exe.toHex(self.session, 4)
    rswStrHex = self.exe.toHex(rswStr, 4)
    pingLogHex = self.exe.toHex(pingLog, 4)
    modeMgrHex = self.exe.toHex(self.g_modeMgr, 4)

    # 2016-2018
    # 0  mov ecx, offset g_session
    # 5  push [edi+struct_packet_73.start_time]
    # 8  call CSession_SetServerTime
    # 13 movzx edx, [edi+struct_packet_73.pos2]
    # 17 movzx esi, [edi+struct_packet_73.pos1]
    # 21 mov eax, edx
    # 23 and eax, 0Fh
    # 26 push eax
    # 27 mov eax, esi
    # 29 and eax, 3Fh
    # 32 shl eax, 4
    # 35 shr edx, 4
    # 38 or eax, edx
    # 40 push eax
    # 41 movzx eax, [edi+struct_packet_73.pos0]
    # 45 shl eax, 2
    # 48 shr esi, 6
    # 51 or esi, eax
    # 53 push esi
    # 54 mov ecx, offset g_session
    # 59 call CSession_SetPlayerPosDir
    # 64 lea eax, [ebp+modeName]
    # 67 push offset g_sessionm_curMap
    # 72 push offset aS_rsw
    # 77 push eax
    # 78 call sprintf
    # 84 add esp, 0Ch
    # 87 lea eax, [ebp+modeName]
    # 90 push eax
    # 91 push 1
    # 93 mov ecx, offset g_modeMgr
    # 98 call CModeMgr_Switch
    # 103 push 0
    # 105 call CRagConnection_instanceR
    # 110 mov ecx, eax
    # 112 call CConnection_SetBlock
    # 117 cmp isDebug, 0
    # 124 jz loc_9FBF42
    # 130 push offset _dword_CCF9F4
    # 135 push offset aPinglog_txt
    # 140 call fopen
    code = (
        b"\xB9" + sessionHex +             # 0
        b"\xFF\x77\xAB"                    # 5
        b"\xE8\xAB\xAB\xAB\xAB"            # 8
        b"\x0F\xB6\x57\xAB"                # 13
        b"\x0F\xB6\x77\xAB"                # 17
        b"\x8B\xC2"                        # 21
        b"\x83\xE0\x0F"                    # 23
        b"\x50"                            # 26
        b"\x8B\xC6"                        # 27
        b"\x83\xE0\x3F"                    # 29
        b"\xC1\xE0\x04"                    # 32
        b"\xC1\xEA\x04"                    # 35
        b"\x0B\xC2"                        # 38
        b"\x50"                            # 40
        b"\x0F\xB6\x47\xAB"                # 41
        b"\xC1\xE0\x02"                    # 45
        b"\xC1\xEE\x06"                    # 48
        b"\x0B\xF0"                        # 51
        b"\x56"                            # 53
        b"\xB9" + sessionHex +             # 54
        b"\xE8\xAB\xAB\xAB\xAB"            # 59
        b"\x8D\x45\xAB"                    # 64
        b"\x68\xAB\xAB\xAB\xAB"            # 67
        b"\x68" + rswStrHex +              # 72
        b"\x50"                            # 77
        b"\xFF\x15\xAB\xAB\xAB\xAB"        # 78
        b"\x83\xC4\x0C"                    # 84
        b"\x8D\x45\xAB"                    # 87
        b"\x50"                            # 90
        b"\x6A\x01"                        # 91
        b"\xB9" + modeMgrHex +             # 93
        b"\xE8\xAB\xAB\xAB\xAB"            # 98
        b"\x6A\x00"                        # 103
        b"\xE8\xAB\xAB\xAB\xAB"            # 105
        b"\x8B\xC8"                        # 110
        b"\xE8\xAB\xAB\xAB\xAB"            # 112
        b"\x80\x3D\xAB\xAB\xAB\xAB\x00"    # 117
        b"\x0F\x84\xAB\xAB\xAB\x00"        # 124
        b"\x68\xAB\xAB\xAB\xAB"            # 130
        b"\x68" + pingLogHex +             # 135
        b"\xFF\x15"                        # 140
    )
    startTimeOffset = (7, 1)
    pos0Offset = (44, 1)
    pos1Offset = (20, 1)
    pos2Offset = (16, 1)
    setServerTimeOffset = 9
    setPlayerPosDirOffset = 60
    map_nameOffset = 68
    sprintfOffset = (80, True)
    modeMgrSwitchOffset = 99
    instanceOffset = 106
    g_instanceOffset = 0
    setBlockOffset = 113
    isDebugOffset = 119
    fopenOffset = (142, True)
    offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2019-02-13
        # 0  push dword ptr packet_buf+2
        # 6  mov ecx, offset g_session
        # 11 call CSession_SetServerTime
        # 16 movzx ecx, packet_buf+8
        # 23 movzx edx, packet_buf+7
        # 30 mov eax, ecx
        # 32 and eax, 0Fh
        # 35 shr ecx, 4
        # 38 push eax
        # 39 mov eax, edx
        # 41 shr edx, 6
        # 44 and eax, 3Fh
        # 47 shl eax, 4
        # 50 or eax, ecx
        # 52 mov ecx, offset g_session
        # 57 push eax
        # 58 movzx eax, packet_buf+6
        # 65 shl eax, 2
        # 68 or eax, edx
        # 70 push eax
        # 71 call CSession_SetPlayerPosDir
        # 76 push offset g_session.m_curMap
        # 81 lea eax, [ebp+modeName]
        # 87 push offset aS_rsw
        # 92 push eax
        # 93 call sprintf
        # 98 add esp, 0Ch
        # 101 lea eax, [ebp+modeName]
        # 107 mov ecx, offset g_modeMgr
        # 112 push eax
        # 113 push 1
        # 115 call CModeMgr_Switch
        # 120 push 0
        # 122 call CRagConnection_instanceR
        # 127 mov ecx, eax
        # 129 call CConnection_SetBlock
        # 134 cmp isDebugLog, 0
        # 141 jz loc_9D836C
        # 147 push offset aAt
        # 152 push offset aPinglog_txt
        # 157 call ds:fopen
        code = (
            b"\xFF\x35\xAB\xAB\xAB\xAB"        # 0 push dword ptr packet_buf+2
            b"\xB9" + sessionHex +             # 6 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"            # 11 call CSession_SetServerTime
            b"\x0F\xB6\x0D\xAB\xAB\xAB\xAB"    # 16 movzx ecx, packet_buf+8
            b"\x0F\xB6\x15\xAB\xAB\xAB\xAB"    # 23 movzx edx, packet_buf+7
            b"\x8B\xC1"                        # 30 mov eax, ecx
            b"\x83\xE0\x0F"                    # 32 and eax, 0Fh
            b"\xC1\xE9\x04"                    # 35 shr ecx, 4
            b"\x50"                            # 38 push eax
            b"\x8B\xC2"                        # 39 mov eax, edx
            b"\xC1\xEA\x06"                    # 41 shr edx, 6
            b"\x83\xE0\x3F"                    # 44 and eax, 3Fh
            b"\xC1\xE0\x04"                    # 47 shl eax, 4
            b"\x0B\xC1"                        # 50 or eax, ecx
            b"\xB9" + sessionHex +             # 52 mov ecx, offset g_session
            b"\x50"                            # 57 push eax
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"    # 58 movzx eax, packet_buf+6
            b"\xC1\xE0\x02"                    # 65 shl eax, 2
            b"\x0B\xC2"                        # 68 or eax, edx
            b"\x50"                            # 70 push eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 71 call CSession_SetPlayerPosD
            b"\x68\xAB\xAB\xAB\xAB"            # 76 push offset g_session.m_cur
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 81 lea eax, [ebp+modeName]
            b"\x68" + rswStrHex +              # 87 push offset aS_rsw
            b"\x50"                            # 92 push eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 93 call sprintf
            b"\x83\xC4\x0C"                    # 98 add esp, 0Ch
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 101 lea eax, [ebp+modeName]
            b"\xB9" + modeMgrHex +             # 107 mov ecx, offset g_modeMgr
            b"\x50"                            # 112 push eax
            b"\x6A\x01"                        # 113 push 1
            b"\xE8\xAB\xAB\xAB\xAB"            # 115 call CModeMgr_Switch
            b"\x6A\x00"                        # 120 push 0
            b"\xE8\xAB\xAB\xAB\xAB"            # 122 call CRagConnection_instan
            b"\x8B\xC8"                        # 127 mov ecx, eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 129 call CConnection_SetBlock
            b"\x80\x3D\xAB\xAB\xAB\xAB\x00"    # 134 cmp isDebugLog, 0
            b"\x0F\x84\xAB\xAB\xAB\xAB"        # 141 jz loc_9D836C
            b"\x68\xAB\xAB\xAB\xAB"            # 147 push offset aAt
            b"\x68" + pingLogHex +             # 152 push offset aPinglog_txt
            b"\xFF\x15"                        # 157 call ds:fopen
        )
        startTimeOffset = (2, 4)
        pos0Offset = (61, 4)
        pos1Offset = (26, 4)
        pos2Offset = (19, 4)
        setServerTimeOffset = 12
        setPlayerPosDirOffset = 72
        map_nameOffset = 77
        sprintfOffset = (94, False)
        modeMgrSwitchOffset = 116
        instanceOffset = 123
        g_instanceOffset = 0
        setBlockOffset = 130
        isDebugOffset = 136
        fopenOffset = (159, True)
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2019-02-13
        # 0  push dword ptr packet_buf+2
        # 6  mov ecx, offset g_session
        # 11 call CSession_SetServerTime
        # 16 movzx ecx, packet_buf+8
        # 23 movzx edx, packet_buf+7
        # 30 mov eax, ecx
        # 32 and eax, 0Fh
        # 35 shr ecx, 4
        # 38 push eax
        # 39 mov eax, edx
        # 41 shr edx, 6
        # 44 and eax, 3Fh
        # 47 shl eax, 4
        # 50 or eax, ecx
        # 52 mov ecx, offset g_session
        # 57 push eax
        # 58 movzx eax, packet_buf+6
        # 65 shl eax, 2
        # 68 or edx, eax
        # 70 push edx
        # 71 call CSession_SetPlayerPosDir
        # 76 push (offset g_session+614h)
        # 81 lea eax, [ebp+modeName]
        # 87 push offset aS_rsw
        # 92 push eax
        # 93 call sprintf
        # 98 add esp, 0Ch
        # 101 lea eax, [ebp+modeName]
        # 107 mov ecx, offset g_modeMgr
        # 112 push eax
        # 113 push 1
        # 115 call CModeMgr_Switch
        # 120 push 0
        # 122 call CRagConnection_instanceR
        # 127 mov ecx, eax
        # 129 call CConnection_SetBlock
        # 134 cmp isDebugLog, 0
        # 141 jz loc_8E6EEC
        # 147 push offset aAt
        # 152 push offset aPinglog_txt
        # 157 call ds:fopen
        code = (
            b"\xFF\x35\xAB\xAB\xAB\xAB"        # 0 push dword ptr packet_buf+2
            b"\xB9" + sessionHex +             # 6 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"            # 11 call CSession_SetServerTime
            b"\x0F\xB6\x0D\xAB\xAB\xAB\xAB"    # 16 movzx ecx, packet_buf+8
            b"\x0F\xB6\x15\xAB\xAB\xAB\xAB"    # 23 movzx edx, packet_buf+7
            b"\x8B\xC1"                        # 30 mov eax, ecx
            b"\x83\xE0\x0F"                    # 32 and eax, 0Fh
            b"\xC1\xE9\x04"                    # 35 shr ecx, 4
            b"\x50"                            # 38 push eax
            b"\x8B\xC2"                        # 39 mov eax, edx
            b"\xC1\xEA\x06"                    # 41 shr edx, 6
            b"\x83\xE0\x3F"                    # 44 and eax, 3Fh
            b"\xC1\xE0\x04"                    # 47 shl eax, 4
            b"\x0B\xC1"                        # 50 or eax, ecx
            b"\xB9" + sessionHex +             # 52 mov ecx, offset g_session
            b"\x50"                            # 57 push eax
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"    # 58 movzx eax, packet_buf+6
            b"\xC1\xE0\x02"                    # 65 shl eax, 2
            b"\x0B\xD0"                        # 68 or edx, eax
            b"\x52"                            # 70 push edx
            b"\xE8\xAB\xAB\xAB\xAB"            # 71 call CSession_SetPlayerPosD
            b"\x68\xAB\xAB\xAB\xAB"            # 76 push offset g_session.m_cur
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 81 lea eax, [ebp+modeName]
            b"\x68" + rswStrHex +              # 87 push offset aS_rsw
            b"\x50"                            # 92 push eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 93 call sprintf
            b"\x83\xC4\x0C"                    # 98 add esp, 0Ch
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 101 lea eax, [ebp+modeName]
            b"\xB9" + modeMgrHex +             # 107 mov ecx, offset g_modeMgr
            b"\x50"                            # 112 push eax
            b"\x6A\x01"                        # 113 push 1
            b"\xE8\xAB\xAB\xAB\xAB"            # 115 call CModeMgr_Switch
            b"\x6A\x00"                        # 120 push 0
            b"\xE8\xAB\xAB\xAB\xAB"            # 122 call CRagConnection_instan
            b"\x8B\xC8"                        # 127 mov ecx, eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 129 call CConnection_SetBlock
            b"\x80\x3D\xAB\xAB\xAB\xAB\x00"    # 134 cmp isDebugLog, 0
            b"\x0F\x84\xAB\xAB\xAB\xAB"        # 141 jz loc_8E6EEC
            b"\x68\xAB\xAB\xAB\xAB"            # 147 push offset aAt
            b"\x68" + pingLogHex +             # 152 push offset aPinglog_txt
            b"\xFF\x15"                        # 157 call ds:fopen
        )
        startTimeOffset = (2, 4)
        pos0Offset = (61, 4)
        pos1Offset = (26, 4)
        pos2Offset = (19, 4)
        setServerTimeOffset = 12
        setPlayerPosDirOffset = 72
        map_nameOffset = 77
        sprintfOffset = (94, False)
        modeMgrSwitchOffset = 116
        instanceOffset = 123
        g_instanceOffset = 0
        setBlockOffset = 130
        isDebugOffset = 136
        fopenOffset = (159, True)
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2016-2018
        # 0  mov ecx, offset g_session
        # 5  push [edi+struct_packet_73.start_time]
        # 8  call CSession_SetServerTime
        # 13 movzx edx, [edi+struct_packet_73.pos2]
        # 17 movzx esi, [edi+struct_packet_73.pos1]
        # 21 mov eax, edx
        # 23 and eax, 0Fh
        # 26 push eax
        # 27 mov eax, esi
        # 29 and eax, 3Fh
        # 32 shl eax, 4
        # 35 shr edx, 4
        # 38 or eax, edx
        # 40 push eax
        # 41 movzx eax, [edi+struct_packet_73.pos0]
        # 45 shl eax, 2
        # 48 shr esi, 6
        # 51 or esi, eax
        # 53 push esi
        # 54 mov ecx, offset g_session
        # 59 call CSession_SetPlayerPosDir
        # 64 lea eax, [ebp+modeName]
        # 67 push offset g_sessionm_curMap
        # 72 push offset aS_rsw
        # 77 push eax
        # 78 nop
        # 79 call sprintf
        # 84 add esp, 0Ch
        # 87 lea eax, [ebp+modeName]
        # 90 push eax
        # 91 push 1
        # 93 mov ecx, offset g_modeMgr
        # 98 call CModeMgr_Switch
        # 103 push 0
        # 105 call CRagConnection_instanceR
        # 110 mov ecx, eax
        # 112 call CConnection_SetBlock
        # 117 cmp isDebug, 0
        # 124 jz loc_9FBF42
        # 130 push offset _dword_CCF9F4
        # 135 push offset aPinglog_txt
        # 140 call fopen
        code = (
            b"\xB9" + sessionHex +             # 0
            b"\xFF\x77\xAB"                    # 5
            b"\xE8\xAB\xAB\xAB\xAB"            # 8
            b"\x0F\xB6\x57\xAB"                # 13
            b"\x0F\xB6\x77\xAB"                # 17
            b"\x8B\xC2"                        # 21
            b"\x83\xE0\x0F"                    # 23
            b"\x50"                            # 26
            b"\x8B\xC6"                        # 27
            b"\x83\xE0\x3F"                    # 29
            b"\xC1\xE0\x04"                    # 32
            b"\xC1\xEA\x04"                    # 35
            b"\x0B\xC2"                        # 38
            b"\x50"                            # 40
            b"\x0F\xB6\x47\xAB"                # 41
            b"\xC1\xE0\x02"                    # 45
            b"\xC1\xEE\x06"                    # 48
            b"\x0B\xF0"                        # 51
            b"\x56"                            # 53
            b"\xB9" + sessionHex +             # 54
            b"\xE8\xAB\xAB\xAB\xAB"            # 59
            b"\x8D\x45\xAB"                    # 64
            b"\x68\xAB\xAB\xAB\xAB"            # 67
            b"\x68" + rswStrHex +              # 72
            b"\x50"                            # 77
            b"\x90"                            # 78
            b"\xE8\xAB\xAB\xAB\xAB"            # 79
            b"\x83\xC4\x0C"                    # 84
            b"\x8D\x45\xAB"                    # 87
            b"\x50"                            # 90
            b"\x6A\x01"                        # 91
            b"\xB9" + modeMgrHex +             # 93
            b"\xE8\xAB\xAB\xAB\xAB"            # 98
            b"\x6A\x00"                        # 103
            b"\xE8\xAB\xAB\xAB\xAB"            # 105
            b"\x8B\xC8"                        # 110
            b"\xE8\xAB\xAB\xAB\xAB"            # 112
            b"\x80\x3D\xAB\xAB\xAB\xAB\x00"    # 117
            b"\x0F\x84\xAB\xAB\xAB\x00"        # 124
            b"\x68\xAB\xAB\xAB\xAB"            # 130
            b"\x68" + pingLogHex +             # 135
            b"\xE8"                            # 140
        )
        startTimeOffset = (7, 1)
        pos0Offset = (44, 1)
        pos1Offset = (20, 1)
        pos2Offset = (16, 1)
        setServerTimeOffset = 9
        setPlayerPosDirOffset = 60
        map_nameOffset = 68
        sprintfOffset = (80, False)
        modeMgrSwitchOffset = 99
        instanceOffset = 106
        g_instanceOffset = 0
        setBlockOffset = 113
        isDebugOffset = 119
        fopenOffset = (141, False)
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2015-01-07
        # 0  mov eax, [esi+struct_packet_73.start_time]
        # 3  push eax
        # 4  mov ecx, offset g_session
        # 9  call CSession_SetServerTime
        # 14 movzx eax, [esi+struct_packet_73.pos2]
        # 18 movzx ecx, [esi+struct_packet_73.pos1]
        # 22 mov edx, eax
        # 24 and edx, 0Fh
        # 27 push edx
        # 28 mov edx, ecx
        # 30 shr eax, 4
        # 33 and edx, 3Fh
        # 36 shl edx, 4
        # 39 or edx, eax
        # 41 movzx eax, [esi+struct_packet_73.pos0]
        # 45 add eax, eax
        # 47 shr ecx, 6
        # 50 add eax, eax
        # 52 or eax, ecx
        # 54 push edx
        # 55 push eax
        # 56 mov ecx, offset g_session
        # 61 call CSession_SetPlayerPosDir
        # 66 push (offset g_session+71Ch)
        # 71 lea ecx, [ebp+modeName]
        # 74 push offset aS_rsw
        # 79 push ecx
        # 80 call sprintf
        # 86 add esp, 0Ch
        # 89 lea edx, [ebp+modeName]
        # 92 push edx
        # 93 push 1
        # 95 mov ecx, offset g_modeMgr
        # 100 call CModeMgr_Switch
        # 105 push 0
        # 107 call CRagConnection_instanceR
        # 112 mov ecx, eax
        # 114 call CConnection_SetBlock
        # 119 cmp isDebugLog, 0
        # 126 jz loc_85E625
        # 132 push offset aAt
        # 137 push offset aPinglog_txt
        # 142 call fopen
        code = (
            b"\x8B\x46\xAB"                    # 0
            b"\x50"                            # 3
            b"\xB9" + sessionHex +             # 4
            b"\xE8\xAB\xAB\xAB\xAB"            # 9
            b"\x0F\xB6\x46\xAB"                # 14
            b"\x0F\xB6\x4E\xAB"                # 18
            b"\x8B\xD0"                        # 22
            b"\x83\xE2\x0F"                    # 24
            b"\x52"                            # 27
            b"\x8B\xD1"                        # 28
            b"\xC1\xE8\x04"                    # 30
            b"\x83\xE2\x3F"                    # 33
            b"\xC1\xE2\x04"                    # 36
            b"\x0B\xD0"                        # 39
            b"\x0F\xB6\x46\xAB"                # 41
            b"\x03\xC0"                        # 45
            b"\xC1\xE9\x06"                    # 47
            b"\x03\xC0"                        # 50
            b"\x0B\xC1"                        # 52
            b"\x52"                            # 54
            b"\x50"                            # 55
            b"\xB9" + sessionHex +             # 56
            b"\xE8\xAB\xAB\xAB\xAB"            # 61
            b"\x68\xAB\xAB\xAB\xAB"            # 66
            b"\x8D\x4D\xAB"                    # 71
            b"\x68" + rswStrHex +              # 74
            b"\x51"                            # 79
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 80
            b"\x83\xC4\x0C"                    # 86
            b"\x8D\x55\xAB"                    # 89
            b"\x52"                            # 92
            b"\x6A\x01"                        # 93
            b"\xB9" + modeMgrHex +             # 95
            b"\xE8\xAB\xAB\xAB\xAB"            # 100
            b"\x6A\x00"                        # 105
            b"\xE8\xAB\xAB\xAB\xAB"            # 107
            b"\x8B\xC8"                        # 112
            b"\xE8\xAB\xAB\xAB\xAB"            # 114
            b"\x80\x3D\xAB\xAB\xAB\xAB\x00"    # 119
            b"\x0F\x84\xAB\xAB\xAB\xAB"        # 126
            b"\x68\xAB\xAB\xAB\xAB"            # 132
            b"\x68" + pingLogHex +             # 137
            b"\xFF\x15"                        # 142
        )
        startTimeOffset = (2, 1)
        pos0Offset = (44, 1)
        pos1Offset = (21, 1)
        pos2Offset = (17, 1)
        setServerTimeOffset = 10
        setPlayerPosDirOffset = 62
        map_nameOffset = 67
        sprintfOffset = (82, True)
        modeMgrSwitchOffset = 101
        instanceOffset = 108
        g_instanceOffset = 0
        setBlockOffset = 115
        isDebugOffset = 121
        fopenOffset = (144, True)
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2015-07-01
        # 0  mov ecx, offset g_session
        # 5  push [edi+struct_packet_73.start_time]
        # 8  call CSession_SetServerTime
        # 13 movzx edx, [edi+struct_packet_73.pos2]
        # 17 movzx esi, [edi+struct_packet_73.pos1]
        # 21 mov eax, edx
        # 23 and eax, 0Fh
        # 26 push eax
        # 27 mov eax, esi
        # 29 and eax, 3Fh
        # 32 shl eax, 4
        # 35 shr edx, 4
        # 38 or eax, edx
        # 40 push eax
        # 41 movzx eax, [edi+struct_packet_73.pos0]
        # 45 shl eax, 2
        # 48 shr esi, 6
        # 51 or esi, eax
        # 53 push esi
        # 54 mov ecx, offset g_session
        # 59 call CSession_SetPlayerPosDir
        # 64 lea eax, [ebp+modeName]
        # 67 push (offset g_session+618h)
        # 72 push offset aS_rsw
        # 77 push eax
        # 78 no nop
        # 79 call near ptr 32E8620h
        # 84 add esp, 0Ch
        # 87 lea eax, [ebp+modeName]
        # 90 push eax
        # 91 push 1
        # 93 mov ecx, offset g_modeMgr
        # 98 call CModeMgr_Switch
        # 103 push 0
        # 105 call CRagConnection_instanceR
        # 110 mov ecx, eax
        # 112 call CConnection_SetBlock
        # 117 cmp isDebugLog, 0
        # 124 jz loc_89B212
        # 130 push offset _dword_AC513C
        # 135 push offset aPinglog_txt
        # 140 no nop
        # 141 call near ptr 334D68Ch
        code = (
            b"\xB9" + sessionHex +             # 0
            b"\xFF\x77\xAB"                    # 5
            b"\xE8\xAB\xAB\xAB\xAB"            # 8
            b"\x0F\xB6\x57\xAB"                # 13
            b"\x0F\xB6\x77\xAB"                # 17
            b"\x8B\xC2"                        # 21
            b"\x83\xE0\x0F"                    # 23
            b"\x50"                            # 26
            b"\x8B\xC6"                        # 27
            b"\x83\xE0\x3F"                    # 29
            b"\xC1\xE0\x04"                    # 32
            b"\xC1\xEA\x04"                    # 35
            b"\x0B\xC2"                        # 38
            b"\x50"                            # 40
            b"\x0F\xB6\x47\xAB"                # 41
            b"\xC1\xE0\x02"                    # 45
            b"\xC1\xEE\x06"                    # 48
            b"\x0B\xF0"                        # 51
            b"\x56"                            # 53
            b"\xB9" + sessionHex +             # 54
            b"\xE8\xAB\xAB\xAB\xAB"            # 59
            b"\x8D\x45\xAB"                    # 64
            b"\x68\xAB\xAB\xAB\xAB"            # 67
            b"\x68" + rswStrHex +              # 72
            b"\x50"                            # 77
            b"\x90"                            # 78
            b"\xE8\xAB\xAB\xAB\xAB"            # 79
            b"\x83\xC4\x0C"                    # 84
            b"\x8D\x45\xAB"                    # 87
            b"\x50"                            # 90
            b"\x6A\x01"                        # 91
            b"\xB9" + modeMgrHex +             # 93
            b"\xE8\xAB\xAB\xAB\xAB"            # 98
            b"\x6A\x00"                        # 103
            b"\xE8\xAB\xAB\xAB\xAB"            # 105
            b"\x8B\xC8"                        # 110
            b"\xE8\xAB\xAB\xAB\xAB"            # 112
            b"\x80\x3D\xAB\xAB\xAB\xAB\x00"    # 117
            b"\x0F\x84\xAB\xAB\xAB\xAB"        # 124
            b"\x68\xAB\xAB\xAB\xAB"            # 130
            b"\x68" + pingLogHex +             # 135
            b"\x90"                            # 140
            b"\xE8"                            # 141
        )
        startTimeOffset = (7, 1)
        pos0Offset = (44, 1)
        pos1Offset = (20, 1)
        pos2Offset = (16, 1)
        setServerTimeOffset = 9
        setPlayerPosDirOffset = 60
        map_nameOffset = 68
        sprintfOffset = (80, False)
        modeMgrSwitchOffset = 99
        instanceOffset = 106
        g_instanceOffset = 0
        setBlockOffset = 113
        isDebugOffset = 119
        fopenOffset = (142, False)
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2013-01-03
        # 0  mov eax, [esi+struct_packet_73.start_time]
        # 3  push eax
        # 4  mov ecx, offset g_session
        # 9  call CSession_SetServerTime
        # 14 movzx eax, [esi+struct_packet_73.pos2]
        # 18 movzx ecx, [esi+struct_packet_73.pos1]
        # 22 mov edx, eax
        # 24 and edx, 0Fh
        # 27 push edx
        # 28 mov edx, ecx
        # 30 shr eax, 4
        # 33 and edx, 3Fh
        # 36 shl edx, 4
        # 39 or edx, eax
        # 41 movzx eax, [esi+struct_packet_73.pos0]
        # 45 add eax, eax
        # 47 shr ecx, 6
        # 50 add eax, eax
        # 52 or eax, ecx
        # 54 push edx
        # 55 push eax
        # 56 mov ecx, offset g_session
        # 61 call CSession_SetPlayerPosDir
        # 66 push (offset g_session.field_704+4)
        # 71 lea ecx, [esp+34h+modeName]
        # 75 push offset aS_rsw
        # 80 push ecx
        # 81 call sprintf
        # 87 add esp, 0Ch
        # 90 lea edx, [esp+30h+modeName]
        # 94 push edx
        # 95 push 1
        # 97 mov ecx, offset g_modeMgr
        # 102 call CModeMgr_Switch
        # 107 push 0
        # 109 call CRagConnection_instanceR
        # 114 mov ecx, eax
        # 116 call CConnection_SetBlock
        # 121 cmp isDebugLog, 0
        # 128 jz loc_7E46C8
        # 134 push offset aAt
        # 139 push offset aPinglog_txt
        # 144 call fopen
        code = (
            b"\x8B\x46\xAB"                    # 0
            b"\x50"                            # 3
            b"\xB9" + sessionHex +             # 4
            b"\xE8\xAB\xAB\xAB\xAB"            # 9
            b"\x0F\xB6\x46\xAB"                # 14
            b"\x0F\xB6\x4E\xAB"                # 18
            b"\x8B\xD0"                        # 22
            b"\x83\xE2\x0F"                    # 24
            b"\x52"                            # 27
            b"\x8B\xD1"                        # 28
            b"\xC1\xE8\x04"                    # 30
            b"\x83\xE2\x3F"                    # 33
            b"\xC1\xE2\x04"                    # 36
            b"\x0B\xD0"                        # 39
            b"\x0F\xB6\x46\xAB"                # 41
            b"\x03\xC0"                        # 45
            b"\xC1\xE9\x06"                    # 47
            b"\x03\xC0"                        # 50
            b"\x0B\xC1"                        # 52
            b"\x52"                            # 54
            b"\x50"                            # 55
            b"\xB9" + sessionHex +             # 56
            b"\xE8\xAB\xAB\xAB\xAB"            # 61
            b"\x68\xAB\xAB\xAB\xAB"            # 66
            b"\x8D\x4C\x24\xAB"                # 71
            b"\x68" + rswStrHex +              # 75
            b"\x51"                            # 80
            b"\xFF\x15\xAB\xAB\xAB\xAB"        # 81
            b"\x83\xC4\x0C"                    # 87
            b"\x8D\x54\x24\xAB"                # 90
            b"\x52"                            # 94
            b"\x6A\x01"                        # 95
            b"\xB9" + modeMgrHex +             # 97
            b"\xE8\xAB\xAB\xAB\xAB"            # 102
            b"\x6A\x00"                        # 107
            b"\xE8\xAB\xAB\xAB\xAB"            # 109
            b"\x8B\xC8"                        # 114
            b"\xE8\xAB\xAB\xAB\xAB"            # 116
            b"\x80\x3D\xAB\xAB\xAB\xAB\x00"    # 121
            b"\x0F\x84\xAB\xAB\xAB\xAB"        # 128
            b"\x68\xAB\xAB\xAB\xAB"            # 134
            b"\x68" + pingLogHex +             # 139
            b"\xFF\x15"                        # 144
        )
        startTimeOffset = (2, 1)
        pos0Offset = (44, 1)
        pos1Offset = (21, 1)
        pos2Offset = (17, 1)
        setServerTimeOffset = 10
        setPlayerPosDirOffset = 62
        map_nameOffset = 67
        sprintfOffset = (83, True)
        modeMgrSwitchOffset = 103
        instanceOffset = 110
        g_instanceOffset = 0
        setBlockOffset = 117
        isDebugOffset = 123
        fopenOffset = (146, True)
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2010-01-05
        # 0  mov ecx, offset g_session
        # 5  mov eax, [esi+struct_packet_73.start_time]
        # 8  push eax
        # 9  call CSession_SetServerTime
        # 14 xor eax, eax
        # 16 xor ecx, ecx
        # 18 mov al, [esi+struct_packet_73.pos2]
        # 21 mov cl, [esi+struct_packet_73.pos1]
        # 24 mov edx, eax
        # 26 and edx, 0Fh
        # 29 push edx
        # 30 mov edx, ecx
        # 32 and edx, 3Fh
        # 35 shl edx, 4
        # 38 shr eax, 4
        # 41 or edx, eax
        # 43 xor eax, eax
        # 45 mov al, [esi+struct_packet_73.pos0]
        # 48 push edx
        # 49 shl eax, 2
        # 52 shr ecx, 6
        # 55 or eax, ecx
        # 57 mov ecx, offset g_session
        # 62 push eax
        # 63 call CSession_SetPlayerPosDir
        # 68 push offset CSession_m_curMap
        # 73 lea ecx, [ebp+modeName]
        # 76 push offset aS_rsw
        # 81 push ecx
        # 82 call sprintf
        # 87 add esp, 0Ch
        # 90 lea edx, [ebp+modeName]
        # 93 mov ecx, offset g_modeMgr
        # 98 push edx
        # 99 push 1
        # 101 call CModeMgr_Switch
        # 106 push 0
        # 108 call CRagConnection_instanceR
        # 113 mov ecx, eax
        # 115 call CConnection_SetBlock
        # 120 mov al, isDebugLog
        # 125 test al, al
        # 127 jz loc_5B7C0B
        # 133 push offset aAt
        # 138 push offset aPinglog_txt
        # 143 call fopen
        code = (
            b"\xB9" + sessionHex +             # 0
            b"\x8B\x46\xAB"                    # 5
            b"\x50"                            # 8
            b"\xE8\xAB\xAB\xAB\xAB"            # 9
            b"\x33\xC0"                        # 14
            b"\x33\xC9"                        # 16
            b"\x8A\x46\xAB"                    # 18
            b"\x8A\x4E\xAB"                    # 21
            b"\x8B\xD0"                        # 24
            b"\x83\xE2\x0F"                    # 26
            b"\x52"                            # 29
            b"\x8B\xD1"                        # 30
            b"\x83\xE2\x3F"                    # 32
            b"\xC1\xE2\x04"                    # 35
            b"\xC1\xE8\x04"                    # 38
            b"\x0B\xD0"                        # 41
            b"\x33\xC0"                        # 43
            b"\x8A\x46\xAB"                    # 45
            b"\x52"                            # 48
            b"\xC1\xE0\x02"                    # 49
            b"\xC1\xE9\x06"                    # 52
            b"\x0B\xC1"                        # 55
            b"\xB9" + sessionHex +             # 57
            b"\x50"                            # 62
            b"\xE8\xAB\xAB\xAB\xAB"            # 63
            b"\x68\xAB\xAB\xAB\xAB"            # 68
            b"\x8D\x4D\xAB"                    # 73
            b"\x68" + rswStrHex +              # 76
            b"\x51"                            # 81
            b"\xE8\xAB\xAB\xAB\xAB"            # 82
            b"\x83\xC4\x0C"                    # 87
            b"\x8D\x55\xAB"                    # 90
            b"\xB9" + modeMgrHex +             # 93
            b"\x52"                            # 98
            b"\x6A\x01"                        # 99
            b"\xE8\xAB\xAB\xAB\xAB"            # 101
            b"\x6A\x00"                        # 106
            b"\xE8\xAB\xAB\xAB\xAB"            # 108
            b"\x8B\xC8"                        # 113
            b"\xE8\xAB\xAB\xAB\xAB"            # 115
            b"\xA0\xAB\xAB\xAB\xAB"            # 120
            b"\x84\xC0"                        # 125
            b"\x0F\x84\xAB\xAB\xAB\xAB"        # 127
            b"\x68\xAB\xAB\xAB\xAB"            # 133
            b"\x68" + pingLogHex +             # 138
            b"\xE8"                            # 143
        )
        startTimeOffset = (7, 1)
        pos0Offset = (47, 1)
        pos1Offset = (23, 1)
        pos2Offset = (20, 1)
        setServerTimeOffset = 10
        setPlayerPosDirOffset = 64
        map_nameOffset = 69
        sprintfOffset = (83, False)
        modeMgrSwitchOffset = 102
        instanceOffset = 109
        g_instanceOffset = 0
        setBlockOffset = 116
        isDebugOffset = 121
        fopenOffset = (144, False)
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2008-05-20
        # 0  mov eax, [esi+struct_packet_73.start_time]
        # 3  push eax
        # 4  mov ecx, offset g_session
        # 9  call CSession_SetServerTime
        # 14 movzx eax, [esi+struct_packet_73.pos2]
        # 18 movzx ecx, [esi+struct_packet_73.pos1]
        # 22 mov edx, eax
        # 24 and edx, 0Fh
        # 27 push edx
        # 28 mov edx, ecx
        # 30 shr eax, 4
        # 33 and edx, 3Fh
        # 36 shl edx, 4
        # 39 or edx, eax
        # 41 movzx eax, [esi+struct_packet_73.pos0]
        # 45 add eax, eax
        # 47 shr ecx, 6
        # 50 add eax, eax
        # 52 or eax, ecx
        # 54 push edx
        # 55 push eax
        # 56 mov ecx, offset g_session
        # 61 call CSession_SetPlayerPosDir
        # 66 push offset g_session_map_name
        # 71 lea ecx, [esp+34h+modeName]
        # 75 push offset aS_rsw
        # 80 push ecx
        # 81 call _sprintf
        # 86 add esp, 0Ch
        # 89 lea edx, [esp+30h+modeName]
        # 93 push edx
        # 94 push 1
        # 96 mov ecx, offset g_modeMgr
        # 101 call CModeMgr_Switch
        # 106 push 0
        # 108 call CRagConnection_instanceR
        # 113 mov ecx, eax
        # 115 call CConnection_SetBlock
        # 120 cmp isDebugLog, 0
        # 127 jz loc_5ECF0A
        # 133 push offset aAt
        # 138 push offset aPinglog_txt
        # 143 call _fopen
        code = (
            b"\x8B\x46\xAB"                    # 0
            b"\x50"                            # 3
            b"\xB9" + sessionHex +             # 4
            b"\xE8\xAB\xAB\xAB\xAB"            # 9
            b"\x0F\xB6\x46\xAB"                # 14
            b"\x0F\xB6\x4E\xAB"                # 18
            b"\x8B\xD0"                        # 22
            b"\x83\xE2\x0F"                    # 24
            b"\x52"                            # 27
            b"\x8B\xD1"                        # 28
            b"\xC1\xE8\x04"                    # 30
            b"\x83\xE2\x3F"                    # 33
            b"\xC1\xE2\x04"                    # 36
            b"\x0B\xD0"                        # 39
            b"\x0F\xB6\x46\xAB"                # 41
            b"\x03\xC0"                        # 45
            b"\xC1\xE9\x06"                    # 47
            b"\x03\xC0"                        # 50
            b"\x0B\xC1"                        # 52
            b"\x52"                            # 54
            b"\x50"                            # 55
            b"\xB9" + sessionHex +             # 56
            b"\xE8\xAB\xAB\xAB\xAB"            # 61
            b"\x68\xAB\xAB\xAB\xAB"            # 66
            b"\x8D\x4C\x24\xAB"                # 71
            b"\x68" + rswStrHex +              # 75
            b"\x51"                            # 80
            b"\xE8\xAB\xAB\xAB\xAB"            # 81
            b"\x83\xC4\x0C"                    # 86
            b"\x8D\x54\x24\xAB"                # 89
            b"\x52"                            # 93
            b"\x6A\x01"                        # 94
            b"\xB9" + modeMgrHex +             # 96
            b"\xE8\xAB\xAB\xAB\xAB"            # 101
            b"\x6A\x00"                        # 106
            b"\xE8\xAB\xAB\xAB\xAB"            # 108
            b"\x8B\xC8"                        # 113
            b"\xE8\xAB\xAB\xAB\xAB"            # 115
            b"\x80\x3D\xAB\xAB\xAB\xAB\x00"    # 120
            b"\x0F\x84\xAB\xAB\xAB\xAB"        # 127
            b"\x68\xAB\xAB\xAB\xAB"            # 133
            b"\x68" + pingLogHex +             # 138
            b"\xE8"                            # 143
        )
        startTimeOffset = (2, 1)
        pos0Offset = (44, 1)
        pos1Offset = (21, 1)
        pos2Offset = (17, 1)
        setServerTimeOffset = 10
        setPlayerPosDirOffset = 62
        map_nameOffset = 67
        sprintfOffset = (82, False)
        modeMgrSwitchOffset = 102
        instanceOffset = 109
        g_instanceOffset = 0
        setBlockOffset = 116
        isDebugOffset = 122
        fopenOffset = (144, False)
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2004-01-07
        # 0  mov ecx, offset g_session
        # 5  mov eax, [esi+struct_packet_73.start_time]
        # 8  push eax
        # 9  call CSession_SetServerTime
        # 14 xor eax, eax
        # 16 xor ecx, ecx
        # 18 mov al, [esi+struct_packet_73.pos2]
        # 21 mov cl, [esi+struct_packet_73.pos1]
        # 24 mov edx, eax
        # 26 and edx, 0Fh
        # 29 push edx
        # 30 mov edx, ecx
        # 32 and edx, 3Fh
        # 35 shl edx, 4
        # 38 shr eax, 4
        # 41 or edx, eax
        # 43 xor eax, eax
        # 45 mov al, [esi+struct_packet_73.pos0]
        # 48 push edx
        # 49 shl eax, 2
        # 52 shr ecx, 6
        # 55 or eax, ecx
        # 57 mov ecx, offset g_session
        # 62 push eax
        # 63 call CSession_SetPlayerPosDir
        # 68 push offset g_session_map_name
        # 73 lea ecx, [ebp+modeName]
        # 76 push offset aS_rsw
        # 81 push ecx
        # 82 call _sprintf
        # 87 add esp, 0Ch
        # 90 lea edx, [ebp+modeName]
        # 93 mov ecx, offset g_modeMgr
        # 98 push edx
        # 99 push 1
        # 101 call CModeMgr_Switch
        # 106 push 0
        # 108 mov ecx, offset g_instanceR
        # 113 call CRagConnection_CConnection_SetBlock
        # 118 mov al, isDebugLog
        # 123 test al, al
        # 125 jz loc_51F105
        # 131 push offset aAt
        # 136 push offset aPinglog_txt
        # 141 call _fopen
        code = (
            b"\xB9" + sessionHex +             # 0
            b"\x8B\x46\xAB"                    # 5
            b"\x50"                            # 8
            b"\xE8\xAB\xAB\xAB\xAB"            # 9
            b"\x33\xC0"                        # 14
            b"\x33\xC9"                        # 16
            b"\x8A\x46\xAB"                    # 18
            b"\x8A\x4E\xAB"                    # 21
            b"\x8B\xD0"                        # 24
            b"\x83\xE2\x0F"                    # 26
            b"\x52"                            # 29
            b"\x8B\xD1"                        # 30
            b"\x83\xE2\x3F"                    # 32
            b"\xC1\xE2\x04"                    # 35
            b"\xC1\xE8\x04"                    # 38
            b"\x0B\xD0"                        # 41
            b"\x33\xC0"                        # 43
            b"\x8A\x46\xAB"                    # 45
            b"\x52"                            # 48
            b"\xC1\xE0\x02"                    # 49
            b"\xC1\xE9\x06"                    # 52
            b"\x0B\xC1"                        # 55
            b"\xB9" + sessionHex +             # 57
            b"\x50"                            # 62
            b"\xE8\xAB\xAB\xAB\xAB"            # 63
            b"\x68\xAB\xAB\xAB\xAB"            # 68
            b"\x8D\x4D\xAB"                    # 73
            b"\x68" + rswStrHex +              # 76
            b"\x51"                            # 81
            b"\xE8\xAB\xAB\xAB\xAB"            # 82
            b"\x83\xC4\x0C"                    # 87
            b"\x8D\x55\xAB"                    # 90
            b"\xB9" + modeMgrHex +             # 93
            b"\x52"                            # 98
            b"\x6A\x01"                        # 99
            b"\xE8\xAB\xAB\xAB\xAB"            # 101
            b"\x6A\x00"                        # 106
            b"\xB9\xAB\xAB\xAB\xAB"            # 108
            b"\xE8\xAB\xAB\xAB\xAB"            # 113
            b"\xA0\xAB\xAB\xAB\xAB"            # 118
            b"\x84\xC0"                        # 123
            b"\x0F\x84\xAB\xAB\xAB\xAB"        # 125
            b"\x68\xAB\xAB\xAB\xAB"            # 131
            b"\x68" + pingLogHex +             # 136
            b"\xE8"                            # 141
        )
        startTimeOffset = (7, 1)
        pos0Offset = (47, 1)
        pos1Offset = (23, 1)
        pos2Offset = (20, 1)
        setServerTimeOffset = 10
        setPlayerPosDirOffset = 64
        map_nameOffset = 69
        sprintfOffset = (83, False)
        modeMgrSwitchOffset = 102
        instanceOffset = 0
        g_instanceOffset = 109
        setBlockOffset = 114
        isDebugOffset = 119
        fopenOffset = (142, False)
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        self.log("failed in search packet73")
        exit(1)

    self.packet73Tmp = offset
    startTime = self.getVarAddr(offset, startTimeOffset)
    pos0 = self.getVarAddr(offset, pos0Offset)
    pos1 = self.getVarAddr(offset, pos1Offset)
    pos2 = self.getVarAddr(offset, pos2Offset)
    if startTime > 0x1000:
        # here all fields not from packet start, but from some offset in buffer
        pos0 = pos0 - startTime + 2
        pos1 = pos1 - startTime + 2
        pos2 = pos2 - startTime + 2
        startTime = 2
    if pos0 + 1 != pos1 or pos1 + 1 != pos2:
        self.log("Error: found wrong pos fields")
        exit(1)
    if abs(startTime - pos0) > 0x100:
        self.log("Error: found wrong startTime or pos fields")
        exit(1)

    if instanceOffset != 0:
        instance = self.getAddr(offset,
                                instanceOffset,
                                instanceOffset + 4)
        if instance != self.instanceR:
            self.log("Error: detected wrong CRagConnection_instanceR.")
            exit(1)
    elif g_instanceOffset != 0:
        instance = self.exe.readUInt(offset + g_instanceOffset)
        if instance != self.g_instanceR:
            self.log("Error: detected wrong g_instanceR.")
            exit(1)

    debug = False
    self.addStruct("struct_packet_73")
    self.addStructMember("packet_id", 0, 2, debug)
    self.addStructMember("start_time", startTime, 4, debug)
    self.addStructMember("pos", pos0, 3, debug)

    self.CSession_SetServerTime = self.getAddr(offset,
                                               setServerTimeOffset,
                                               setServerTimeOffset + 4)
    self.addRawFunc("CSession::SetServerTime", self.CSession_SetServerTime)
    self.CSession_SetPlayerPosDir = self.getAddr(offset,
                                                 setPlayerPosDirOffset,
                                                 setPlayerPosDirOffset + 4)
    self.addRawFunc("CSession::SetPlayerPosDir", self.CSession_SetPlayerPosDir)
    self.CModeMgr_Switch = self.getAddr(offset,
                                        modeMgrSwitchOffset,
                                        modeMgrSwitchOffset + 4)
    self.addRawFunc("CModeMgr::Switch", self.CModeMgr_Switch)
    self.setBlock = self.getAddr(offset, setBlockOffset, setBlockOffset + 4)
    self.addRawFunc("CConnection::SetBlock", self.setBlock)
    self.isDebugLog = self.exe.readUInt(offset + isDebugOffset)
    self.addVaVar("isDebugLog", self.isDebugLog)
    if sprintfOffset[1] is True:
        self.sprintf = self.exe.readUInt(offset + sprintfOffset[0])
        self.addVaFunc("sprintf", self.sprintf)
    else:
        self.sprintf = self.getAddr(offset,
                                    sprintfOffset[0],
                                    sprintfOffset[0] + 4)
        self.addRawFunc("sprintf", self.sprintf)
    if fopenOffset[1] is True:
        self.fopen = self.exe.readUInt(offset + fopenOffset[0])
        self.addVaFunc("fopen", self.fopen)
    else:
        self.fopen = self.getAddr(offset,
                                  fopenOffset[0],
                                  fopenOffset[0] + 4)
        self.addRawFunc("fopen", self.fopen)

    self.CSession_curMap = self.exe.readUInt(offset + map_nameOffset) - \
        self.session
    self.addStruct("CSession")
    self.addStructMember("m_curMap", self.CSession_curMap, 16, True)
    self.setStructMemberType(self.CSession_curMap, "char[16]")
