#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchItemInfoFunctions(self):
    # search in recv_packet_A31

    offset, section = self.exe.string(b"%s\t\t\t%d\t%c%d%c\n")
    if offset is False:
        self.log("failed in search '%s\\t\\t\\t%d\\t%c%d%c\\n'")
        if self.packetVersion < "20160000" or \
           self.clientType in ("ruro", "bro", "iro", "euro", "tro"):
            return
        exit(1)
    formatStr = section.rawToVa(offset)

    formatStrHex = self.exe.toHex(formatStr, 4)
    fprintfHex = self.exe.toHex(self.fprintf, 4)

    # 0  push 103h
    # 5  lea eax, [ebp+buf]
    # 11 push 0
    # 13 push eax
    # 14 mov [ebp+dstName], 0
    # 21 call j_memset
    # 26 mov eax, [esi]
    # 28 mov esi, [esi+4]
    # 31 mov [ebp+var_B18], eax
    # 37 movzx eax, ax
    # 40 add esp, 0Ch
    # 43 lea ecx, [ebp+item]
    # 49 push eax
    # 50 call ITEM_INFO_SetItemId
    # 55 lea eax, [ebp+dstName]
    # 61 push eax
    # 62 lea ecx, [ebp+item]
    # 68 call ITEM_INFO_GetIdDisplayName
    # 73 movzx eax, word ptr [ebp+var_B18+2]
    # 80 push 29h
    # 82 push eax
    # 83 push 28h
    # 85 push esi
    # 86 lea eax, [ebp+dstName]
    # 92 push eax
    # 93 push offset aSDCDC
    # 98 push ebx
    # 99 call fprintf
    code = (
        b"\x68\x03\x01\x00\x00"            # 0
        b"\x8D\x85\xE9\xFD\xFF\xFF"        # 5
        b"\x6A\x00"                        # 11
        b"\x50"                            # 13
        b"\xC6\x85\xE8\xAB\xAB\xAB\xAB"    # 14
        b"\xE8\xAB\xAB\xAB\xAB"            # 21
        b"\x8B\x06"                        # 26
        b"\x8B\x76\x04"                    # 28
        b"\x89\x85\xAB\xAB\xAB\xAB"        # 31
        b"\x0F\xB7\xC0"                    # 37
        b"\x83\xC4\x0C"                    # 40
        b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 43
        b"\x50"                            # 49
        b"\xE8\xAB\xAB\xAB\xAB"            # 50
        b"\x8D\x85\xAB\xAB\xAB\xAB"        # 55
        b"\x50"                            # 61
        b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 62
        b"\xE8\xAB\xAB\xAB\xAB"            # 68
        b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"    # 73
        b"\x6A\x29"                        # 80
        b"\x50"                            # 82
        b"\x6A\x28"                        # 83
        b"\x56"                            # 85
        b"\x8D\x85\xAB\xAB\xAB\xAB"        # 86
        b"\x50"                            # 92
        b"\x68" + formatStrHex +           # 93
        b"\x53"                            # 98
        b"\xFF\x15" + fprintfHex           # 99
    )
    j_memsetOffset = 22
    itemIdOffset = 51
    nameOffset = 69
    fprintfOffset = 0
    offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2018-07-04
        # 0  push 103h
        # 5  lea eax, [ebp+var_217]
        # 11 push 0
        # 13 push eax
        # 14 mov [ebp+var_218], 0
        # 21 call j_memset
        # 26 movq xmm0, qword ptr [esi]
        # 30 mov ax, [esi+8]
        # 34 add esp, 0Ch
        # 37 movq qword ptr [ebp+Val], xmm0
        # 45 push [ebp+Val]
        # 51 lea ecx, [ebp+var_B08]
        # 57 mov [ebp+var_B18], ax
        # 64 call ITEM_INFO_SetItemId
        # 69 lea eax, [ebp+var_218]
        # 75 push eax
        # 76 lea ecx, [ebp+var_B08]
        # 82 call ITEM_INFO_GetIdDisplayName
        # 87 movzx eax, word ptr [ebp+Val+4]
        # 94 mov esi, [ebp+Val+6]
        # 100 push 29h
        # 102 push eax
        # 103 push 28h
        # 105 push esi
        # 106 lea eax, [ebp+var_218]
        # 112 push eax
        # 113 push offset aSDCDC
        # 118 push edi
        # 119 call fprintf
        code = (
            b"\x68\x03\x01\x00\x00"            # 0
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 5
            b"\x6A\x00"                        # 11
            b"\x50"                            # 13
            b"\xC6\x85\xE8\xAB\xAB\xAB\xAB"    # 14
            b"\xE8\xAB\xAB\xAB\xAB"            # 21
            b"\xF3\x0F\x7E\x06"                # 26
            b"\x66\x8B\x46\x08"                # 30
            b"\x83\xC4\x0C"                    # 34
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 37
            b"\xFF\xB5\xAB\xAB\xAB\xAB"        # 45
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 51
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"    # 57
            b"\xE8\xAB\xAB\xAB\xAB"            # 64
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 69
            b"\x50"                            # 75
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 76
            b"\xE8\xAB\xAB\xAB\xAB"            # 82
            b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"    # 87
            b"\x8B\xB5\xAB\xAB\xAB\xAB"        # 94
            b"\x6A\x29"                        # 100
            b"\x50"                            # 102
            b"\x6A\x28"                        # 103
            b"\x56"                            # 105
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 106
            b"\x50"                            # 112
            b"\x68" + formatStrHex +           # 113
            b"\x57"                            # 118
            b"\xFF\x15" + fprintfHex           # 119
        )
        j_memsetOffset = 22
        itemIdOffset = 65
        nameOffset = 83
        fprintfOffset = 0
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2016-04-27
        # 0  push 103h
        # 5  lea eax, [ebp+buf]
        # 11 push 0
        # 13 push eax
        # 14 mov [ebp+dstName], 0
        # 21 call j_memset
        # 26 mov eax, [esi]
        # 28 mov esi, [esi+4]
        # 31 mov [ebp+var_B18], eax
        # 37 movzx eax, ax
        # 40 add esp, 0Ch
        # 43 lea ecx, [ebp+item]
        # 49 push eax
        # 50 call ITEM_INFO_SetItemId
        # 55 lea eax, [ebp+dstName]
        # 61 push eax
        # 62 lea ecx, [ebp+item]
        # 68 call ITEM_INFO_GetIdDisplayName
        # 73 movzx eax, word ptr [ebp+var_B18+2]
        # 80 push 29h
        # 82 push eax
        # 83 push 28h
        # 85 push esi
        # 86 lea eax, [ebp+dstName]
        # 92 push eax
        # 93 push offset aSDCDC
        # 98 push ebx
        # 99 call fprintf
        code = (
            b"\x68\x03\x01\x00\x00"            # 0
            b"\x8D\x85\xE9\xFD\xFF\xFF"        # 5
            b"\x6A\x00"                        # 11
            b"\x50"                            # 13
            b"\xC6\x85\xE8\xAB\xAB\xAB\xAB"    # 14
            b"\xE8\xAB\xAB\xAB\xAB"            # 21
            b"\x8B\x06"                        # 26
            b"\x8B\x76\x04"                    # 28
            b"\x89\x85\xAB\xAB\xAB\xAB"        # 31
            b"\x0F\xB7\xC0"                    # 37
            b"\x83\xC4\x0C"                    # 40
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 43
            b"\x50"                            # 49
            b"\xE8\xAB\xAB\xAB\xAB"            # 50
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 55
            b"\x50"                            # 61
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 62
            b"\xE8\xAB\xAB\xAB\xAB"            # 68
            b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"    # 73
            b"\x6A\x29"                        # 80
            b"\x50"                            # 82
            b"\x6A\x28"                        # 83
            b"\x56"                            # 85
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 86
            b"\x50"                            # 92
            b"\x68" + formatStrHex +           # 93
            b"\x53"                            # 98
            b"\x90"                            # 99
            b"\xE8"                            # 100
        )
        j_memsetOffset = 22
        itemIdOffset = 51
        nameOffset = 69
        fprintfOffset = 0
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2019-02-13
        # 0  push 104h
        # 5  lea eax, [ebp+dstName]
        # 11 push 0
        # 13 push eax
        # 14 call memset
        # 19 movq xmm0, qword ptr [esi]
        # 23 lea ecx, [ebp+item]
        # 29 mov ax, [esi+8]
        # 33 add esp, 0Ch
        # 36 movq qword ptr [ebp+Val], xmm0
        # 44 mov [ebp+var_B14], ax
        # 51 push [ebp+Val]
        # 57 call ITEM_INFO_SetItemId
        # 62 lea eax, [ebp+dstName]
        # 68 push eax
        # 69 lea ecx, [ebp+item]
        # 75 call ITEM_INFO_GetIdDisplayName
        # 80 movzx eax, word ptr [ebp+Val+4]
        # 87 mov esi, [ebp+Val+6]
        # 93 push 29h
        # 95 push eax
        # 96 push 28h
        # 98 push esi
        # 99 lea eax, [ebp+dstName]
        # 105 push eax
        # 106 push offset aSDCDC
        # 111 push edi
        # 112 call fprintf
        code = (
            b"\x68\x04\x01\x00\x00"            # 0 push 104h
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 5 lea eax, [ebp+dstName]
            b"\x6A\x00"                        # 11 push 0
            b"\x50"                            # 13 push eax
            b"\xE8\xAB\xAB\xAB\xAB"            # 14 call memset
            b"\xF3\x0F\x7E\x06"                # 19 movq xmm0, qword ptr [esi]
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 23 lea ecx, [ebp+item]
            b"\x66\x8B\x46\x08"                # 29 mov ax, [esi+8]
            b"\x83\xC4\x0C"                    # 33 add esp, 0Ch
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 36 movq qword ptr [ebp+Val],
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"    # 44 mov [ebp+var_B14], ax
            b"\xFF\xB5\xAB\xAB\xAB\xAB"        # 51 push [ebp+Val]
            b"\xE8\xAB\xAB\xAB\xAB"            # 57 call ITEM_INFO_SetItemId
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 62 lea eax, [ebp+dstName]
            b"\x50"                            # 68 push eax
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 69 lea ecx, [ebp+item]
            b"\xE8\xAB\xAB\xAB\xAB"            # 75 call ITEM_INFO_GetIdDisplay
            b"\x0F\xB7\x85\xAB\xAB\xAB\xAB"    # 80 movzx eax, word ptr [ebp+Va
            b"\x8B\xB5\xAB\xAB\xAB\xAB"        # 87 mov esi, [ebp+Val+6]
            b"\x6A\x29"                        # 93 push 29h
            b"\x50"                            # 95 push eax
            b"\x6A\x28"                        # 96 push 28h
            b"\x56"                            # 98 push esi
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 99 lea eax, [ebp+dstName]
            b"\x50"                            # 105 push eax
            b"\x68" + formatStrHex +           # 106 push offset aSDCDC
            b"\x57"                            # 111 push edi
            b"\xE8"                            # 112 call fprintf
        )
        j_memsetOffset = 15
        itemIdOffset = 58
        nameOffset = 76
        fprintfOffset = 113
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        self.log("failed in ITEM_INFO")
        exit(1)

    offset1 = offset
    if fprintfOffset != 0:
        self.fprintf = self.getAddr(offset,
                                    fprintfOffset,
                                    fprintfOffset + 4)
        self.addRawFunc("fprintf", self.fprintf)
    self.ITEM_INFO_SetItemId = self.getAddr(offset,
                                            itemIdOffset,
                                            itemIdOffset + 4)
    self.addRawFunc("ITEM_INFO::SetItemId", self.ITEM_INFO_SetItemId)
    self.ITEM_INFO_GetIdDisplayName = self.getAddr(offset,
                                                   nameOffset,
                                                   nameOffset + 4)
    self.addRawFunc("ITEM_INFO::GetIdDisplayName",
                    self.ITEM_INFO_GetIdDisplayName)

    self.j_memset = self.getAddr(offset,
                                 j_memsetOffset,
                                 j_memsetOffset + 4)
    if self.memset != 0 and self.memset != self.j_memset:
        self.addRawFunc("j_memset", self.j_memset)

    # 0  lea ecx, [ebp+item]
    # 6  call ITEM_INFO_init
    # 11 mov [ebp+var_4], 2
    # 18 movzx eax, word ptr [ebx+4]
    # 22 push eax
    # 23 lea ecx, [ebp+item]
    # 29 call ITEM_INFO_SetItemId
    # 34 lea eax, [ebp+name]
    # 40 push eax
    # 41 lea ecx, [ebp+item]
    # 47 call ITEM_INFO_GetIdDisplayName
    code = (
        b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 0
        b"\xE8\xAB\xAB\xAB\xAB"            # 6
        b"\xC7\x45\xAB\x02\x00\x00\x00"    # 11
        b"\x0F\xB7\x43\xAB"                # 18
        b"\x50"                            # 22
        b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 23
        b"\xE8\xAB\xAB\xAB\xAB"            # 29
        b"\x8D\x85\xAB\xAB\xAB\xAB"        # 34
        b"\x50"                            # 40
        b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 41
        b"\xE8"                            # 47
    )
    itemOffsets = ((2, 4), (25, 4), (43, 4))
    initOffset = 7
    setItemIdOffset = 30
    getIdDisplayNameOffset = 48
    offset = self.exe.codeWildcard(code, b"\xAB", offset1 - 0x300, offset1)
    if offset is False:
        # 2018-07-04
        # 0  lea ecx, [ebp+item]
        # 6  call ITEM_INFO_init
        # 11 mov [ebp+var_4], 2
        # 18 push dword ptr [ebx+4]
        # 21 lea ecx, [ebp+item]
        # 27 call ITEM_INFO_SetItemId
        # 32 lea eax, [ebp+var_114]
        # 38 push eax
        # 39 lea ecx, [ebp+item]
        # 45 call ITEM_INFO_GetIdDisplayName
        code = (
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 0
            b"\xE8\xAB\xAB\xAB\xAB"            # 6
            b"\xC7\x45\xAB\x02\x00\x00\x00"    # 11
            b"\xFF\x73\xAB"                    # 18
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 21
            b"\xE8\xAB\xAB\xAB\xAB"            # 27
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 32
            b"\x50"                            # 38
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 39
            b"\xE8"                            # 45
        )
        itemOffsets = ((2, 4), (23, 4), (41, 4))
        initOffset = 7
        setItemIdOffset = 28
        getIdDisplayNameOffset = 46
        offset = self.exe.codeWildcard(code, b"\xAB", offset1 - 0x300, offset1)
    if offset is False:
        # 0  lea ecx, [ebp+item]
        # 6  call ITEM_INFO_init
        # 11 mov [ebp+var_4], 2
        # 18 lea ecx, [ebp+item]
        # 24 push dword ptr [ebx+4]
        # 27 call ITEM_INFO_SetItemId
        # 32 lea eax, [ebp+Dst]
        # 38 push eax
        # 39 lea ecx, [ebp+item]
        # 45 call ITEM_INFO_GetIdDisplayName
        code = (
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 0 lea ecx, [ebp+item]
            b"\xE8\xAB\xAB\xAB\xAB"            # 6 call ITEM_INFO_init
            b"\xC7\x45\xAB\x02\x00\x00\x00"    # 11 mov [ebp+var_4], 2
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 18 lea ecx, [ebp+item]
            b"\xFF\x73\xAB"                    # 24 push dword ptr [ebx+4]
            b"\xE8\xAB\xAB\xAB\xAB"            # 27 call ITEM_INFO_SetItemId
            b"\x8D\x85\xAB\xAB\xAB\xAB"        # 32 lea eax, [ebp+Dst]
            b"\x50"                            # 38 push eax
            b"\x8D\x8D\xAB\xAB\xAB\xAB"        # 39 lea ecx, [ebp+item]
            b"\xE8"                            # 45 call ITEM_INFO_GetIdDisplay
        )
        itemOffsets = ((2, 4), (20, 4), (41, 4))
        initOffset = 7
        setItemIdOffset = 28
        getIdDisplayNameOffset = 46
        offset = self.exe.codeWildcard(code, b"\xAB", offset1 - 0x300, offset1)

    if offset is False:
        self.log("failed in ITEM_INFO (second block)")
        exit(1)

    itemAddr = 0
    for itemOffset in itemOffsets:
        tmpOffset = self.getVarAddr(offset, itemOffset)
        if itemAddr == 0:
            itemAddr = tmpOffset
        elif itemAddr != tmpOffset:
            self.log("Error: found different item offsets")
            exit(1)
    setItemId = self.getAddr(offset,
                             setItemIdOffset,
                             setItemIdOffset + 4)
    if setItemId != self.ITEM_INFO_SetItemId:
        self.log("Error: found different ITEM_INFO::SetItemId")
        exit(1)
    getIdDisplayName = self.getAddr(offset,
                                    getIdDisplayNameOffset,
                                    getIdDisplayNameOffset + 4)
    if getIdDisplayName != self.ITEM_INFO_GetIdDisplayName:
        self.log("Error: found different ITEM_INFO::GetIdDisplayName")
        exit(1)
    self.ITEM_INFO_init = self.getAddr(offset,
                                       initOffset,
                                       initOffset + 4)
    self.addRawFunc("ITEM_INFO::init", self.ITEM_INFO_init)
