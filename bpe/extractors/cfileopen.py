#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

# search inside CFile::open
# 2017+


def searchCFileOpen(self):
    # 2017-11-01
    # 0  lea esi, [ebx+CFile.m_name]
    # 3  push edi
    # 4  mov ecx, esi
    # 6  call std_string_assign
    # 11 mov [ebx+CFile.m_cursor], 0
    # 18 cmp dword ptr [esi+14h], 10h
    # 22 jb short loc_44B519
    # 24 mov esi, [esi]
    # 26 push dword ptr [ebp+var_2C]
    # 29 lea eax, [ebx+CFile.m_size]
    # 32 push eax
    # 33 push esi
    # 34 mov ecx, offset g_fileMgr
    # 39 call CFileMgr_readFileWithFlag
    # 44 test eax, eax
    # 46 mov [ebx+CFile.m_buf], eax
    # 49 setnz al
    # 52 jmp short loc_44B5A5
    code = (
        b"\x8D\x73\xAB"                    # 0 lea esi, [ebx+CFile.m_name]
        b"\x57"                            # 3 push edi
        b"\x8B\xCE"                        # 4 mov ecx, esi
        b"\xE8\xAB\xAB\xAB\xAB"            # 6 call std_string_assign
        b"\xC7\x43\xAB\x00\x00\x00\x00"    # 11 mov [ebx+CFile.m_cursor], 0
        b"\x83\x7E\xAB\xAB"                # 18 cmp dword ptr [esi+14h], 10h
        b"\x72\x02"                        # 22 jb short loc_44B519
        b"\x8B\x36"                        # 24 mov esi, [esi]
        b"\xFF\x75\xAB"                    # 26 push dword ptr [ebp+var_2C]
        b"\x8D\x43\xAB"                    # 29 lea eax, [ebx+CFile.m_size]
        b"\x50"                            # 32 push eax
        b"\x56"                            # 33 push esi
        b"\xB9\xAB\xAB\xAB\xAB"            # 34 mov ecx, offset g_fileMgr
        b"\xE8\xAB\xAB\xAB\xAB"            # 39 call CFileMgr_readFileWithFlag
        b"\x85\xC0"                        # 44 test eax, eax
        b"\x89\x43\xAB"                    # 46 mov [ebx+CFile.m_buf], eax
        b"\x0F\x95\xC0"                    # 49 setnz al
        b"\xEB"                            # 52 jmp short loc_44B5A5
    )
    fileNameOffset = (2, 1)
    stringAssignOffset = 7
    cursorOffset = (13, 1)
    str1Offset = (21, 1)
    str2Offset = (20, 1)
    sizeOffset = (31, 1)
    gFileMgrOffset = 35
    readFileOffset = 40
    bufOffset = (48, 1)
    offset = self.exe.codeWildcard(code,
                                   b"\xAB",
                                   self.CFile_open,
                                   self.CFile_open + 0x150)

    if offset is False:
        # 2019-02-13
        # 0  lea esi, [edi+CFile.m_name]
        # 3  push ecx
        # 4  push edx
        # 5  mov ecx, esi
        # 7  call std_string_assign
        # 12 mov [edi+CFile.m_cursor], 0
        # 19 mov eax, esi
        # 21 cmp dword ptr [esi+14h], 10h
        # 25 jb short loc_48B3AB
        # 27 mov eax, [esi]
        # 29 push dword ptr [ebp+var_30]
        # 32 lea ecx, [edi+CFile.m_size]
        # 35 push ecx
        # 36 push eax
        # 37 mov ecx, offset g_fileMgr
        # 42 call CFileMgr_readFileWithFlag
        # 47 mov [edi+CFile.m_buf], eax
        # 50 test eax, eax
        # 52 jz loc_48B490
        code = (
            b"\x8D\x77\xAB"                    # 0 lea esi, [edi+CFile.m_name]
            b"\x51"                            # 3 push ecx
            b"\x52"                            # 4 push edx
            b"\x8B\xCE"                        # 5 mov ecx, esi
            b"\xE8\xAB\xAB\xAB\xAB"            # 7 call std_string_assign
            b"\xC7\x47\xAB\x00\x00\x00\x00"    # 12 mov [edi+CFile.m_cursor], 0
            b"\x8B\xC6"                        # 19 mov eax, esi
            b"\x83\x7E\xAB\xAB"                # 21 cmp dword ptr [esi+14h], 10
            b"\x72\x02"                        # 25 jb short loc_48B3AB
            b"\x8B\x06"                        # 27 mov eax, [esi]
            b"\xFF\x75\xAB"                    # 29 push dword ptr [ebp+var_30]
            b"\x8D\x4F\xAB"                    # 32 lea ecx, [edi+CFile.m_size]
            b"\x51"                            # 35 push ecx
            b"\x50"                            # 36 push eax
            b"\xB9\xAB\xAB\xAB\xAB"            # 37 mov ecx, offset g_fileMgr
            b"\xE8\xAB\xAB\xAB\xAB"            # 42 call CFileMgr_readFileWithF
            b"\x89\x47\xAB"                    # 47 mov [edi+CFile.m_buf], eax
            b"\x85\xC0"                        # 50 test eax, eax
            b"\x0F\x84"                        # 52 jz loc_48B490
        )
        fileNameOffset = (2, 1)
        stringAssignOffset = 8
        cursorOffset = (14, 1)
        str1Offset = (24, 1)
        str2Offset = (23, 1)
        sizeOffset = (34, 1)
        gFileMgrOffset = 38
        readFileOffset = 43
        bufOffset = (49, 1)
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.CFile_open,
                                       self.CFile_open + 0x150)

    if offset is False:
        # 2019-01-09
        # 0  lea esi, [ebx+CFile.m_name]
        # 3  push edi
        # 4  mov ecx, esi
        # 6  call std_string_assign
        # 11 mov [ebx+CFile.m_cursor], 0
        # 18 cmp dword ptr [esi+14h], 10h
        # 22 jb short loc_4518DB
        # 24 mov eax, [esi]
        # 26 jmp short loc_4518DD
        # 28 mov eax, esi
        # 30 push dword ptr [ebp+directFlag]
        # 33 lea edi, [ebx+CFile.m_size]
        # 36 push edi
        # 37 push eax
        # 38 mov ecx, offset g_fileMgr
        # 43 call CFileMgr_readFileWithFlag
        # 48 mov [ebx+CFile.m_buf], eax
        # 51 test eax, eax
        # 53 jz loc_4519D6
        code = (
            b"\x8D\x73\xAB"                    # 0 lea esi, [ebx+CFile.m_name]
            b"\x57"                            # 3 push edi
            b"\x8B\xCE"                        # 4 mov ecx, esi
            b"\xE8\xAB\xAB\xAB\xAB"            # 6 call std_string_assign
            b"\xC7\x43\xAB\x00\x00\x00\x00"    # 11 mov [ebx+CFile.m_cursor], 0
            b"\x83\x7E\xAB\xAB"                # 18 cmp dword ptr [esi+14h], 10
            b"\x72\x04"                        # 22 jb short loc_4518DB
            b"\x8B\x06"                        # 24 mov eax, [esi]
            b"\xEB\x02"                        # 26 jmp short loc_4518DD
            b"\x8B\xC6"                        # 28 mov eax, esi
            b"\xFF\x75\xAB"                    # 30 push dword ptr [ebp+directF
            b"\x8D\x7B\xAB"                    # 33 lea edi, [ebx+CFile.m_size]
            b"\x57"                            # 36 push edi
            b"\x50"                            # 37 push eax
            b"\xB9\xAB\xAB\xAB\xAB"            # 38 mov ecx, offset g_fileMgr
            b"\xE8\xAB\xAB\xAB\xAB"            # 43 call CFileMgr_readFileWithF
            b"\x89\x43\xAB"                    # 48 mov [ebx+CFile.m_buf], eax
            b"\x85\xC0"                        # 51 test eax, eax
            b"\x0F\x84"                        # 53 jz loc_4519D6
        )
        fileNameOffset = (2, 1)
        stringAssignOffset = 7
        cursorOffset = (13, 1)
        str1Offset = (21, 1)
        str2Offset = (20, 1)
        sizeOffset = (35, 1)
        gFileMgrOffset = 39
        readFileOffset = 44
        bufOffset = (50, 1)
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.CFile_open,
                                       self.CFile_open + 0x150)

    if offset is False:
        # 2019-01-13
        # 0  lea esi, [edi+CFile.m_name]
        # 3  push ecx
        # 4  push edx
        # 5  mov ecx, esi
        # 7  call std_string_assign
        # 12 mov [edi+CFile.m_cursor], 0
        # 19 cmp dword ptr [esi+14h], 10h
        # 23 jb short loc_48B0F9
        # 25 mov esi, [esi]
        # 27 push dword ptr [ebp+directFlag]
        # 30 lea eax, [edi+CFile.m_size]
        # 33 mov ecx, offset g_fileMgr
        # 38 push eax
        # 39 push esi
        # 40 call CFileMgr_readFileWithFlag
        # 45 test eax, eax
        # 47 mov [edi+CFile.m_buf], eax
        # 50 setnz al
        # 53 jmp short loc_48B182
        code = (
            b"\x8D\x77\xAB"                    # 0 lea esi, [edi+CFile.m_name]
            b"\x51"                            # 3 push ecx
            b"\x52"                            # 4 push edx
            b"\x8B\xCE"                        # 5 mov ecx, esi
            b"\xE8\xAB\xAB\xAB\xAB"            # 7 call std_string_assign
            b"\xC7\x47\xAB\x00\x00\x00\x00"    # 12 mov [edi+CFile.m_cursor], 0
            b"\x83\x7E\xAB\xAB"                # 19 cmp dword ptr [esi+14h], 10
            b"\x72\x02"                        # 23 jb short loc_48B0F9
            b"\x8B\x36"                        # 25 mov esi, [esi]
            b"\xFF\x75\xAB"                    # 27 push dword ptr [ebp+directF
            b"\x8D\x47\xAB"                    # 30 lea eax, [edi+CFile.m_size]
            b"\xB9\xAB\xAB\xAB\xAB"            # 33 mov ecx, offset g_fileMgr
            b"\x50"                            # 38 push eax
            b"\x56"                            # 39 push esi
            b"\xE8\xAB\xAB\xAB\xAB"            # 40 call CFileMgr_readFileWithF
            b"\x85\xC0"                        # 45 test eax, eax
            b"\x89\x47\xAB"                    # 47 mov [edi+CFile.m_buf], eax
            b"\x0F\x95\xC0"                    # 50 setnz al
            b"\xEB"                            # 53 jmp short loc_48B182
        )
        fileNameOffset = (2, 1)
        stringAssignOffset = 8
        cursorOffset = (14, 1)
        str1Offset = (22, 1)
        str2Offset = (21, 1)
        sizeOffset = (32, 1)
        gFileMgrOffset = 34
        readFileOffset = 41
        bufOffset = (49, 1)
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.CFile_open,
                                       self.CFile_open + 0x150)

    if offset is False:
        # 2017-01-04
        # 0  lea esi, [ebx+CFile.m_name]
        # 3  push edi
        # 4  mov ecx, esi
        # 6  call std_string_assign
        # 11 mov [ebx+CFile.m_cursor], 0
        # 18 cmp dword ptr [esi+14h], 10h
        # 22 jb short loc_42EAB9
        # 24 mov esi, [esi]
        # 26 push dword ptr [ebp+directFlag]
        # 29 lea eax, [ebx+CFile.m_size]
        # 32 push eax
        # 33 push esi
        # 34 mov ecx, offset g_fileMgr
        # 39 call CFileMgr_readFileWithFlag
        # 44 xor ecx, ecx
        # 46 test eax, eax
        # 48 mov [ebx+CFile.m_buf], eax
        # 51 setnz al
        # 54 jmp short loc_42EB4E
        code = (
            b"\x8D\x73\xAB"                    # 0 lea esi, [ebx+CFile.m_name]
            b"\x57"                            # 3 push edi
            b"\x8B\xCE"                        # 4 mov ecx, esi
            b"\xE8\xAB\xAB\xAB\xAB"            # 6 call std_string_assign
            b"\xC7\x43\xAB\x00\x00\x00\x00"    # 11 mov [ebx+CFile.m_cursor], 0
            b"\x83\x7E\xAB\xAB"                # 18 cmp dword ptr [esi+14h], 10
            b"\x72\x02"                        # 22 jb short loc_42EAB9
            b"\x8B\x36"                        # 24 mov esi, [esi]
            b"\xFF\x75\xAB"                    # 26 push dword ptr [ebp+directF
            b"\x8D\x43\xAB"                    # 29 lea eax, [ebx+CFile.m_size]
            b"\x50"                            # 32 push eax
            b"\x56"                            # 33 push esi
            b"\xB9\xAB\xAB\xAB\xAB"            # 34 mov ecx, offset g_fileMgr
            b"\xE8\xAB\xAB\xAB\xAB"            # 39 call CFileMgr_readFileWithF
            b"\x33\xC9"                        # 44 xor ecx, ecx
            b"\x85\xC0"                        # 46 test eax, eax
            b"\x89\x43\xAB"                    # 48 mov [ebx+CFile.m_buf], eax
            b"\x0F\x95\xC0"                    # 51 setnz al
            b"\xEB"                            # 54 jmp short loc_42EB4E
        )
        fileNameOffset = (2, 1)
        stringAssignOffset = 7
        cursorOffset = (13, 1)
        str1Offset = (21, 1)
        str2Offset = (20, 1)
        sizeOffset = (31, 1)
        gFileMgrOffset = 35
        readFileOffset = 40
        bufOffset = (50, 1)
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.CFile_open,
                                       self.CFile_open + 0x150)

    if offset is False:
        self.log("Error: failed in seach CFile::open")
        return

    cursor = self.getVarAddr(offset, cursorOffset)
    buf = self.getVarAddr(offset, bufOffset)
    size = self.getVarAddr(offset, sizeOffset)
    fileName = self.getVarAddr(offset, fileNameOffset)
    if fileName == 0 or cursor == 0 or buf == 0 or size == 0:
        self.log("Error: wrong CFile struct member offset")
        exit(1)
    str1 = self.getVarAddr(offset, str1Offset)
    str2 = self.getVarAddr(offset, str2Offset)

    if str1 != self.stdString[1]:
        self.log("Error: wrong m_len offset in seach CFile::open")
        exit(1)
    if str2 != self.stdString[2]:
        self.log("Error: wrong m_allocated_len offset in seach CFile::open")
        exit(1)

    self.std_string_assign = self.getAddr(offset,
                                          stringAssignOffset,
                                          stringAssignOffset + 4)
    self.addRawFuncType("std::string::assign",
                        self.std_string_assign,
                        "int __thiscall std_string_assign("
                        "struct_std_string *std_string_ptr,"
                        " char *inStr, int len)")
    self.gFileMgr = self.exe.readUInt(offset + gFileMgrOffset)
    self.addVaVar("g_fileMgr", self.gFileMgr)

    self.addStruct("CFile")
    if buf == 8:
        self.addStructMember("vptr", 0, 4, True)
        self.addStructMember("m_hFile", 4, 4, True)
        self.setStructMemberType(4, "HANDLE")
    elif buf == 4:
        self.addStructMember("m_hFile", 0, 4, True)
        self.setStructMemberType(0, "HANDLE")
    else:
        self.log("Error: unknown buf offset: {0}".format(buf))
    self.addStructMember("m_buf", buf, 4, True)
    self.setStructMemberType(buf, "char*")
    self.addStructMember("m_cursor", cursor, 4, True)
    self.addStructMember("m_size", size, 4, True)
    self.addStructMember("m_name", fileName, self.stdstringSize, True)
    self.setStructMemberType(fileName, "struct_std_string")

    self.CFileMgr_readFileWithFlag = self.getAddr(offset,
                                                  readFileOffset,
                                                  readFileOffset + 4)
    self.CFileMgr_readFileWithFlagCall = offset + readFileOffset
    self.addRawFuncType("CFileMgr::readFileWithFlag",
                        self.CFileMgr_readFileWithFlag,
                        "void *__thiscall CFileMgr_readFileWithFlag("
                        "CFileMgr *this, LPCSTR lpFileName, "
                        "int *fileSize, char directFlag)")

    self.cFileMode = 1
