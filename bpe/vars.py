#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from bpe.funcs import mangleFunctionName


def addToNameAddrDict(self, val):
    name = val[1]
    addr = val[2]
    if name in self.nameAddrDict:
        if addr != self.nameAddrDict[name]:
            self.log("Error: found different object "
                     "{0} with type {1}: {2} vs {3}".
                     format(name, val[0], addr, self.nameAddrDict[name]))
            exit(1)
    else:
        self.nameAddrDict[name] = addr


def showRawAddr(self, name, addr):
    self.exe.printRawAddr(name, addr)


def showVaAddr(self, name, addr):
    self.log("{0}: {1}".format(name, hex(addr)))


def addVaCommentAddr(self, name, addr, pos):
    self.addrList.append(
        ("ExtLine", name, hex(addr), pos))


def addVaFunctionCommentAddr(self, name, addr):
    self.addrList.append(
        ("SetFunctionCmt", name, hex(addr), 1))


def addRawFunc(self, name, addr):
    addr = self.exe.rawToVa(addr)
    addVaFunc(self, name, addr)


def getRawAddrToName(self, addr):
    addr = self.exe.rawToVa(addr)
    return getVaAddrToName(addr)


def getVaAddrToName(self, addr):
    if addr in self.addrNameDict:
        return self.addrNameDict[addr]
    else:
        return None


def isVarPresent(self, name):
    return name in self.nameAddrDict


def getAddrByName(self, name):
    if name in self.nameAddrDict:
        return int(self.nameAddrDict[name], 16)
    return False


def addVaFunc(self, name, addr, show=True):
    if addr < 0:
        print("Error: adding function '{0}' with wrong address: {1}".
              format(name, hex(addr)))
        exit(1)
    name1 = mangleFunctionName(name)
    if name != name1:
        addVaFunctionCommentAddr(self, name, addr)
    val = ("MakeFunction", name1, hex(addr))
    self.addrList.append(val)
    addToNameAddrDict(self, val)
    if addr in self.addrNameDict:
        if val != self.addrNameDict[addr]:
            self.log("Error: found duplicate function {0} {1}"
                     .format(self.addrNameDict[addr], val))
            exit(1)
        else:
            # adding same object
            return
    if show is True:
        self.log("{0}: {1}".format(name, hex(addr)))
    self.addrNameDict[addr] = val
    idx = name.find("::")
    if idx >= 0:
        name1 = mangleFunctionName(name)
        self.paramFunctions[addr] = (name1, name[:idx] + "*")


def removeVaFunc(self, name, addr):
    if addr in self.addrNameDict:
        del self.addrNameDict[addr]
    if addr in self.paramFunctions:
        del self.paramFunctions[addr]
    name1 = mangleFunctionName(name)
    found = True
    while found:
        found = False
        idx = 0
        for val in self.addrList:
            if val[1] == name or val[1] == name1:
                del self.addrList[idx]
                found = True
                break
            idx = idx + 1
    for tbl in self.vtables:
        idx = 0
        for member in tbl.members:
            if member == addr:
                del tbl.members[idx]
                break
            idx = idx + 1


def setVarType(self, addr, typeStr):
    val = ("SetType", "", hex(addr), typeStr)
    self.addrList.append(val)


def setVaFuncType(self, addr, typeStr):
    val = ("SetType", "", hex(addr), typeStr)
    self.addrList.append(val)


def setRawFuncType(self, addr, typeStr):
    addr = self.exe.rawToVa(addr)
    val = ("SetType", "", hex(addr), typeStr)
    self.addrList.append(val)


def addVaFuncType(self, name, addr, funcType):
    addVaFunc(self, name, addr)
    setVaFuncType(self, addr, funcType)


def addRawFuncType(self, name, addr, funcType):
    addr = self.exe.rawToVa(addr)
    addVaFunc(self, name, addr)
    setVaFuncType(self, addr, funcType)


def addRawVar(self, name, addr):
    addr = self.exe.rawToVa(addr)
    addVaVar(self, name, addr)


def addVaVar(self, name, addr, show=True):
    if show is True:
        self.log("{0}: {1}".format(name, hex(addr)))
    name1 = mangleFunctionName(name)
    if name != name1:
        addVaCommentAddr(self, name, addr, 0)
    val = ("MakeName", name1, hex(addr))
    self.addrList.append(val)
    self.addrNameDict[addr] = val
    addToNameAddrDict(self, val)


def addVaVarStr(self, name, addr, show=True):
    if show is True:
        data = self.exe.readStr(self.exe.vaToRawUnknown(addr)[0])
        self.log("{0}: {1}, {2}".format(name, hex(addr), str(data.decode())))
    name1 = mangleFunctionName(name)
    if name != name1:
        addVaCommentAddr(self, name, addr, 0)
    val = ("MakeName", name1, hex(addr))
    self.addrList.append(val)
    self.addrNameDict[addr] = val
    addToNameAddrDict(self, val)


def addVaStruct(self, name, addr, structName, show=True):
    if show is True:
        self.log("{0}: {1}".format(name, hex(addr)))
    name1 = mangleFunctionName(name)
    if name != name1:
        addVaCommentAddr(self, name, addr, 0)
    val = ("MakeName", name1, hex(addr))
    self.addrList.append(val)
    self.addrNameDict[addr] = val
    addToNameAddrDict(self, val)
    val = ("MakeStruct",
           "",
           hex(addr),
           structName)
    self.addrList.append(val)


def makeVaStruct(self, addr, structName):
    val = ("MakeStruct",
           "",
           hex(addr),
           structName)
    self.addrList.append(val)


def addVaLabel(self, name, addr, show=True):
    addVaVar(self, name, addr, show)


def addRawArray(self, name, addr, size, addFlag):
    addr = self.exe.rawToVa(addr)
    self.addVaArray(name, addr, size, addFlag)


def makeUnknown(self, addr, size):
    val = ("MakeUnknown", "", hex(addr), hex(size), "DOUNK_DELNAMES")
    self.addrList.append(val)


def makeDword(self, addr):
    val = ("MakeDword", "", hex(addr))
    self.addrList.append(val)


def makeByte(self, addr):
    val = ("MakeByte", "", hex(addr))
    self.addrList.append(val)


def addVaArray(self, name, addr, size, addFlag):
    self.log("{0}[{2}]: {1}".format(name, hex(addr), size))
    if addFlag is True:
        makeUnknown(self, addr, size)
        makeByte(self, addr)
        self.addrList.append(("MakeArray", "", hex(addr), hex(size)))
        val = ("MakeName", name, hex(addr))
        self.addrList.append(val)
        self.addrNameDict[addr] = val
        addToNameAddrDict(self, val)


def getStructIdByName(self, name):
    self.currentStructName = name
    val = ("GetStrucIdByName", name, 0)
    self.addrList.append(val)


def addStruct(self, name):
    if name in self.strcutsDefined:
        getStructIdByName(self, name)
        return
    self.currentStructName = name
    val = ("AddStrucEx", name, -1)
    self.addrList.append(val)
    self.strcutsDefined.add(name)
    self.structs[name] = dict()


def showStructMember(self, name, offset):
    self.log("{0}::{1}: {2}".format(self.currentStructName,
                                    name,
                                    hex(offset)))


def addStructMember(self, name, offset, size, show=False):
    if offset < 0 or offset > 0xc000:
        fmt = "Struct member {0}.{1} is wrong: {2}"
        raise Exception(fmt.format(self.currentStructName, name, offset))
    if show is True:
        showStructMember(self, name, offset)
    if size == 1:
        typeStr = "FF_BYTE | FF_DATA"
    elif size == 2:
        typeStr = "FF_WORD | FF_DATA"
    elif size == 4:
        typeStr = "FF_DWRD | FF_DATA"
    elif size == 8:
        typeStr = "FF_QWRD | FF_DATA"
    else:
        typeStr = "FF_DATA"
    # add member to struct selected by addStruct or getStrucIdByName
    val = ("AddStrucMember",
           name,
           0,
           "id",
           hex(offset),
           typeStr,
           -1,
           size)
    self.addrList.append(val)
    self.structs[self.currentStructName][name] = (offset, size)


def setStructMemberType(self, offset, typeStr):
    # set member type to struct selected by addStruct or getStrucIdByName
    val = ("SetTypeMember", "", 0, "id", offset, typeStr)
    self.addrList.append(val)


def setStructComment(self, comment):
    # set comment to struct selected by addStruct or getStrucIdByName
    val = ("SetStrucComment", "", 0, "id", comment, 1)
    self.addrList.append(val)


def setStructMemberComment(self, offset, comment):
    # set comment to struct member in struct selected by
    # addStruct or getStrucIdByName
    val = ("SetMemberComment", "", 0, "id", offset, comment, 1)
    self.addrList.append(val)


def setVaFuncParam(self, name, addr, thisParamType):
    val = ("AddTypeParam", name, hex(addr), thisParamType)
    self.addrList.append(val)


def setVtblMemberType(self, offset, funcAddr):
    # set vtbl member type to struct selected by addStruct or getStrucIdByName
    val = ("SetVtblTypeMember", "", 0, "id", offset, hex(funcAddr))
    self.addrList.append(val)


def setFuncCallPrefix(self, offset, prefix):
    # search next call after offset and set function name to prefix_Name
    # and first parameter same as setVaFuncParam
    val = ("SetFuncCallPrefix", "", hex(offset), prefix)
    self.addrList.append(val)
