#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


blocks = [
    # 2017-11-01
    [
        (
            b"\xB9\xAB\xAB\xAB\xAB"      # 0  mov ecx, offset g_windowMgr
            b"\x68\xAB\xAB\xAB\xAB"      # 5  push 134h
            b"\xE8"                      # 10 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 10,
            "windowId": 6,
            "retOffset": 5,
        },
        {
            "g_windowMgr": (1, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 0Ch
            b"\xB9\xAB\xAB\xAB\xAB"      # 2  mov ecx, offset g_windowMgr
            b"\xC6\x85\xAB\xAB\xFF\xFF\xAB"  # 7  mov byte ptr [ebp-90h], 1
            b"\xE8"                      # 14 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 14,
            "windowId": (1, 1),
            "retOffset": 0,
        },
        {
            "g_windowMgr": (3, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xB9\xAB\xAB\xAB\xAB"      # 0  mov ecx, offset g_windowMgr
            b"\x68\xAB\xAB\xAB\xAB"      # 5  push 80h
            b"\x8B\xF0"                  # 10 mov esi, eax
            b"\xE8"                      # 12 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 12,
            "windowId": 6,
            "retOffset": 5,
        },
        {
            "g_windowMgr": (1, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 1Ah
            b"\xB9\xAB\xAB\xAB\xAB"      # 2  mov ecx, offset g_windowMgr
            b"\x89\x45\xAB"              # 7  mov [ebp+var_5C], eax
            b"\xE8"                      # 10 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 10,
            "windowId": (1, 1),
            "retOffset": 0,
        },
        {
            "g_windowMgr": (3, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x75\xAB"              # 0  push [ebp+windowId]
            b"\xB9\xAB\xAB\xAB\xAB"      # 3  mov ecx, offset g_windowMgr
            b"\xE8"                      # 8  call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 8,
            "windowId": -1,
            "retOffset": -1,
        },
        {
            "g_windowMgr": (4, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 57h
            b"\xB9\xAB\xAB\xAB\xAB"      # 2  mov ecx, offset g_windowMgr
            b"\x39\x05\xAB\xAB\xAB\xAB"  # 7  cmp g_windowMgr.field_344, eax
            b"\x75\xAB"                  # 13 jnz short loc_59093C
            b"\xE8"                      # 15 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 15,
            "windowId": (1, 1),
            "retOffset": 0,
        },
        {
            "g_windowMgr": (3, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 2Eh
            b"\xB9\xAB\xAB\xAB\xAB"      # 2  mov ecx, offset g_windowMgr
            b"\x0F\x84\xAB\xAB\x00\x00"  # 7  jz loc_620E7F
            b"\xE8"                      # 13 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 13,
            "windowId": (1, 1),
            "retOffset": 0,
        },
        {
            "g_windowMgr": (3, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xB9\xAB\xAB\xAB\xAB"      # 0  mov ecx, offset g_windowMgr
            b"\x0F\x84\xAB\xAB\x00\x00"  # 5  jz loc_627463
            b"\x6A\xAB"                  # 11 push 1Ah
            b"\xE8"                      # 13 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 13,
            "windowId": (12, 1),
            "retOffset": 11,
        },
        {
            "g_windowMgr": (1, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\xB3\xAB\xAB\x00\x00"  # 0  push dword ptr [ebx+0BCh]
            b"\xB9\xAB\xAB\xAB\xAB"      # 6  mov ecx, offset g_windowMgr
            b"\xE8"                      # 11 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 11,
            "windowId": -1,
            "retOffset": -1,
        },
        {
            "g_windowMgr": (7, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 4Bh
            b"\xF2\x0F\x58\x04\xC5\xAB\xAB\xAB\xAB"  # 2  addsd xmm0, _qword_C7
            b"\xB9\xAB\xAB\xAB\xAB"      # 11 mov ecx, offset g_windowMgr
            b"\x66\x0F\x5A\xC0"          # 16 cvtpd2ps xmm0, xmm0
            b"\xF3\x0F\x11\x05\xAB\xAB\xAB\xAB"  # 20 movss g_session.field_DE8
            b"\xE8"                      # 28 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 28,
            "windowId": (1, 1),
            "retOffset": 0,
        },
        {
            "g_windowMgr": (12, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xB9\xAB\xAB\xAB\xAB"      # 0  mov ecx, offset g_windowMgr
            b"\x50"                      # 5  push eax
            b"\x74\xAB"                  # 6  jz short loc_689CC4
            b"\xC7\x84\xBE\xAB\xAB\xAB\xAB\x00\x00\x00\x00"  # 8  mov dword ptr
            b"\xE8"                      # 19 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 19,
            "windowId": -1,
            "retOffset": -1,
        },
        {
            "g_windowMgr": (1, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 135h
            b"\x83\x78\xAB\xAB"          # 5  cmp dword ptr [eax+30h], 0
            b"\xB9\xAB\xAB\xAB\xAB"      # 9  mov ecx, offset g_windowMgr
            b"\x74\xAB"                  # 14 jz short loc_689D2C
            b"\xC7\x86\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 16 mov dword ptr [es
            b"\xE8"                      # 26 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 26,
            "windowId": 1,
            "retOffset": 0,
        },
        {
            "g_windowMgr": (10, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0CCh
            b"\x0F\x94\xC0"              # 5  setz al
            b"\x83\x3D\xAB\xAB\xAB\xAB\xAB"  # 8  cmp g_windowMgr.field_3EC, 0
            b"\xB9\xAB\xAB\xAB\xAB"      # 15 mov ecx, offset g_windowMgr
            b"\xA3\xAB\xAB\xAB\xAB"      # 20 mov g_session.field_74, eax
            b"\x0F\x85\xAB\xAB\xFF\xFF"  # 25 jnz loc_68E70C
            b"\xE8"                      # 31 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 31,
            "windowId": 1,
            "retOffset": 0,
        },
        {
            "g_windowMgr": (16, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 8
            b"\x8B\xCB"                  # 2  mov ecx, ebx
            b"\xE8"                      # 4  call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 4,
            "windowId": (1, 1),
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0CDh
            b"\x8B\xCB"                  # 5  mov ecx, ebx
            b"\xE8"                      # 7  call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 7,
            "windowId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 22h
            b"\x8B\xCB"                  # 2  mov ecx, ebx
            b"\x39\xBB\xAB\xAB\xAB\x00"  # 4  cmp [ebx+UIWindowMgr.MESSENGERGRO
            b"\x74\xAB"                  # 10 jz short loc_6DD346
            b"\xE8"                      # 12 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 12,
            "windowId": (1, 1),
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xE9\xAB\xAB\x00\x00"      # 0  jmp loc_6E2BE5
            b"\xE8"                      # 5  call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 5,
            "windowId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 8
            b"\x8B\x41\xAB"              # 2  mov eax, [ecx+24h]
            b"\x2D\xAB\xAB\xAB\x00"      # 5  sub eax, 108h
            b"\x66\x0F\x6E\xC0"          # 10 movd xmm0, eax
            b"\x8B\x41\xAB"              # 14 mov eax, [ecx+28h]
            b"\x0F\x5B\xC0"              # 17 cvtdq2ps xmm0, xmm0
            b"\x2D\xAB\xAB\xAB\x00"      # 20 sub eax, 15Ah
            b"\xF3\x0F\x59\x05\xAB\xAB\xAB\xAB"  # 25 mulss xmm0, _dword_C7A970
            b"\x8B\xCB"                  # 33 mov ecx, ebx
            b"\xF3\x0F\x2C\xF0"          # 35 cvttss2si esi, xmm0
            b"\x66\x0F\x6E\xC0"          # 39 movd xmm0, eax
            b"\x0F\x5B\xC0"              # 43 cvtdq2ps xmm0, xmm0
            b"\xF3\x0F\x59\x05\xAB\xAB\xAB\xAB"  # 46 mulss xmm0, _dword_C7A970
            b"\xF3\x0F\x2C\xF8"          # 54 cvttss2si edi, xmm0
            b"\x89\x7D\xAB"              # 58 mov [ebp+a2._First], edi
            b"\xE8"                      # 61 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 61,
            "windowId": (1, 1),
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 9Bh
            b"\x8B\xCF"                  # 5  mov ecx, edi
            b"\xE8"                      # 7  call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 7,
            "windowId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 0Eh
            b"\x8B\xCE"                  # 2  mov ecx, esi
            b"\xE8"                      # 4  call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 4,
            "windowId": (1, 1),
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\xBF\xC1"              # 0  movsx eax, cx
            b"\x8B\x4D\xAB"              # 3  mov ecx, [ebp+var_10]
            b"\x2D\xAB\xAB\xAB\x00"      # 6  sub eax, 4E84h
            b"\x50"                      # 11 push eax
            b"\xE8"                      # 12 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 12,
            "windowId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 24h
            b"\xB9\xAB\xAB\xAB\xAB"      # 2  mov ecx, offset g_windowMgr
            b"\x89\xBE\xAB\xAB\xAB\x00"  # 7  mov [esi+71Ch], edi
            b"\xE8"                      # 13 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 13,
            "windowId": (1, 1),
            "retOffset": 0,
        },
        {
            "g_windowMgr": (3, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\xE5"                  # 0  mov esp, ebp
            b"\x5D"                      # 2  pop ebp
            b"\xC2\xAB\x00"              # 3  retn 8
            b"\xE8"                      # 6  call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 6,
            "windowId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0CCh
            b"\x8B\x40\xAB"              # 5  mov eax, [eax+30h]
            b"\xA3\xAB\xAB\xAB\xAB"      # 8  mov g_session.field_74, eax
            b"\xA3\xAB\xAB\xAB\xAB"      # 13 mov g_windowMgr.QUESTDISPWNDINFO_
            b"\xB9\xAB\xAB\xAB\xAB"      # 18 mov ecx, offset g_windowMgr
            b"\x85\xC0"                  # 23 test eax, eax
            b"\x74\xAB"                  # 25 jz short loc_6F681F
            b"\xE8"                      # 27 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 27,
            "windowId": 1,
            "retOffset": 0,
        },
        {
            "g_windowMgr": (19, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x72\xAB"              # 0  push dword ptr [edx+6]
            b"\xB9\xAB\xAB\xAB\xAB"      # 3  mov ecx, offset g_windowMgr
            b"\xE8"                      # 8  call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 8,
            "windowId": -1,
            "retOffset": -1,
        },
        {
            "g_windowMgr": (4, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0CAh
            b"\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_windowMgr
            b"\xC6\x05\xAB\xAB\xAB\xAB\xAB"  # 10 mov byte ptr g_session.field_
            b"\xE8"                      # 17 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 17,
            "windowId": 1,
            "retOffset": 0,
        },
        {
            "g_windowMgr": (6, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xB9\xAB\xAB\xAB\xAB"      # 0  mov ecx, offset g_windowMgr
            b"\xFF\x70\xAB"              # 5  push dword ptr [eax+6]
            b"\xE8"                      # 8  call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 8,
            "windowId": -1,
            "retOffset": -1,
        },
        {
            "g_windowMgr": (1, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 101h
            b"\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_windowMgr
            b"\x89\xB5\xAB\xAB\xFF\xFF"  # 10 mov [ebp+item.item_index], esi
            b"\x89\x85\xAB\xAB\xFF\xFF"  # 16 mov [ebp+item.price], eax
            b"\xE8"                      # 22 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 22,
            "windowId": 1,
            "retOffset": 0,
        },
        {
            "g_windowMgr": (6, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 3Ah
            b"\xB9\xAB\xAB\xAB\xAB"      # 2  mov ecx, offset g_windowMgr
            b"\xC7\x45\xAB\xAB\xAB\xAB\x00"  # 7  mov dword ptr [ebp+var_1C+4],
            b"\xC7\x45\xAB\xAB\xAB\x00\x00"  # 14 mov dword ptr [ebp+var_1C], 0
            b"\xC6\x45\xAB\xAB"          # 21 mov byte ptr [ebp+std_str_itemNam
            b"\xE8"                      # 25 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 25,
            "windowId": (1, 1),
            "retOffset": 0,
        },
        {
            "g_windowMgr": (3, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 102h
            b"\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_windowMgr
            b"\x89\x85\xAB\xAB\xAB\xFF"  # 10 mov [ebp+itemInfo.price], eax
            b"\xE8"                      # 16 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 16,
            "windowId": 1,
            "retOffset": 0,
        },
        {
            "g_windowMgr": (6, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 5Eh
            b"\xB9\xAB\xAB\xAB\xAB"      # 2  mov ecx, offset g_windowMgr
            b"\xD1\xEB"                  # 7  shr ebx, 1
            b"\xE8"                      # 9  call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 9,
            "windowId": (1, 1),
            "retOffset": 0,
        },
        {
            "g_windowMgr": (3, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 89h
            b"\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_windowMgr
            b"\xA3\xAB\xAB\xAB\xAB"      # 10 mov g_session.m_priority, eax
            b"\xC7\x05\xAB\xAB\xAB\xAB\xAB\xAB\xAB\x00"  # 15 mov g_session.m_d
            b"\xE8"                      # 25 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 25,
            "windowId": 1,
            "retOffset": 0,
        },
        {
            "g_windowMgr": (6, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 89h
            b"\x0F\xBF\x46\xAB"          # 5  movsx eax, word ptr [esi+2]
            b"\xB9\xAB\xAB\xAB\xAB"      # 9  mov ecx, offset g_windowMgr
            b"\xA3\xAB\xAB\xAB\xAB"      # 14 mov g_session.m_priority, eax
            b"\xE8"                      # 19 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 19,
            "windowId": 1,
            "retOffset": 0,
        },
        {
            "g_windowMgr": (10, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 100h
            b"\x0F\xBF\x5F\xAB"          # 5  movsx ebx, word ptr [edi+2]
            b"\x83\xEB\xAB"              # 9  sub ebx, 5
            b"\xC1\xEB\xAB"              # 12 shr ebx, 3
            b"\xB9\xAB\xAB\xAB\xAB"      # 15 mov ecx, offset g_windowMgr
            b"\x89\x7D\xAB"              # 20 mov [ebp+var_48], edi
            b"\x89\x5D\xAB"              # 23 mov [ebp+cnt_1], ebx
            b"\xE8"                      # 26 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 26,
            "windowId": 1,
            "retOffset": 0,
        },
        {
            "g_windowMgr": (16, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0A4h
            b"\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_windowMgr
            b"\xA3\xAB\xAB\xAB\xAB"      # 10 mov g_session.tickCount, eax
            b"\xE8"                      # 15 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 15,
            "windowId": 1,
            "retOffset": 0,
        },
        {
            "g_windowMgr": (6, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 16h
            b"\xB9\xAB\xAB\xAB\xAB"      # 2  mov ecx, offset g_windowMgr
            b"\xC7\x80\xAB\xAB\xAB\x00\xAB\xAB\xAB\x00"  # 7  mov dword ptr [ea
            b"\xE8"                      # 17 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 17,
            "windowId": (1, 1),
            "retOffset": 0,
        },
        {
            "g_windowMgr": (3, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 5Ah
            b"\x0F\xBF\x7E\xAB"          # 2  movsx edi, word ptr [esi+2]
            b"\x83\xEF\xAB"              # 6  sub edi, 4
            b"\xB9\xAB\xAB\xAB\xAB"      # 9  mov ecx, offset g_windowMgr
            b"\xD1\xEF"                  # 14 shr edi, 1
            b"\xE8"                      # 16 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 16,
            "windowId": (1, 1),
            "retOffset": 0,
        },
        {
            "g_windowMgr": (10, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 0Ch
            b"\xB9\xAB\xAB\xAB\xAB"      # 2  mov ecx, offset g_windowMgr
            b"\x66\x89\x46\xAB"          # 7  mov [esi+1Bh], ax
            b"\xE8"                      # 11 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 11,
            "windowId": (1, 1),
            "retOffset": 0,
        },
        {
            "g_windowMgr": (3, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 69h
            b"\xB9\xAB\xAB\xAB\xAB"      # 2  mov ecx, offset g_windowMgr
            b"\xC6\x45\xAB\xAB"          # 7  mov [ebp+var_8], 0
            b"\x89\x86\xAB\xAB\xAB\x00"  # 11 mov [esi+2A4h], eax
            b"\xE8"                      # 17 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 17,
            "windowId": (1, 1),
            "retOffset": 0,
        },
        {
            "g_windowMgr": (3, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 20h
            b"\xB9\xAB\xAB\xAB\xAB"      # 2  mov ecx, offset g_windowMgr
            b"\x66\x0F\xD6\x45\xAB"      # 7  movq [ebp+var_10], xmm0
            b"\xC6\x45\xAB\xAB"          # 12 mov [ebp+var_8], 0
            b"\xE8"                      # 16 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 16,
            "windowId": (1, 1),
            "retOffset": 0,
        },
        {
            "g_windowMgr": (3, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 10h
            b"\xB9\xAB\xAB\xAB\xAB"      # 2  mov ecx, offset g_windowMgr
            b"\xC6\x84\x35\xAB\xAB\xAB\xFF\xAB"  # 7  mov [ebp+esi+dst], 0
            b"\xE8"                      # 15 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 15,
            "windowId": (1, 1),
            "retOffset": 0,
        },
        {
            "g_windowMgr": (3, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 4Ah
            b"\x0F\xB7\x78\xAB"          # 2  movzx edi, word ptr [eax+2]
            b"\x83\xEF\xAB"              # 6  sub edi, 4
            b"\xB9\xAB\xAB\xAB\xAB"      # 9  mov ecx, offset g_windowMgr
            b"\x89\x85\xAB\xAB\xAB\xFF"  # 14 mov [ebp+var_F4], eax
            b"\xD1\xEF"                  # 20 shr edi, 1
            b"\xE8"                      # 22 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 22,
            "windowId": (1, 1),
            "retOffset": 0,
        },
        {
            "g_windowMgr": (10, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 49h
            b"\x0F\xB7\x7E\xAB"          # 2  movzx edi, word ptr [esi+2]
            b"\x83\xEF\xAB"              # 6  sub edi, 4
            b"\xB9\xAB\xAB\xAB\xAB"      # 9  mov ecx, offset g_windowMgr
            b"\xD1\xAB"                  # 14 shr edi, 1
            b"\xE8"                      # 16 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 16,
            "windowId": (1, 1),
            "retOffset": 0,
        },
        {
            "g_windowMgr": (10, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 48h
            b"\xF3\x0F\x7E\x46\xAB"      # 2  movq xmm0, qword ptr [esi+6]
            b"\x66\x0F\xD6\x45\xAB"      # 7  movq [ebp+var_20], xmm0
            b"\xF3\x0F\x7E\x46\xAB"      # 12 movq xmm0, qword ptr [esi+0Eh]
            b"\x66\x0F\xD6\x45\xAB"      # 17 movq [ebp+var_18], xmm0
            b"\xF3\x0F\x7E\x46\xAB"      # 22 movq xmm0, qword ptr [esi+16h]
            b"\xB9\xAB\xAB\xAB\xAB"      # 27 mov ecx, offset g_windowMgr
            b"\x66\x0F\xD6\x45\xAB"      # 32 movq [ebp+var_10], xmm0
            b"\xC6\x45\xAB\xAB"          # 37 mov [ebp+var_8], 0
            b"\xE8"                      # 41 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 41,
            "windowId": (1, 1),
            "retOffset": 0,
        },
        {
            "g_windowMgr": (28, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 55h
            b"\xB9\xAB\xAB\xAB\xAB"      # 2  mov ecx, offset g_windowMgr
            b"\x85\xC0"                  # 7  test eax, eax
            b"\x74\xAB"                  # 9  jz short loc_ADB449
            b"\xE8"                      # 11 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 11,
            "windowId": (1, 1),
            "retOffset": 0,
        },
        {
            "g_windowMgr": (3, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x85\xAB\xAB\xAB\x00"  # 0  jnz loc_453AB5
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 132h
            b"\xB9\xAB\xAB\xAB\xAB"      # 11 mov ecx, offset g_windowMgr
            b"\xE8"                      # 16 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 16,
            "windowId": 7,
            "retOffset": 6,
        },
        {
            "g_windowMgr": (12, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x84\xAB\xAB\xAB\x00"  # 0  jz loc_453AB5
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 13Ah
            b"\xB9\xAB\xAB\xAB\xAB"      # 11 mov ecx, offset g_windowMgr
            b"\xE8"                      # 16 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 16,
            "windowId": 7,
            "retOffset": 6,
        },
        {
            "g_windowMgr": (12, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x8D\xAB\xAB\xAB\x00"  # 0  jge loc_4E013B
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 116h
            b"\xB9\xAB\xAB\xAB\xAB"      # 11 mov ecx, offset g_windowMgr
            b"\xE8"                      # 16 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 16,
            "windowId": 7,
            "retOffset": 6,
        },
        {
            "g_windowMgr": (12, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x50\xAB"              # 0  call dword ptr [eax+34h]
            b"\x68\xAB\xAB\xAB\xAB"      # 3  push 11Bh
            b"\xB9\xAB\xAB\xAB\xAB"      # 8  mov ecx, offset g_windowMgr
            b"\xE8"                      # 13 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 13,
            "windowId": 4,
            "retOffset": 3,
        },
        {
            "g_windowMgr": (9, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x85\xAB\xAB\xAB\xFF"  # 0  jnz loc_501B99
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 11Fh
            b"\xB9\xAB\xAB\xAB\xAB"      # 11 mov ecx, offset g_windowMgr
            b"\xE8"                      # 16 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 16,
            "windowId": 7,
            "retOffset": 6,
        },
        {
            "g_windowMgr": (12, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xE9\xAB\xAB\xAB\x00"      # 0  jmp loc_540D38
            b"\x68\xAB\xAB\xAB\xAB"      # 5  push 0CBh
            b"\xB9\xAB\xAB\xAB\xAB"      # 10 mov ecx, offset g_windowMgr
            b"\xE8"                      # 15 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 15,
            "windowId": 6,
            "retOffset": 5,
        },
        {
            "g_windowMgr": (11, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\xF1"                  # 0  mov esi, ecx
            b"\x68\xAB\xAB\xAB\xAB"      # 2  push 114h
            b"\xB9\xAB\xAB\xAB\xAB"      # 7  mov ecx, offset g_windowMgr
            b"\xE8"                      # 12 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 12,
            "windowId": 3,
            "retOffset": 2,
        },
        {
            "g_windowMgr": (8, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xE9\xAB\xAB\xAB\xFF"      # 0  jmp loc_5A8548
            b"\x68\xAB\xAB\xAB\xAB"      # 5  push 104h
            b"\xB9\xAB\xAB\xAB\xAB"      # 10 mov ecx, offset g_windowMgr
            b"\xE8"                      # 15 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 15,
            "windowId": 6,
            "retOffset": 5,
        },
        {
            "g_windowMgr": (11, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x24\x85\x64\x80\x62\x00"  # 0  jmp off_628064[eax*4]
            b"\x68\xAB\xAB\xAB\xAB"      # 7  push 84h
            b"\xB9\xAB\xAB\xAB\xAB"      # 12 mov ecx, offset g_windowMgr
            b"\xE8"                      # 17 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 17,
            "windowId": 8,
            "retOffset": 7,
        },
        {
            "g_windowMgr": (13, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x7E\xAB"                  # 0  jle short loc_67EFB5
            b"\x68\xAB\xAB\xAB\xAB"      # 2  push 87h
            b"\xB9\xAB\xAB\xAB\xAB"      # 7  mov ecx, offset g_windowMgr
            b"\xE8"                      # 12 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 12,
            "windowId": 3,
            "retOffset": 2,
        },
        {
            "g_windowMgr": (8, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x50\xAB"              # 0  call dword ptr [eax+18h]
            b"\xEB\xAB"                  # 3  jmp short loc_68BD45
            b"\x68\xAB\xAB\xAB\xAB"      # 5  push 0E6h
            b"\xB9\xAB\xAB\xAB\xAB"      # 10 mov ecx, offset g_windowMgr
            b"\xE8"                      # 15 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 15,
            "windowId": 6,
            "retOffset": 5,
        },
        {
            "g_windowMgr": (11, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x8E\xAB\xAB\xAB\x00"  # 0  jle loc_68EA08
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 87h
            b"\xB9\xAB\xAB\xAB\xAB"      # 11 mov ecx, offset g_windowMgr
            b"\xE8"                      # 16 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 16,
            "windowId": 7,
            "retOffset": 6,
        },
        {
            "g_windowMgr": (12, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x8C\xAB\xAB\xAB\xFF"  # 0  jl loc_9F9F40
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 0A0h
            b"\xB9\xAB\xAB\xAB\xAB"      # 11 mov ecx, offset g_windowMgr
            b"\xE8"                      # 16 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 16,
            "windowId": 7,
            "retOffset": 6,
        },
        {
            "g_windowMgr": (12, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x90\xAB\xAB\xAB\x00"  # 0  call dword ptr [eax+88h]
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 0A1h
            b"\xB9\xAB\xAB\xAB\xAB"      # 11 mov ecx, offset g_windowMgr
            b"\xE8"                      # 16 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 16,
            "windowId": 7,
            "retOffset": 6,
        },
        {
            "g_windowMgr": (12, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xCC\xCC\xCC\xCC\xCC"      # 0  align 10h
            b"\x68\xAB\xAB\xAB\xAB"      # 5  push 103h
            b"\xB9\xAB\xAB\xAB\xAB"      # 10 mov ecx, offset g_windowMgr
            b"\xE8"                      # 15 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 15,
            "windowId": 6,
            "retOffset": 5,
        },
        {
            "g_windowMgr": (11, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 0
            b"\x68\xAB\xAB\xAB\xAB"      # 2  push 0B0h
            b"\xB9\xAB\xAB\xAB\xAB"      # 7  mov ecx, offset g_windowMgr
            b"\xE8"                      # 12 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 12,
            "windowId": 3,
            "retOffset": 2,
        },
        {
            "g_windowMgr": (8, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\x7D\xAB"              # 0  mov edi, [ebp+a3]
            b"\x68\xAB\xAB\xAB\xAB"      # 3  push 100h
            b"\xB9\xAB\xAB\xAB\xAB"      # 8  mov ecx, offset g_windowMgr
            b"\xE8"                      # 13 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 13,
            "windowId": 4,
            "retOffset": 3,
        },
        {
            "g_windowMgr": (9, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x84\xAB\xAB\xAB\x00"  # 0  jz loc_4535D0
            b"\x6A\xAB"                  # 6  push 1Ah
            b"\xB9\xAB\xAB\xAB\xAB"      # 8  mov ecx, offset g_windowMgr
            b"\xE8"                      # 13 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 13,
            "windowId": (7, 1),
            "retOffset": 6,
        },
        {
            "g_windowMgr": (9, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x85\xAB\xAB\xAB\xFF"  # 0  jnz loc_4F0A0D
            b"\x6A\xAB"                  # 6  push 70h
            b"\xB9\xAB\xAB\xAB\xAB"      # 8  mov ecx, offset g_windowMgr
            b"\xE8"                      # 13 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 13,
            "windowId": (7, 1),
            "retOffset": 6,
        },
        {
            "g_windowMgr": (9, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x74\xAB"                  # 0  jz short loc_4F5B22
            b"\x6A\xAB"                  # 2  push 0Ch
            b"\xB9\xAB\xAB\xAB\xAB"      # 4  mov ecx, offset g_windowMgr
            b"\xE8"                      # 9  call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 9,
            "windowId": (3, 1),
            "retOffset": 2,
        },
        {
            "g_windowMgr": (5, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x7F\xAB"                  # 0  jg short loc_50B060
            b"\x6A\xAB"                  # 2  push 0Ch
            b"\xB9\xAB\xAB\xAB\xAB"      # 4  mov ecx, offset g_windowMgr
            b"\xE8"                      # 9  call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 9,
            "windowId": (3, 1),
            "retOffset": 2,
        },
        {
            "g_windowMgr": (5, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x7E\xAB"                  # 0  jle short loc_50B1DC
            b"\x6A\xAB"                  # 2  push 0Ch
            b"\xB9\xAB\xAB\xAB\xAB"      # 4  mov ecx, offset g_windowMgr
            b"\xE8"                      # 9  call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 9,
            "windowId": (3, 1),
            "retOffset": 2,
        },
        {
            "g_windowMgr": (5, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x77\xAB"                  # 0  ja short loc_50B1DC
            b"\x6A\xAB"                  # 2  push 0Ch
            b"\xB9\xAB\xAB\xAB\xAB"      # 4  mov ecx, offset g_windowMgr
            b"\xE8"                      # 9  call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 9,
            "windowId": (3, 1),
            "retOffset": 2,
        },
        {
            "g_windowMgr": (5, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x84\xAB\xAB\xAB\xFF"  # 0  jz loc_5564E2
            b"\x6A\xAB"                  # 6  push 0Ch
            b"\xB9\xAB\xAB\xAB\xAB"      # 8  mov ecx, offset g_windowMgr
            b"\xE8"                      # 13 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 13,
            "windowId": (7, 1),
            "retOffset": 6,
        },
        {
            "g_windowMgr": (9, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x7C\xAB"                  # 0  jl short loc_57F1CD
            b"\x56"                      # 2  push esi
            b"\x6A\xAB"                  # 3  push 2Eh
            b"\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_windowMgr
            b"\xE8"                      # 10 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 10,
            "windowId": (4, 1),
            "retOffset": 3,
        },
        {
            "g_windowMgr": (6, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\xF1"                  # 0  mov esi, ecx
            b"\x6A\xAB"                  # 2  push 0Ch
            b"\xB9\xAB\xAB\xAB\xAB"      # 4  mov ecx, offset g_windowMgr
            b"\xE8"                      # 9  call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 9,
            "windowId": (3, 1),
            "retOffset": 2,
        },
        {
            "g_windowMgr": (5, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x53"                      # 0  push ebx
            b"\x57"                      # 1  push edi
            b"\x6A\xAB"                  # 2  push 1Ah
            b"\xB9\xAB\xAB\xAB\xAB"      # 4  mov ecx, offset g_windowMgr
            b"\xE8"                      # 9  call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 9,
            "windowId": (3, 1),
            "retOffset": 2,
        },
        {
            "g_windowMgr": (5, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xE9\xAB\xAB\xAB\x00"      # 0  jmp loc_5A63AA
            b"\x6A\xAB"                  # 5  push 1Eh
            b"\xB9\xAB\xAB\xAB\xAB"      # 7  mov ecx, offset g_windowMgr
            b"\xE8"                      # 12 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 12,
            "windowId": (6, 1),
            "retOffset": 5,
        },
        {
            "g_windowMgr": (8, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x85\xAB\xAB\xAB\x00"  # 0  jnz loc_5A6BE4
            b"\x6A\xAB"                  # 6  push 72h
            b"\xB9\xAB\xAB\xAB\xAB"      # 8  mov ecx, offset g_windowMgr
            b"\xE8"                      # 13 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 13,
            "windowId": (7, 1),
            "retOffset": 6,
        },
        {
            "g_windowMgr": (9, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x75\xAB"                  # 0  jnz short loc_5A7BE7
            b"\x6A\xAB"                  # 2  push 7Eh
            b"\xB9\xAB\xAB\xAB\xAB"      # 4  mov ecx, offset g_windowMgr
            b"\xE8"                      # 9  call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 9,
            "windowId": (3, 1),
            "retOffset": 2,
        },
        {
            "g_windowMgr": (5, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x7C\xAB"                  # 0  jl short loc_5D8420
            b"\x6A\xAB"                  # 2  push 0Ch
            b"\xB9\xAB\xAB\xAB\xAB"      # 4  mov ecx, offset g_windowMgr
            b"\xE8"                      # 9  call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 9,
            "windowId": (3, 1),
            "retOffset": 2,
        },
        {
            "g_windowMgr": (5, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x8E\xAB\xAB\xAB\x00"  # 0  jle loc_5D9A4D
            b"\x6A\xAB"                  # 6  push 28h
            b"\xB9\xAB\xAB\xAB\xAB"      # 8  mov ecx, offset g_windowMgr
            b"\xE8"                      # 13 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 13,
            "windowId": (7, 1),
            "retOffset": 6,
        },
        {
            "g_windowMgr": (9, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x75\xAB"                  # 0  jnz short loc_5E11E7
            b"\x56"                      # 2  push esi
            b"\x6A\xAB"                  # 3  push 0Bh
            b"\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_windowMgr
            b"\xE8"                      # 10 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 10,
            "windowId": (4, 1),
            "retOffset": 3,
        },
        {
            "g_windowMgr": (6, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x24\xB5\xAB\xAB\xAB\x00"  # 0  jmp off_65DC1C[esi*4]
            b"\x6A\xAB"                  # 7  push 3Ch
            b"\xB9\xAB\xAB\xAB\xAB"      # 9  mov ecx, offset g_windowMgr
            b"\xE8"                      # 14 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 14,
            "windowId": (8, 1),
            "retOffset": 7,
        },
        {
            "g_windowMgr": (10, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x24\x85\xAB\xAB\xAB\x00"  # 0  jmp off_6E309C[eax*4]
            b"\x6A\xAB"                  # 7  push 3Ch
            b"\xB9\xAB\xAB\xAB\xAB"      # 9  mov ecx, offset g_windowMgr
            b"\xE8"                      # 14 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 14,
            "windowId": (8, 1),
            "retOffset": 7,
        },
        {
            "g_windowMgr": (10, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xE9\xAB\xAB\xAB\xFF"      # 0  jmp loc_9DBAE3
            b"\x6A\xAB"                  # 5  push 67h
            b"\xB9\xAB\xAB\xAB\xAB"      # 7  mov ecx, offset g_windowMgr
            b"\xE8"                      # 12 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 12,
            "windowId": (6, 1),
            "retOffset": 5,
        },
        {
            "g_windowMgr": (8, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x92\xAB\xAB\xAB\x00"  # 0  call dword ptr [edx+88h]
            b"\x6A\xAB"                  # 6  push 29h
            b"\xB9\xAB\xAB\xAB\xAB"      # 8  mov ecx, offset g_windowMgr
            b"\xE8"                      # 13 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 13,
            "windowId": (7, 1),
            "retOffset": 6,
        },
        {
            "g_windowMgr": (9, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xCC\xCC\xCC\xCC"          # 0  align 10h
            b"\x6A\xAB"                  # 4  push 5Fh
            b"\xB9\xAB\xAB\xAB\xAB"      # 6  mov ecx, offset g_windowMgr
            b"\xE8"                      # 11 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 11,
            "windowId": (5, 1),
            "retOffset": 4,
        },
        {
            "g_windowMgr": (7, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x8C\xAB\xAB\xAB\xFF"  # 0  jl loc_A06780
            b"\x6A\xAB"                  # 6  push 1Ch
            b"\xB9\xAB\xAB\xAB\xAB"      # 8  mov ecx, offset g_windowMgr
            b"\xE8"                      # 13 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 13,
            "windowId": (7, 1),
            "retOffset": 6,
        },
        {
            "g_windowMgr": (9, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\xB7\x76\xAB"          # 0  movzx esi, word ptr [esi+4]
            b"\x6A\xAB"                  # 4  push 3Ah
            b"\xB9\xAB\xAB\xAB\xAB"      # 6  mov ecx, offset g_windowMgr
            b"\xE8"                      # 11 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 11,
            "windowId": (5, 1),
            "retOffset": 4,
        },
        {
            "g_windowMgr": (7, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\xEC"                  # 0  mov ebp, esp
            b"\x56"                      # 2  push esi
            b"\x6A\xAB"                  # 3  push 21h
            b"\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_windowMgr
            b"\xE8"                      # 10 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 10,
            "windowId": (4, 1),
            "retOffset": 3,
        },
        {
            "g_windowMgr": (6, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x90\xAB\xAB\xAB\x00"  # 0  call dword ptr [eax+88h]
            b"\x6A\xAB"                  # 6  push 17h
            b"\xB9\xAB\xAB\xAB\xAB"      # 8  mov ecx, offset g_windowMgr
            b"\xE8"                      # 13 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 13,
            "windowId": (7, 1),
            "retOffset": 6,
        },
        {
            "g_windowMgr": (9, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xC7\x81\xAB\xAB\xAB\x00\xAB\xAB\xAB\x00"  # 0  mov [ecx+CGameMod
            b"\x6A\xAB"                  # 10 push 19h
            b"\xB9\xAB\xAB\xAB\xAB"      # 12 mov ecx, offset g_windowMgr
            b"\xE8"                      # 17 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 17,
            "windowId": (11, 1),
            "retOffset": 10,
        },
        {
            "g_windowMgr": (13, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x7E\xAB"                  # 0  jle short loc_A1EC9F
            b"\x57"                      # 2  push edi
            b"\x6A\xAB"                  # 3  push 63h
            b"\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_windowMgr
            b"\xE8"                      # 10 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 10,
            "windowId": (4, 1),
            "retOffset": 3,
        },
        {
            "g_windowMgr": (6, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\xEC"                  # 0  mov ebp, esp
            b"\x6A\xAB"                  # 2  push 77h
            b"\xB9\xAB\xAB\xAB\xAB"      # 4  mov ecx, offset g_windowMgr
            b"\xE8"                      # 9  call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 9,
            "windowId": (3, 1),
            "retOffset": 2,
        },
        {
            "g_windowMgr": (5, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x89\x81\xAB\xAB\xAB\x00"  # 0  mov [ecx+2FCh], eax
            b"\x6A\xAB"                  # 6  push 10h
            b"\xB9\xAB\xAB\xAB\xAB"      # 8  mov ecx, offset g_windowMgr
            b"\xE8"                      # 13 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 13,
            "windowId": (7, 1),
            "retOffset": 6,
        },
        {
            "g_windowMgr": (9, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\xBD\xAB\xAB\xAB\xFF"  # 0  mov edi, [ebp+var_134]
            b"\x6A\xAB"                  # 6  push 2Bh
            b"\xB9\xAB\xAB\xAB\xAB"      # 8  mov ecx, offset g_windowMgr
            b"\xE8"                      # 13 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 13,
            "windowId": (7, 1),
            "retOffset": 6,
        },
        {
            "g_windowMgr": (9, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\xC8"                  # 0  mov ecx, eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 2  call std_string_substr
            b"\x6A\xAB"                  # 7  push 0Ch
            b"\xB9\xAB\xAB\xAB\xAB"      # 9  mov ecx, offset g_windowMgr
            b"\xE8"                      # 14 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 14,
            "windowId": (8, 1),
            "retOffset": 7,
        },
        {
            "g_windowMgr": (10, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8D\x8D\xAB\xAB\xAB\xFF"  # 0  lea ecx, [ebp+var_100]
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call std_string_destructor
            b"\x6A\xAB"                  # 11 push 0Ch
            b"\xB9\xAB\xAB\xAB\xAB"      # 13 mov ecx, offset g_windowMgr
            b"\xE8"                      # 18 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 18,
            "windowId": (12, 1),
            "retOffset": 11,
        },
        {
            "g_windowMgr": (14, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xC7\x85\xAB\xAB\xAB\xFF\xAB\xAB\xAB\x00"  # 0  mov [ebp+item.ref
            b"\xE8\xAB\xAB\xAB\xAB"      # 10 call ITEM_INFO_SetItemId
            b"\x6A\xAB"                  # 15 push 52h
            b"\xB9\xAB\xAB\xAB\xAB"      # 17 mov ecx, offset g_windowMgr
            b"\xE8"                      # 22 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 22,
            "windowId": (16, 1),
            "retOffset": 15,
        },
        {
            "g_windowMgr": (18, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x50"                      # 0  push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 1  call sub_42F350
            b"\x6A\xAB"                  # 6  push 12h
            b"\xB9\xAB\xAB\xAB\xAB"      # 8  mov ecx, offset g_windowMgr
            b"\xE8"                      # 13 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 13,
            "windowId": (7, 1),
            "retOffset": 6,
        },
        {
            "g_windowMgr": (9, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x89\xB5\xAB\xAB\xAB\xFF"  # 0  mov [ebp+m_menuIdList_1], esi
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_42F350
            b"\x6A\xAB"                  # 11 push 12h
            b"\xB9\xAB\xAB\xAB\xAB"      # 13 mov ecx, offset g_windowMgr
            b"\xE8"                      # 18 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 18,
            "windowId": (12, 1),
            "retOffset": 11,
        },
        {
            "g_windowMgr": (14, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xC6\x45\xAB\xAB"          # 0  mov [ebp+var_14], 0
            b"\xE8\xAB\xAB\xAB\xAB"      # 4  call CSession_GetCharName
            b"\x6A\xAB"                  # 9  push 1Ch
            b"\xB9\xAB\xAB\xAB\xAB"      # 11 mov ecx, offset g_windowMgr
            b"\xE8"                      # 16 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 16,
            "windowId": (10, 1),
            "retOffset": 9,
        },
        {
            "g_windowMgr": (12, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xC6\x84\x35\xAB\xAB\xAB\xFF\xAB"  # 0  mov [ebp+esi+dialogSay],
            b"\xE8\xAB\xAB\xAB\xAB"      # 8  call UIWindowMgr_DeleteWindow
            b"\x6A\xAB"                  # 13 push 11h
            b"\xB9\xAB\xAB\xAB\xAB"      # 15 mov ecx, offset g_windowMgr
            b"\xE8"                      # 20 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 20,
            "windowId": (14, 1),
            "retOffset": 13,
        },
        {
            "g_windowMgr": (16, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8D\x4D\xAB"              # 0  lea ecx, [ebp+var_18]
            b"\xE8\xAB\xAB\xAB\xAB"      # 3  call sub_6BC280
            b"\x6A\xAB"                  # 8  push 5Fh
            b"\xB9\xAB\xAB\xAB\xAB"      # 10 mov ecx, offset g_windowMgr
            b"\xE8"                      # 15 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 15,
            "windowId": (9, 1),
            "retOffset": 8,
        },
        {
            "g_windowMgr": (11, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\xCE"                  # 0  mov ecx, esi
            b"\xE8\xAB\xAB\xAB\xAB"      # 2  call sub_692400
            b"\x6A\xAB"                  # 7  push 2Ch
            b"\xB9\xAB\xAB\xAB\xAB"      # 9  mov ecx, offset g_windowMgr
            b"\xE8"                      # 14 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 14,
            "windowId": (8, 1),
            "retOffset": 7,
        },
        {
            "g_windowMgr": (10, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8D\x8B\xAB\xAB\xAB\x00"  # 0  lea ecx, [ebx+6980h]
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call std_string_opearator_copy
            b"\x6A\xAB"                  # 11 push 10h
            b"\xB9\xAB\xAB\xAB\xAB"      # 13 mov ecx, offset g_windowMgr
            b"\xE8"                      # 18 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 18,
            "windowId": (12, 1),
            "retOffset": 11,
        },
        {
            "g_windowMgr": (14, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 1Ah
            b"\xB9\xAB\xAB\xAB\xAB"      # 2  mov ecx, offset g_windowMgr
            b"\xE8\xAB\xAB\xAB\xAB"      # 7  call UIWindowMgr_DeleteWindow
            b"\x6A\xAB"                  # 12 push 1Ah
            b"\xB9\xAB\xAB\xAB\xAB"      # 14 mov ecx, offset g_windowMgr
            b"\xE8"                      # 19 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 19,
            "windowId": (13, 1),
            "retOffset": 12,
        },
        {
            "g_windowMgr": (15, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x77\xAB"                  # 0  ja short loc_621F68
            b"\xB9\xAB\xAB\xAB\xAB"      # 2  mov ecx, offset g_modeMgr
            b"\xE8\xAB\xAB\xAB\xAB"      # 7  call CModeMgr_GetGameMode
            b"\x6A\xAB"                  # 12 push 0Ch
            b"\xB9\xAB\xAB\xAB\xAB"      # 14 mov ecx, offset g_windowMgr
            b"\xE8"                      # 19 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 19,
            "windowId": (13, 1),
            "retOffset": 12,
        },
        {
            "g_windowMgr": (15, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x74\xAB"                  # 0  jz short loc_683D9E
            b"\xB9\xAB\xAB\xAB\xAB"      # 2  mov ecx, offset g_modeMgr
            b"\xE8\xAB\xAB\xAB\xAB"      # 7  call CModeMgr_GetGameMode
            b"\x6A\xAB"                  # 12 push 0Ch
            b"\xB9\xAB\xAB\xAB\xAB"      # 14 mov ecx, offset g_windowMgr
            b"\xE8"                      # 19 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 19,
            "windowId": (13, 1),
            "retOffset": 12,
        },
        {
            "g_windowMgr": (15, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x50"                      # 0  push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 1  mov ecx, offset g_session.m_party
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call std_string_assign
            b"\x6A\xAB"                  # 11 push 23h
            b"\xB9\xAB\xAB\xAB\xAB"      # 13 mov ecx, offset g_windowMgr
            b"\xE8"                      # 18 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 18,
            "windowId": (12, 1),
            "retOffset": 11,
        },
        {
            "g_windowMgr": (14, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x5D"                      # 0  pop ebp
            b"\xC2\xAB\x00"              # 1  retn 18h
            b"\x6A\xAB"                  # 4  push 3Dh
            b"\xB9\xAB\xAB\xAB\xAB"      # 6  mov ecx, offset g_windowMgr
            b"\xE8"                      # 11 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 11,
            "windowId": (5, 1),
            "retOffset": 4,
        },
        {
            "g_windowMgr": (7, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x52\xAB"              # 0  call dword ptr [edx+18h]
            b"\x5E"                      # 3  pop esi
            b"\xC2\xAB\x00"              # 4  retn 8
            b"\x6A\xAB"                  # 7  push 1Dh
            b"\xB9\xAB\xAB\xAB\xAB"      # 9  mov ecx, offset g_windowMgr
            b"\xE8"                      # 14 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 14,
            "windowId": (8, 1),
            "retOffset": 7,
        },
        {
            "g_windowMgr": (10, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x50\xAB"              # 0  call dword ptr [eax+18h]
            b"\xEB\xAB"                  # 3  jmp short loc_68436E
            b"\x6A\xAB"                  # 5  push 0Ch
            b"\xB9\xAB\xAB\xAB\xAB"      # 7  mov ecx, offset g_windowMgr
            b"\xE8"                      # 12 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 12,
            "windowId": (6, 1),
            "retOffset": 5,
        },
        {
            "g_windowMgr": (8, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xE8\xAB\xAB\xAB\xAB"      # 0  call CRagConnection_SendPacket
            b"\xEB\xAB"                  # 5  jmp short loc_684A11
            b"\x6A\xAB"                  # 7  push 0Ch
            b"\xB9\xAB\xAB\xAB\xAB"      # 9  mov ecx, offset g_windowMgr
            b"\xE8"                      # 14 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 14,
            "windowId": (8, 1),
            "retOffset": 7,
        },
        {
            "g_windowMgr": (10, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x90\xAB\xAB\xAB\x00"  # 0  call dword ptr [eax+88h]
            b"\xEB\xAB"                  # 6  jmp short loc_684CDE
            b"\x6A\xAB"                  # 8  push 0Ch
            b"\xB9\xAB\xAB\xAB\xAB"      # 10 mov ecx, offset g_windowMgr
            b"\xE8"                      # 15 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 15,
            "windowId": (9, 1),
            "retOffset": 8,
        },
        {
            "g_windowMgr": (11, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x03\x8D\xAB\xAB\xAB\xFF"  # 0  add ecx, [ebp+var_180+2]
            b"\xEB\xCF"                  # 6  jmp short loc_687690
            b"\x6A\xAB"                  # 8  push 16h
            b"\xB9\xAB\xAB\xAB\xAB"      # 10 mov ecx, offset g_windowMgr
            b"\xE8"                      # 15 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 15,
            "windowId": (9, 1),
            "retOffset": 8,
        },
        {
            "g_windowMgr": (11, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0AFh
            b"\xEB\xAB"                  # 5  jmp short loc_68D657
            b"\x6A\xAB"                  # 7  push 2Ah
            b"\xB9\xAB\xAB\xAB\xAB"      # 9  mov ecx, offset g_windowMgr
            b"\xE8"                      # 14 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 14,
            "windowId": (8, 1),
            "retOffset": 7,
        },
        {
            "g_windowMgr": (10, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\x1D\xAB\xAB\xAB\xAB"  # 0  mov ebx, g_language
            b"\xEB\xAB"                  # 6  jmp short loc_ADB380
            b"\x6A\xAB"                  # 8  push 33h
            b"\xB9\xAB\xAB\xAB\xAB"      # 10 mov ecx, offset g_windowMgr
            b"\xE8"                      # 15 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 15,
            "windowId": (9, 1),
            "retOffset": 8,
        },
        {
            "g_windowMgr": (11, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xB9\xAB\xAB\xAB\xAB"      # 0  mov ecx, offset g_windowMgr
            b"\x50"                      # 5  push eax
            b"\x6A\xAB"                  # 6  push 1
            b"\xE8\xAB\xAB\xAB\xAB"      # 8  call UIWindowMgr_SendMsg
            b"\x6A\xAB"                  # 13 push 1Ch
            b"\xB9\xAB\xAB\xAB\xAB"      # 15 mov ecx, offset g_windowMgr
            b"\xE8"                      # 20 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 20,
            "windowId": (14, 1),
            "retOffset": 13,
        },
        {
            "g_windowMgr": (16, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x75\xAB"              # 0  push [ebp+var_80]
            b"\x6A\xAB"                  # 3  push 44h
            b"\xEB\xAB"                  # 5  jmp short loc_65D7C7
            b"\x6A\xAB"                  # 7  push 2Eh
            b"\xB9\xAB\xAB\xAB\xAB"      # 9  mov ecx, offset g_windowMgr
            b"\xE8"                      # 14 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 14,
            "windowId": (8, 1),
            "retOffset": 7,
        },
        {
            "g_windowMgr": (10, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\xB5\xAB\xAB\xAB\xFF"  # 0  push [ebp+dstSkill.skill_id]
            b"\x6A\xAB"                  # 6  push 44h
            b"\xEB\xAB"                  # 8  jmp short loc_68F6F6
            b"\x6A\xAB"                  # 10 push 2Eh
            b"\xB9\xAB\xAB\xAB\xAB"      # 12 mov ecx, offset g_windowMgr
            b"\xE8"                      # 17 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 17,
            "windowId": (11, 1),
            "retOffset": 10,
        },
        {
            "g_windowMgr": (13, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x75\xAB"                  # 0  jnz short loc_4EB20D
            b"\x68\xAB\xAB\xAB\xAB"      # 2  push 11Ah
            b"\xB9\xAB\xAB\xAB\xAB"      # 7  mov ecx, offset g_windowMgr
            b"\xE8"                      # 12 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 12,
            "windowId": 3,
            "retOffset": 2,
        },
        {
            "g_windowMgr": (8, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x85\xC0"                  # 0  test eax, eax
            b"\x75\xAB"                  # 2  jnz short loc_A37693
            b"\x56"                      # 4  push esi
            b"\x68\xAB\xAB\xAB\xAB"      # 5  push 11Dh
            b"\xB9\xAB\xAB\xAB\xAB"      # 10 mov ecx, offset g_windowMgr
            b"\xE8"                      # 15 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 15,
            "windowId": 6,
            "retOffset": 5,
        },
        {
            "g_windowMgr": (11, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\xBB\xAB\xAB\xAB\xAB\xAB"  # 0  cmp dword ptr [ebx+8Ch], 0
            b"\x74\xAB"                  # 7  jz short loc_57D6D3
            b"\x68\xAB\xAB\xAB\xAB"      # 9  push 0C9h
            b"\xB9\xAB\xAB\xAB\xAB"      # 14 mov ecx, offset g_windowMgr
            b"\xE8"                      # 19 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 19,
            "windowId": 10,
            "retOffset": 9,
        },
        {
            "g_windowMgr": (15, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\x3D\xAB\xAB\xAB\xAB\xAB"  # 0  cmp g_session.m_amIPartyMaste
            b"\x74\xAB"                  # 7  jz short loc_69A2A7
            b"\x68\xAB\xAB\xAB\xAB"      # 9  push 0D0h
            b"\xB9\xAB\xAB\xAB\xAB"      # 14 mov ecx, offset g_windowMgr
            b"\xE8"                      # 19 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 19,
            "windowId": 10,
            "retOffset": 9,
        },
        {
            "g_windowMgr": (15, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x85\xFF"                  # 0  test edi, edi
            b"\x74\xAB"                  # 2  jz short loc_6E9C62
            b"\x68\xAB\xAB\xAB\xAB"      # 4  push 114h
            b"\xB9\xAB\xAB\xAB\xAB"      # 9  mov ecx, offset g_windowMgr
            b"\xE8"                      # 14 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 14,
            "windowId": 5,
            "retOffset": 4,
        },
        {
            "g_windowMgr": (10, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xC7\x05\xAB\xAB\xAB\xAB\xAB\xAB\xAB\x00"  # 0  mov g_windowMgr.f
            b"\x74\xAB"                  # 10 jz short loc_9BD04E
            b"\x68\xAB\xAB\xAB\xAB"      # 12 push 0CAh
            b"\xB9\xAB\xAB\xAB\xAB"      # 17 mov ecx, offset g_windowMgr
            b"\xE8"                      # 22 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 22,
            "windowId": 13,
            "retOffset": 12,
        },
        {
            "g_windowMgr": (18, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x80\x3D\xAB\xAB\xAB\xAB\xAB"  # 0  cmp g_session.field_5540, 0
            b"\x74\xAB"                  # 7  jz short loc_9BD0F9
            b"\x68\xAB\xAB\xAB\xAB"      # 9  push 0B5h
            b"\xB9\xAB\xAB\xAB\xAB"      # 14 mov ecx, offset g_windowMgr
            b"\xE8"                      # 19 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 19,
            "windowId": 10,
            "retOffset": 9,
        },
        {
            "g_windowMgr": (15, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\x70\xAB"              # 0  mov esi, [eax+0Ch]
            b"\x74\xAB"                  # 3  jz short loc_9BE6AF
            b"\x68\xAB\xAB\xAB\xAB"      # 5  push 0BBh
            b"\xB9\xAB\xAB\xAB\xAB"      # 10 mov ecx, offset g_windowMgr
            b"\xE8"                      # 15 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 15,
            "windowId": 6,
            "retOffset": 5,
        },
        {
            "g_windowMgr": (11, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x90\xAB\xAB\xAB\x00"  # 0  call dword ptr [eax+88h]
            b"\xEB\xAB"                  # 6  jmp short loc_6E91DF
            b"\x6A\xAB"                  # 8  push 33h
            b"\xE8"                      # 10 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 10,
            "windowId": (9, 1),
            "retOffset": 8,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xE8\xAB\xAB\xAB\xAB"      # 0  call UIWindowMgr_SendMsg
            b"\xEB\xAB"                  # 5  jmp short loc_A1B09D
            b"\x6A\xAB"                  # 7  push 6Ah
            b"\xE8"                      # 9  call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 9,
            "windowId": (8, 1),
            "retOffset": 7,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xE8\xAB\xAB\xAB\xAB"      # 0  call UIWindowMgr_DeleteWindow
            b"\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_windowMgr
            b"\x6A\xAB"                  # 10 push 27h
            b"\xE8"                      # 12 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 12,
            "windowId": (11, 1),
            "retOffset": 10,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x33\xC0"                  # 0  xor eax, eax
            b"\xE9\xAB\xAB\xAB\x00"      # 2  jmp loc_5DEB8C
            b"\x6A\xAB"                  # 7  push 6Ah
            b"\xE8"                      # 9  call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 9,
            "windowId": (8, 1),
            "retOffset": 7,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x7E\xAB"                  # 0  jle short loc_A0151B
            b"\x53"                      # 2  push ebx
            b"\x6A\xAB"                  # 3  push 63h
            b"\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_windowMgr
            b"\xE8"                      # 10 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 10,
            "windowId": (4, 1),
            "retOffset": 3,
        },
        {
            "g_windowMgr": (6, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xC2\xAB\x00"              # 0  retn 4
            b"\x53"                      # 3  push ebx
            b"\x6A\xAB"                  # 4  push 6Fh
            b"\xB9\xAB\xAB\xAB\xAB"      # 6  mov ecx, offset g_windowMgr
            b"\xE8"                      # 11 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 11,
            "windowId": (5, 1),
            "retOffset": 4,
        },
        {
            "g_windowMgr": (7, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x85\xAB\xAB\xAB\xFF"  # 0  jnz loc_A18200
            b"\x5B"                      # 6  pop ebx
            b"\x68\xAB\xAB\xAB\xAB"      # 7  push 0FEh
            b"\xB9\xAB\xAB\xAB\xAB"      # 12 mov ecx, offset g_windowMgr
            b"\xE8"                      # 17 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 17,
            "windowId": 8,
            "retOffset": 7,
        },
        {
            "g_windowMgr": (13, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\xEC"                  # 0  mov ebp, esp
            b"\x57"                      # 2  push edi
            b"\x68\xAB\xAB\xAB\xAB"      # 3  push 10Bh
            b"\xB9\xAB\xAB\xAB\xAB"      # 8  mov ecx, offset g_windowMgr
            b"\xE8"                      # 13 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 13,
            "windowId": 4,
            "retOffset": 3,
        },
        {
            "g_windowMgr": (9, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xC7\x45\xAB\xAB\xAB\xAB\x00"  # 0  mov [ebp+std_string_ptr.m_len
            b"\xC6\x40\xAB\xAB"          # 7  mov byte ptr [eax+3Ch], 0
            b"\x6A\xAB"                  # 11 push 1Ch
            b"\xB9\xAB\xAB\xAB\xAB"      # 13 mov ecx, offset g_windowMgr
            b"\xE8"                      # 18 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 18,
            "windowId": (12, 1),
            "retOffset": 11,
        },
        {
            "g_windowMgr": (14, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xB9\xAB\xAB\xAB\xAB"      # 0  mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"      # 5  call sub_AAEEE0
            b"\x68\xAB\xAB\xAB\xAB"      # 10 push 10Fh
            b"\xB9\xAB\xAB\xAB\xAB"      # 15 mov ecx, offset g_windowMgr
            b"\xE8"                      # 20 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 20,
            "windowId": 11,
            "retOffset": 10,
        },
        {
            "g_windowMgr": (16, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\xC7\x85\xAB\xAB\xAB\xFF\xAB\xAB\xAB\x00"  # 0  mov [ebp+item.ref
            b"\xE8\xAB\xAB\xAB\xAB"      # 10 call ITEM_INFO_SetItemId
            b"\x68\xAB\xAB\xAB\xAB"      # 15 push 0FAh
            b"\xB9\xAB\xAB\xAB\xAB"      # 20 mov ecx, offset g_windowMgr
            b"\xE8"                      # 25 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 25,
            "windowId": 16,
            "retOffset": 15,
        },
        {
            "g_windowMgr": (21, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x50"                      # 0  push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 1  call UIWindowMgr_ErrorMsg
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 0D7h
            b"\xB9\xAB\xAB\xAB\xAB"      # 11 mov ecx, offset g_windowMgr
            b"\xE8"                      # 16 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 16,
            "windowId": 7,
            "retOffset": 6,
        },
        {
            "g_windowMgr": (12, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\xCE"                  # 0  mov ecx, esi
            b"\xE8\xAB\xAB\xAB\xAB"      # 2  call sub_88F4C0
            b"\x68\xAB\xAB\xAB\xAB"      # 7  push 0CBh
            b"\xB9\xAB\xAB\xAB\xAB"      # 12 mov ecx, offset g_windowMgr
            b"\xE8"                      # 17 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 17,
            "windowId": 8,
            "retOffset": 7,
        },
        {
            "g_windowMgr": (13, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\xC8"                  # 0  mov ecx, eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 2  call ReplaySomething_sub_874AA0
            b"\x68\xAB\xAB\xAB\xAB"      # 7  push 0C6h
            b"\xB9\xAB\xAB\xAB\xAB"      # 12 mov ecx, offset g_windowMgr
            b"\xE8"                      # 17 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 17,
            "windowId": 8,
            "retOffset": 7,
        },
        {
            "g_windowMgr": (13, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8D\x4D\xAB"              # 0  lea ecx, [ebp+var_18]
            b"\xE8\xAB\xAB\xAB\xAB"      # 3  call sub_4E4A90
            b"\x68\xAB\xAB\xAB\xAB"      # 8  push 0E3h
            b"\xB9\xAB\xAB\xAB\xAB"      # 13 mov ecx, offset g_windowMgr
            b"\xE8"                      # 18 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 18,
            "windowId": 9,
            "retOffset": 8,
        },
        {
            "g_windowMgr": (14, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 0  mov ecx, g_RodexSystemMgr
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_50CF90
            b"\x68\xAB\xAB\xAB\xAB"      # 11 push 115h
            b"\xB9\xAB\xAB\xAB\xAB"      # 16 mov ecx, offset g_windowMgr
            b"\xE8"                      # 21 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 21,
            "windowId": 12,
            "retOffset": 11,
        },
        {
            "g_windowMgr": (17, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x5D"                      # 0  pop ebp
            b"\xC2\xAB\x00"              # 1  retn 18h
            b"\x68\xAB\xAB\xAB\xAB"      # 4  push 0C8h
            b"\xB9\xAB\xAB\xAB\xAB"      # 9  mov ecx, offset g_windowMgr
            b"\xE8"                      # 14 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 14,
            "windowId": 5,
            "retOffset": 4,
        },
        {
            "g_windowMgr": (10, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x48\xC8"              # 0  cmovs ecx, eax
            b"\x89\x0D\xAB\xAB\xAB\xAB"  # 3  mov g_windowMgr.field_3244, ecx
            b"\x68\xAB\xAB\xAB\xAB"      # 9  push 112h
            b"\xB9\xAB\xAB\xAB\xAB"      # 14 mov ecx, offset g_windowMgr
            b"\xE8"                      # 19 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 19,
            "windowId": 10,
            "retOffset": 9,
        },
        {
            "g_windowMgr": (15, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\xF8\xAB"              # 0  cmp eax, 1
            b"\x75\xAB"                  # 3  jnz short loc_88C1B4
            b"\xE8\xAB\xAB\xAB\xAB"      # 5  call sub_88F4C0
            b"\x68\xAB\xAB\xAB\xAB"      # 10 push 0CBh
            b"\xB9\xAB\xAB\xAB\xAB"      # 15 mov ecx, offset g_windowMgr
            b"\xE8"                      # 20 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 20,
            "windowId": 11,
            "retOffset": 10,
        },
        {
            "g_windowMgr": (16, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x75\xAB"                  # 0  jnz short loc_A19D80
            b"\x5B"                      # 2  pop ebx
            b"\x6A\xAB"                  # 3  push 16h
            b"\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_windowMgr
            b"\xE8"                      # 10 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 10,
            "windowId": (4, 1),
            "retOffset": 3,
        },
        {
            "g_windowMgr": (6, False),
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push offset off_DB643C
            b"\x6A\xAB"                  # 5  push 0
            b"\x6A\xAB"                  # 7  push 2Dh
            b"\xB9\xAB\xAB\xAB\xAB"      # 9  mov ecx, offset g_windowMgr
            b"\xE8"                      # 14 call UIWindowMgr_MakeWindow
        ),
        {
            "fixedOffset": 14,
            "windowId": (8, 1),
            "retOffset": 7,
        },
        {
            "g_windowMgr": (10, False),
        }
    ],
]
