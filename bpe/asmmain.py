#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import struct


class Asm:
    from bpe.asm.opcodes import codes

    def __init__(self):
        self.pos = 0
        self.registers = dict()
        self.flags = dict()
        self.memory = dict()
        self.popCounter = 0
        self.pushCounter = 0
        self.runFlag = 0
        self.callFunctions = dict()
        self.lastCallFunctionAddr = 0
        self.lastCallFunctionPos = 0
        self.stacOAkAlign = 0
        self.packets = []
        self.debug = False
        self.debugMemory = False
        self.lastRegValue = None
        self.prevPacketId = 0
        self.prevLens = (0, 0)
        self.altPos = 0
        self.altLens = (0, 0)
        self.beforeSecondCall = None
        self.allCallAddrs = []
        self.cxSet = False
        # set if previous command was jump
        self.Jumped = False
        # set if any jumps was executed before
        self.inJump = False
        # 0, unset
        # 1, call1 -> packetid, len1, len2, flag, call2 -> ignore
        self.callModeSp8 = 0
        # 0, not called sp12 before
        # 1, called sp12 before
        # 2, called sp12 in alt way before
        self.callModeSp12 = 0


    def getByte(self, pos):
        return self.exe.readUByte(pos)


    def getWord(self, pos):
        return self.exe.read(pos, 2, "H")


    def decodeOpCode(self):
        self.codeText = self.code[2]
        self.codeSize = self.code[3]
        self.codeFunc = self.code[4]
        self.regs = self.code[5]
        self.params = self.code[6]


    def init(self, offset):
        self.pos = offset
        # print("start pos: {0}".format(self.pos))
        self.code = self.findOpcode()
        if self.code is False:
            cmd = self.getWord(self.pos)
            self.exe.log("Error: Unknown opcode: {0}".format(hex(cmd)))
            exit(1)
        self.decodeOpCode()


    def initState(self, runFlag):
        self.registers["eax"] = 0
        self.registers["ecx"] = 0x1000000
        self.registers["edx"] = 0x3000000
        self.registers["ebx"] = 0x5000000
        self.registers["esp"] = 0x7000000
        self.registers["ebp"] = 0x9000000
        self.registers["esi"] = 0xB000000
        self.registers["edi"] = 0xD000000
        self.registers["xmm0"] = 0x0001000
        self.registers["xmm1"] = 0x0002000
        self.runFlag = runFlag
        self.startEsp = self.registers["esp"]
        self.flags["le"] = False


    def findOpcode(self):
        # 4 max of opcode len
        data = self.exe.exe[self.pos:self.pos + 4]
        for code in self.codes:
            if data[:code[0]] == code[1]:
                return code
        return False


    def getSize(self):
        return self.codeSize


    def inc(self, num):
        self.pos = self.pos + num


    def printLine(self):
        print("{0} {1}".format(self.exe.getAddrSec(self.pos), self.codeText))


    def printMemory(self):
        for key in sorted(self.memory):
            if key % 4 == 0:
                val = self.getMemory32(key)
                print("{0}: {1}, {2}".format(hex(key), hex(val), val))


    def printRegisters(self):
        for key in self.registers:
            print("{0}: {1}".format(key, hex(self.registers[key])))
        print("flags LE: {0}".format(self.flags["le"]))


    def move(self):
        if self.codeText == "retn" or self.codeText == "retn N":
            return False
        if self.Jumped is False:
            self.inc(self.getSize())
        else:
            self.Jumped = False
        self.code = self.findOpcode()
        if self.code is False:
            cmd = self.getWord(self.pos)
            self.exe.log("Error: Unknown opcode: {0} {1}".format(
                self.exe.getAddrSec(self.pos), hex(cmd)))
            exit(1)
        self.decodeOpCode()
        return True


    def run(self):
        if self.debug is True:
            self.printLine()
        self.codeFunc(self)
        if self.debugMemory is True:
            self.printRegisters()
            self.printMemory()


    def printAll(self):
        self.printLine()
        self.printRegisters()
        self.printMemory()


    def setMemory32(self, addr, value):
        try:
            arr = struct.unpack("4B", struct.pack("I", value))
        except BaseException:
            arr = struct.unpack("4B", struct.pack("i", value))
        self.setMemoryData(addr, arr)


    def setMemoryData(self, addr, arr):
        for idx in range(0, len(arr)):
            self.memory[addr + idx] = arr[idx]


    def getMemory32(self, addr):
        if addr not in self.memory:
            return 0
        val = struct.unpack("I",
                            struct.pack("4B",
                                        self.memory[addr],
                                        self.memory[addr + 1],
                                        self.memory[addr + 2],
                                        self.memory[addr + 3]
                                        )
                            )[0]
        return val


    def getMemory32Sign(self, addr):
        if addr not in self.memory:
            return 0
        val = struct.unpack("i",
                            struct.pack("4B",
                                        self.memory[addr],
                                        self.memory[addr + 1],
                                        self.memory[addr + 2],
                                        self.memory[addr + 3]
                                        )
                            )[0]
        return val


    def addRegister(self, register, value):
        self.registers[register] = (self.registers[register] + value) & \
            0xffffffff


    def subRegister(self, register, value):
        self.registers[register] = (self.registers[register] - value) & \
            0xffffffff


    def andRegister(self, register, value):
        self.registers[register] = (self.registers[register] & value) & \
            0xffffffff


    def orRegister(self, register, value):
        self.registers[register] = (self.registers[register] | value) & \
            0xffffffff


    def setRegister(self, register, value):
        self.registers[register] = value & 0xffffffff


    def addPacket(self, packetId, len1, len2, flag):
        # fix negative -1
        if len1 == 255 and len1 != len2:
            len1 = -1
        elif len1 == 4294967295:
            len1 = -1
            if len2 == 4294967295:
                len2 = -1
        elif len1 == 117440492:
            len1 = -1
            if len2 == 117440492:
                len2 = -1
        if packetId < 0xc00 and \
           packetId > 0 and \
           flag >= -1 and \
           flag <= 1 and \
           len1 >= -1 and \
           len2 >= -1 and \
           len1 != 0 and \
           len1 < 60000 and \
           len2 < 60000:
            self.packets.append((
                packetId,
                len1,
                len2,
                flag
            ))
            if self.debug is True:
                print("packet found {0}: {1}, {2}, {3}".format(
                      hex(packetId), len1, len2, flag))
        else:
            self.exe.log("Error: Wrong packet detected")
            self.exe.log("Wrong packet found {0}: {1}, {2}, {3}".format(
                hex(packetId), len1, len2, flag))
            exit(1)
